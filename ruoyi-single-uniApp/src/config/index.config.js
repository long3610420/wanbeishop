﻿const CONFIG = {
	// 开发环境配置
	development: {
		assetsPath: '/static', // 静态资源路径
		qqMapKey:'MVGBZ-R2U3U-W5CVY-2PQID-AT4VZ-PDF35',
	//	baseUrl: 'https://mojin.121.36.14.109/api-web',
		baseUrl: 'http://localhost:8091', // 后台接口请求地址
		hostUrl: 'https://mojin.121.36.14.109', // H5地址(前端运行地址)
		websocketUrl: '', // websocket服务端地址
		weixinAppId: 'wx7a3e78f36464b170', // 微信公众号appid
		storeId: 0 //
	},
	// 生产环境配置
	production: {
		qqMapKey:'MVGBZ-R2U3U-W5CVY-2PQID-AT4VZ-PDF35',
		assetsPath: '/static', // 静态资源路径
		baseUrl: 'https://mojin.121.36.14.109/api-web', // 后台接口请求地址
		hostUrl: 'https://mojin.121.36.14.109', // H5地址(前端运行地址)
		websocketUrl: '', // websocket服务端地址
		weixinAppId: 'wx7a3e78f36464b170', // 微信公众号appid
		storeId: 0 //
	}

};
export default CONFIG[process.env.NODE_ENV];
