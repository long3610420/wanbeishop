##  微商城

### 前言



### 项目地址




![image](docs/images/h5-qrcode.png)


### 更新记录

#### v1.2.0

updated 2020.07.08

- 新增：全新ui
- 新增：android/ios 微信授权登录
- 新增：ios 苹果授权登录
- 新增：商品列表直接下单
- 重构：登录注册页面ui重构
- 新增：首页分类模块
- 新增：小程序直播
- 新增：首页分类分页
- 新增：广告点击统计
- 新增：回到顶部组件
- 新增：客服按钮 点击显示客服二维码
- 新增：9大loading展示
- 新增：消息tab
- 新增：15个主题切换 主题换肤
- 新增: 站点帮助
- 优化：扫一扫功能
- 重构：商品详情页ui重构
- 新增：商品轮播图支持视频播放
- 新增：商品详情页支持视频播放
- 新增：H5 微信H5 小程序 APP分享功能

> [更多更新记录](docs/UPDATE.md)

### 思维导图



### 安装说明

1、安装相关依赖, 默认已拥有node环境
 改config/index.config.js里的配置

```
// 安装yarn命令
npm i -g yarn
// 安装依赖
yarn
```

2. 运行到指定平台

```
// 运行到H5
yarn serve
// 运行至对应的小程序平台
yarn dev:PLATFORM
```

值 | 平台
---|---
h5 | H5
mp-weixin | 微信小程序
mp-alipay | 支付宝小程序
mp-baidu | 百度小程序
mp-toutiao | 头条小程序
mp-qq | qq 小程序


3. 打包至线上部署
```
// H5打包
yarn build
yarn build:PLATFORM // 平台信息同上
```
使用对应的小程序客户端进行发布。

4、发布uni-app(打包为原生App云端)

配合HBuilderX使用

> 注： H5除主页外刷新页面会出现404 需配置nginx伪静态

```
location / {
    try_files $uri $uri/ /index.html;
}
```

### 配置文件

> [详见配置说明](docs/CONFIG.md)

### 官网


### 问题反馈

在使用中有任何问题，欢迎反馈给我，可以用以下联系方式跟我们交流


### 特别鸣谢

感谢以下的项目，排名不分先后

Vuejs： https://github.com/vuejs/vue

uni-app： https://uniapp.dcloud.io/

mix-mall: https://ext.dcloud.net.cn/plugin?id=200

ColorUI: https://github.com/weilanwl/ColorUI

...


