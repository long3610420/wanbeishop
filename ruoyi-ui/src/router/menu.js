export default [
    {
        "name": "SmsManager",
        "path": "/smsManager",
        "hidden": false,
        "redirect": "noRedirect",
        "component": "Layout",
        "alwaysShow": true,
        "meta": {
            "visible": "0",
            "title": "运营管理",
            "icon": "system"
        },
        "children": [
            {
                "name": "Setting",
                "path": "setting",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/brand/index",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "商城配置",
                    "icon": "post"
                },
                "children": [
                    {
                        "name": "BasicSetting",
                        "path": "basicSetting",
                        "hidden": false,
                        "component": "setting/basicSetting",
                        "meta": {
                            "visible": "0",
                            "title": "基本设置",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "YunSetting",
                        "path": "yunSetting",
                        "hidden": false,
                        "component": "setting/yunsetting",
                        "meta": {
                            "visible": "0",
                            "title": "OSS设置",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "LsPaySetting",
                        "path": "LsPaySetting",
                        "hidden": false,
                        "component": "setting/LsPaySetting/index",
                        "meta": {
                            "visible": "0",
                            "title": "支付设置",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "OmsOrderSetting",
                        "path": "OmsOrderSetting",
                        "hidden": false,
                        "component": "setting/OmsOrderSetting/index",
                        "meta": {
                            "visible": "0",
                            "title": "订单设置",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "LsEmailSetting",
                        "path": "LsEmailSetting",
                        "hidden": false,
                        "component": "setting/LsEmailSetting/index",
                        "meta": {
                            "visible": "0",
                            "title": "邮箱设置",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "LsSmsSetting",
                        "path": "LsSmsSetting",
                        "hidden": false,
                        "component": "setting/LsSmsSetting/index",
                        "meta": {
                            "visible": "0",
                            "title": "短信接口",
                            "icon": "documentation"
                        }
                    }
                ]
            },
            {
                "name": "HomeConfig",
                "path": "homeConfig",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "首页配置",
                    "icon": "post"
                },
                "children": [
                    {
                        "name": "SmsHomeBanner",
                        "path": "SmsHomeBanner",
                        "hidden": false,
                        "component": "sms/SmsHomeBanner/index",
                        "meta": {
                            "visible": "0",
                            "title": "轮播区",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "SmsHomeClassify",
                        "path": "SmsHomeClassify",
                        "hidden": false,
                        "component": "sms/SmsHomeClassify/index",
                        "meta": {
                            "visible": "0",
                            "title": "分类区",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "SmsHomeClassifyWaterfall",
                        "path": "SmsHomeClassifyWaterfall",
                        "hidden": false,
                        "component": "sms/SmsHomeClassifyWaterfall/index",
                        "meta": {
                            "visible": "0",
                            "title": "瀑布流分类区",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "SmsHomeActivity",
                        "path": "SmsHomeActivity",
                        "hidden": false,
                        "component": "sms/SmsHomeActivity/index",
                        "meta": {
                            "visible": "0",
                            "title": "活动区",
                            "icon": "documentation"
                        }
                    },
                ]
            },
            {
                "name": "ArticleMamaber",
                "path": "articleMamaber",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "文章管理",
                    "icon": "post"
                },
                "children": [
                    {
                        "name": "CmsArticle",
                        "path": "CmsArticle",
                        "hidden": false,
                        "component": "cms/article/articlelist/articlelist",
                        "meta": {
                            "visible": "0",
                            "title": "文章列表",
                            "icon": "#"
                        }
                    },
                    {
                        "name": "CmsArticleColumn",
                        "path": "CmsArticleColumn",
                        "hidden": false,
                        "component": "cms/article/columnlist/columnlist",
                        "meta": {
                            "visible": "0",
                            "title": "文章栏目",
                            "icon": "#"
                        }
                    }
                ]
            },
            {
                "name": "HelpManager",
                "path": "helpManager",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "帮助中心",
                    "icon": "post"
                },
                "children": [
                    {
                        "name": "CmsHelp",
                        "path": "CmsHelp",
                        "hidden": false,
                        "component": "cms/helpmanagement/helplist/helplist",
                        "meta": {
                            "visible": "0",
                            "title": "帮助列表",
                            "icon": "#"
                        }
                    },
                    {
                        "name": "CmsHelpCate",
                        "path": "CmsHelpCate",
                        "hidden": false,
                        "component": "cms/helpmanagement/helpcate/helpcate",
                        "meta": {
                            "visible": "0",
                            "title": "帮助分类",
                            "icon": "#"
                        }
                    }
                ]
            }
        ]
    },
    {
        "name": "Goods",
        "path": "/goods",
        "hidden": false,
        "redirect": "noRedirect",
        "component": "Layout",
        "alwaysShow": true,
        "meta": {
            "visible": "0",
            "title": "商品管理",
            "icon": "post"
        },
        "children": [
            {
                "name": "GoodsManager",
                "path": "goodsManager",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "商品管理",
                    "icon": "post"
                },
                "children": [
                    {
                        "name": "Goods",
                        "path": "goods",
                        "hidden": false,
                        "component": "goods/goods/index",
                        "meta": {
                            "visible": "0",
                            "title": "商品列表",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Addgoods",
                        "path": "addgoods",
                        "hidden": false,
                        "component": "goods/goods/updatespu",
                        "meta": {
                            "visible": "0",
                            "title": "商品新增",
                            "icon": "server"
                        }
                    }
                ]
            },
            {
                "name": "GoodsConfig",
                "path": "goodsConfig",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/brand/index",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "商品配置",
                    "icon": "post"
                },
                "children": [
                    {
                        "name": "Category",
                        "path": "category",
                        "hidden": false,
                        "component": "goods/category/index",
                        "meta": {
                            "visible": "0",
                            "title": "商品分类",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "Brand",
                        "path": "brand",
                        "hidden": false,
                        "component": "goods/brand/brand",
                        "meta": {
                            "visible": "0",
                            "title": "品牌管理",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "Spec",
                        "path": "spec",
                        "hidden": false,
                        "component": "goods/spec/index",
                        "meta": {
                            "visible": "0",
                            "title": "规格管理",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "Type",
                        "path": "type",
                        "hidden": false,
                        "component": "goods/type/index",
                        "meta": {
                            "visible": "0",
                            "title": "商品属性",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "Support",
                        "path": "support",
                        "hidden": false,
                        "component": "goods/support/index",
                        "meta": {
                            "visible": "0",
                            "title": "服务支持",
                            "icon": "documentation"
                        }
                    }
                ]
            },
            {
                "name": "GoodsData",
                "path": "goodsData",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "商品数据",
                    "icon": "post"
                },
                "children": [
                    {
                        "name": "Comment",
                        "path": "comment",
                        "hidden": false,
                        "component": "goods/comment/index",
                        "meta": {
                            "visible": "0",
                            "title": "单品评论",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "Attention",
                        "path": "attention",
                        "hidden": false,
                        "component": "goods/attention/index",
                        "meta": {
                            "visible": "0",
                            "title": "商品关注",
                            "icon": "documentation"
                        }
                    }
                ]
            }
        ]
    },
    {
        "name": "Member",
        "path": "/member",
        "hidden": false,
        "redirect": "noRedirect",
        "component": "Layout",
        "alwaysShow": true,
        "meta": {
            "visible": "0",
            "title": "会员管理",
            "icon": "post"
        },
        "children": [
            {
                "name": "MemberMuL",
                "path": "memberMuL",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "会员管理",
                    "icon": "post"
                },
                "children": [
                    {
                        "name": "UmsMember",
                        "path": "UmsMember",
                        "hidden": false,
                        "component": "member/UmsMember/index",
                        "meta": {
                            "visible": "0",
                            "title": "会员列表",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "UmsMemberLevel",
                        "path": "UmsMemberLevel",
                        "hidden": false,
                        "component": "member/UmsMemberLevel/index",
                        "meta": {
                            "visible": "0",
                            "title": "会员等级",
                            "icon": "documentation"
                        }
                    }
                ]
            },
            {
                "name": "MemberData",
                "path": "memberData",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "会员数据",
                    "icon": "post"
                },
                "children": [
                    {
                        "name": "UmsBrowseRecord",
                        "path": "UmsBrowseRecord",
                        "hidden": false,
                        "component": "member/UmsBrowseRecord/index",
                        "meta": {
                            "visible": "0",
                            "title": "会员浏览记录",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "UmsMemberAddress",
                        "path": "UmsMemberAddress",
                        "hidden": false,
                        "component": "member/UmsMemberAddress/index",
                        "meta": {
                            "visible": "0",
                            "title": "用户收货地址",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "UmsMemberPanicRecord",
                        "path": "UmsMemberPanicRecord",
                        "hidden": false,
                        "component": "member/UmsMemberPanicRecord/index",
                        "meta": {
                            "visible": "0",
                            "title": "用户抢购记录",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "UmsMemberPoint",
                        "path": "UmsMemberPoint",
                        "hidden": false,
                        "component": "member/UmsMemberPoint/index",
                        "meta": {
                            "visible": "0",
                            "title": "会员积分",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "UmsPreDepositRecord",
                        "path": "UmsPreDepositRecord",
                        "hidden": false,
                        "component": "member/UmsPreDepositRecord/index",
                        "meta": {
                            "visible": "0",
                            "title": "会员预存款",
                            "icon": "documentation"
                        }
                    }
                ]
            }
        ]
    },
    {
        "name": "Order",
        "path": "/order",
        "hidden": false,
        "redirect": "noRedirect",
        "component": "Layout",
        "alwaysShow": true,
        "meta": {
            "visible": "0",
            "title": "订单管理",
            "icon": "post"
        },
        "children": [
            {
                "name": "OrderManager",
                "path": "orderManager",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "订单管理",
                    "icon": "post"
                },
                "children": [
                    {
                        "name": "OmsOrder",
                        "path": "OmsOrder",
                        "hidden": false,
                        "component": "order/OmsOrder/index",
                        "meta": {
                            "visible": "0",
                            "title": "订单列表",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "Grouplist",
                        "path": "grouplist",
                        "hidden": false,
                        "component": "order/grouporder/grouplist",
                        "meta": {
                            "visible": "0",
                            "title": "拼团订单",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "OmsBackOrder",
                        "path": "OmsBackOrder",
                        "hidden": false,
                        "component": "order/OmsBackOrder/index",
                        "meta": {
                            "visible": "0",
                            "title": "退单退款",
                            "icon": "documentation"
                        }
                    }
                ]
            },
            {
                "name": "DelevierManager",
                "path": "delevierManager",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "配送管理",
                    "icon": "documentation"
                },
                "children": [
                    {
                        "name": "OmsLogisticsCompany",
                        "path": "OmsLogisticsCompany",
                        "hidden": false,
                        "component": "order/OmsLogisticsCompany/index",
                        "meta": {
                            "visible": "0",
                            "title": "物流公司",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "OmsLogisticsTemplate",
                        "path": "OmsLogisticsTemplate",
                        "hidden": false,
                        "component": "order/OmsLogisticsTemplate/index",
                        "meta": {
                            "visible": "0",
                            "title": "物流模版",
                            "icon": "documentation"
                        }
                    }
                ]
            },
            {
                "name": "OrderData",
                "path": "orderData",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "订单数据",
                    "icon": "post"
                },
                "children": [
                    {
                        "name": "OmsCommissionRecords",
                        "path": "OmsCommissionRecords",
                        "hidden": false,
                        "component": "order/OmsCommissionRecords/index",
                        "meta": {
                            "visible": "0",
                            "title": "佣金记录",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "OmsTransRecords",
                        "path": "OmsTransRecords",
                        "hidden": false,
                        "component": "order/OmsTransRecords/index",
                        "meta": {
                            "visible": "0",
                            "title": "支付流水",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "OmsShoppingCart",
                        "path": "OmsShoppingCart",
                        "hidden": false,
                        "component": "order/OmsShoppingCart/index",
                        "meta": {
                            "visible": "0",
                            "title": "购物车",
                            "icon": "documentation"
                        }
                    },
                    {
                        "name": "OmsBillingRecords",
                        "path": "OmsBillingRecords",
                        "hidden": false,
                        "component": "order/OmsBillingRecords/index",
                        "meta": {
                            "visible": "0",
                            "title": "账单记录",
                            "icon": "documentation"
                        }
                    }
                ]
            }
        ]
    },
    {
        "name": "MarketingManager",
        "path": "/marketingManager",
        "hidden": false,
        "redirect": "noRedirect",
        "component": "Layout",
        "alwaysShow": true,
        "meta": {
            "visible": "0",
            "title": "促销管理",
            "icon": "post"
        },
        "children": [
            {
                "name": "MarketingConfig",
                "path": "marketingConfig",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "促销设置",
                    "icon": "post"
                },
                "children": [
                    {
                        "name": "Marketingcate",
                        "path": "marketingcate",
                        "hidden": false,
                        "component": "marketingmanagement/marketing/marketingcate/marketingcates",
                        "meta": {
                            "visible": "0",
                            "title": "促销分类",
                            "icon": "job"
                        }
                    },
                    {
                        "name": "Marketingsetting",
                        "path": "marketingsetting",
                        "hidden": false,
                        "component": "marketingmanagement/marketing/marketingsetting/marketingsetting",
                        "meta": {
                            "visible": "0",
                            "title": "促销设置",
                            "icon": "job"
                        }
                    }
                ]
            },
            {
                "name": "MarketingPlatForm",
                "path": "marketingPlatForm",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "平台促销",
                    "icon": "post"
                },
                "children": [
                    {
                        "name": "Redenvelopelist",
                        "path": "redenvelopelist",
                        "hidden": false,
                        "component": "marketingmanagement/mallmarketing/redenvelopelist/redenvelopelist",
                        "meta": {
                            "visible": "0",
                            "title": "平台红包",
                            "icon": "job"
                        }
                    }
                ]
            },
            {
                "name": "MarketingStore",
                "path": "marketingStore",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "店铺促销",
                    "icon": "post"
                },
                "children": [
                    {
                        "name": "Registermarketing",
                        "path": "registermarketing",
                        "hidden": false,
                        "component": "marketingmanagement/storemarketing/register/registermarketing",
                        "meta": {
                            "visible": "0",
                            "title": "注册送券",
                            "icon": "job"
                        }
                    },
                    {
                        "name": "Marketingcoupon",
                        "path": "marketingcoupon",
                        "hidden": false,
                        "component": "marketingmanagement/storemarketing/coupon/coupon",
                        "meta": {
                            "visible": "0",
                            "title": "优惠券促销",
                            "icon": "job"
                        }
                    }
                ]
            },
            {
                "name": "MarketingGoods",
                "path": "marketingGoods",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "商品促销",
                    "icon": "post"
                },
                "children": [
                    {
                        "name": "GoodAudit",
                        "path": "goodAudit",
                        "hidden": true,
                        "component": "store/goodsaudit/goodsauditlist/spuauditdetail",
                        "meta": {
                            "visible": "1",
                            "title": "商品审核",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Trialpromotionapplylist",
                        "path": "trialpromotionapplylist",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/try/trialpromotionapplylist",
                        "meta": {
                            "visible": "1",
                            "title": "试用促销申请",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Addarticle",
                        "path": "addarticle",
                        "hidden": true,
                        "component": "cms/article/articlelist/addarticle",
                        "meta": {
                            "visible": "1",
                            "title": "添加文章",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Updatearticle",
                        "path": "updatearticle",
                        "hidden": true,
                        "component": "cms/article/articlelist/updatearticle",
                        "meta": {
                            "visible": "1",
                            "title": "修改文章",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Addhelp",
                        "path": "addhelp",
                        "hidden": true,
                        "component": "cms/helpmanagement/helplist/addhelp",
                        "meta": {
                            "visible": "1",
                            "title": "新增帮助",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Updatehelp",
                        "path": "updatehelp",
                        "hidden": true,
                        "component": "cms/helpmanagement/helplist/updatehelp",
                        "meta": {
                            "visible": "1",
                            "title": "修改帮助",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "OrderDetail",
                        "path": "orderDetail",
                        "hidden": true,
                        "component": "order/OmsOrder/orderdetail",
                        "meta": {
                            "visible": "1",
                            "title": "订单详情",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Nopaylist",
                        "path": "nopaylist",
                        "hidden": true,
                        "component": "order/grouporder/nopaylist",
                        "meta": {
                            "visible": "1",
                            "title": "团购未付款订单页面",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Grouporderdetail",
                        "path": "grouporderdetail",
                        "hidden": true,
                        "component": "order/grouporder/grouporderdetail",
                        "meta": {
                            "visible": "1",
                            "title": "拼团订单详情",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Nopaygrouporderdetail",
                        "path": "nopaygrouporderdetail",
                        "hidden": true,
                        "component": "order/grouporder/nopaygrouporderdetail",
                        "meta": {
                            "visible": "1",
                            "title": "团购未付款订单详情页",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Returnreview",
                        "path": "returnreview",
                        "hidden": true,
                        "component": "order/OmsBackOrder/returnreview",
                        "meta": {
                            "visible": "1",
                            "title": "退货审核",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Refundreview",
                        "path": "refundreview",
                        "hidden": true,
                        "component": "order/OmsBackOrder/refundreview",
                        "meta": {
                            "visible": "1",
                            "title": "退款审核",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Groupmarketinglist",
                        "path": "groupmarketinglist",
                        "hidden": false,
                        "component": "marketingmanagement/mobilemarketing/groupmarketing/groupmarketinglist",
                        "meta": {
                            "visible": "0",
                            "title": "拼团促销",
                            "icon": "job"
                        }
                    },
                    {
                        "name": "Addfulldown",
                        "path": "addfulldown",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/fulldown/addfulldown",
                        "meta": {
                            "visible": "1",
                            "title": "添加满减促销",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Coupondetail",
                        "path": "coupondetail",
                        "hidden": true,
                        "component": "marketingmanagement/storemarketing/coupon/coupondetail",
                        "meta": {
                            "visible": "1",
                            "title": "优惠券详情",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Redenvelopedetail",
                        "path": "redenvelopedetail",
                        "hidden": true,
                        "component": "marketingmanagement/mallmarketing/redenvelopelist/redenvelopedetail",
                        "meta": {
                            "visible": "1",
                            "title": "红包详情",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Combinedskus",
                        "path": "combinedskus",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/combinedmarketing/combinedskus",
                        "meta": {
                            "visible": "1",
                            "title": "商品组合单品",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Addpanicbuy",
                        "path": "addpanicbuy",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/panicbuy/addpanicbuy",
                        "meta": {
                            "visible": "1",
                            "title": "新增秒杀促销",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Updatepanicbuy",
                        "path": "updatepanicbuy",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/panicbuy/updatepanicbuy",
                        "meta": {
                            "visible": "1",
                            "title": "修改秒杀促销",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Addfall",
                        "path": "addfall",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/fallmarketing/addfallmarketing",
                        "meta": {
                            "visible": "1",
                            "title": " 新增直降促销",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Updatefall",
                        "path": "updatefall",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/fallmarketing/updatefallmarketing",
                        "meta": {
                            "visible": "1",
                            "title": "修改直降促销",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Updatefulldown",
                        "path": "updatefulldown",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/fulldown/updatefulldown",
                        "meta": {
                            "visible": "1",
                            "title": "修改满减促销",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Addpointsku",
                        "path": "addpointsku",
                        "hidden": true,
                        "component": "marketingmanagement/pointsmall/pointsku/addpointsku",
                        "meta": {
                            "visible": "1",
                            "title": "新增积分商品",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Updatepointsku",
                        "path": "updatepointsku",
                        "hidden": true,
                        "component": "marketingmanagement/pointsmall/pointsku/updatepointsku",
                        "meta": {
                            "visible": "1",
                            "title": "修改积分商品",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Addfulldiscount",
                        "path": "addfulldiscount",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/fulldiscount/addfulldiscount",
                        "meta": {
                            "visible": "1",
                            "title": "新增满折促销",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Updatefulldiscount",
                        "path": "updatefulldiscount",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/fulldiscount/updatefulldiscount",
                        "meta": {
                            "visible": "1",
                            "title": "修改满折促销",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Addtry",
                        "path": "addtry",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/try/addtry",
                        "meta": {
                            "visible": "1",
                            "title": "新增试用促销",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Updatetry",
                        "path": "updatetry",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/try/updatetry",
                        "meta": {
                            "visible": "1",
                            "title": "修改试用促销",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Trydetail",
                        "path": "trydetail",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/try/trydetail",
                        "meta": {
                            "visible": "1",
                            "title": "试用促销详情",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Pointorderdetail",
                        "path": "pointorderdetail",
                        "hidden": true,
                        "component": "marketingmanagement/pointsmall/pointorder/pointorderdetail",
                        "meta": {
                            "visible": "1",
                            "title": "积分订单详情",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Adddepositpresale",
                        "path": "adddepositpresale",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/presale/adddepositpresale",
                        "meta": {
                            "visible": "1",
                            "title": "添加定金预售",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Updatedepositpresale",
                        "path": "updatedepositpresale",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/presale/updatedepositpresale",
                        "meta": {
                            "visible": "1",
                            "title": "修改定金预售",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Addfullpresale",
                        "path": "addfullpresale",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/presale/addfullpresale",
                        "meta": {
                            "visible": "1",
                            "title": "添加全款预售",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Updatefullpresale",
                        "path": "updatefullpresale",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/presale/updatefullpresale",
                        "meta": {
                            "visible": "1",
                            "title": "修改全款预售",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Addfullgift",
                        "path": "addfullgift",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/fullgift/addfullgift",
                        "meta": {
                            "visible": "1",
                            "title": "新增满赠促销",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Crowdfundingschedule",
                        "path": "crowdfundingschedule",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/crowdfunding/crowdfundingschedule",
                        "meta": {
                            "visible": "1",
                            "title": "项目进度管理",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Addgroupmarketing",
                        "path": "addgroupmarketing",
                        "hidden": true,
                        "component": "marketingmanagement/mobilemarketing/groupmarketing/addgroupmarketing",
                        "meta": {
                            "visible": "1",
                            "title": "新增拼团促销",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Updategroupmarketing",
                        "path": "updategroupmarketing",
                        "hidden": true,
                        "component": "marketingmanagement/mobilemarketing/groupmarketing/updategroupmarketing",
                        "meta": {
                            "visible": "1",
                            "title": "修改拼团促销",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Groupmarketingdetail",
                        "path": "groupmarketingdetail",
                        "hidden": true,
                        "component": "marketingmanagement/mobilemarketing/groupmarketing/groupmarketingdetail",
                        "meta": {
                            "visible": "1",
                            "title": "拼团促销详情",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Addcrowdfunding",
                        "path": "addcrowdfunding",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/crowdfunding/addcrowdfunding",
                        "meta": {
                            "visible": "1",
                            "title": "新增众筹",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Updatefullgift",
                        "path": "updatefullgift",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/fullgift/updatefullgift",
                        "meta": {
                            "visible": "1",
                            "title": "更新满赠促销",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Updatecrowdfunding",
                        "path": "updatecrowdfunding",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/crowdfunding/updatecrowdfunding",
                        "meta": {
                            "visible": "1",
                            "title": "修改众筹",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Crowdfundingdetail",
                        "path": "crowdfundingdetail",
                        "hidden": true,
                        "component": "marketingmanagement/goodsmarketing/crowdfunding/crowdfundingdetail",
                        "meta": {
                            "visible": "1",
                            "title": "查看众筹详情 ",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Addlogisticstemplate",
                        "path": "addlogisticstemplate",
                        "hidden": true,
                        "component": "order/OmsLogisticsTemplate/addlogisticstemplate",
                        "meta": {
                            "visible": "1",
                            "title": "添加物流模板",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Updlogisticstemplate",
                        "path": "updlogisticstemplate",
                        "hidden": true,
                        "component": "order/OmsLogisticsTemplate/updatelogisticstemplate",
                        "meta": {
                            "visible": "1",
                            "title": "修改物流模板",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "Fulldown",
                        "path": "fulldown",
                        "hidden": false,
                        "component": "marketingmanagement/goodsmarketing/fulldown/fulldown",
                        "meta": {
                            "visible": "0",
                            "title": "满减促销",
                            "icon": "job"
                        }
                    },
                    {
                        "name": "Fulldiscount",
                        "path": "fulldiscount",
                        "hidden": false,
                        "component": "marketingmanagement/goodsmarketing/fulldiscount/fulldiscount",
                        "meta": {
                            "visible": "0",
                            "title": "满折促销",
                            "icon": "job"
                        }
                    },
                    {
                        "name": "Fullgift",
                        "path": "fullgift",
                        "hidden": false,
                        "component": "marketingmanagement/goodsmarketing/fullgift/fullgift",
                        "meta": {
                            "visible": "0",
                            "title": "满赠促销",
                            "icon": "job"
                        }
                    },
                    {
                        "name": "Fallmarketing",
                        "path": "fallmarketing",
                        "hidden": false,
                        "component": "marketingmanagement/goodsmarketing/fallmarketing/fallmarketing",
                        "meta": {
                            "visible": "0",
                            "title": "直降促销",
                            "icon": "job"
                        }
                    },
                    {
                        "name": "Presale",
                        "path": "presale",
                        "hidden": false,
                        "component": "marketingmanagement/goodsmarketing/presale/presale",
                        "meta": {
                            "visible": "0",
                            "title": "预售促销",
                            "icon": "job"
                        }
                    },
                    {
                        "name": "Combinedlist",
                        "path": "combinedlist",
                        "hidden": false,
                        "component": "marketingmanagement/goodsmarketing/combinedmarketing/combinedlist",
                        "meta": {
                            "visible": "0",
                            "title": "商品组合",
                            "icon": "job"
                        }
                    },
                    {
                        "name": "Try",
                        "path": "try",
                        "hidden": false,
                        "component": "marketingmanagement/goodsmarketing/try/try",
                        "meta": {
                            "visible": "0",
                            "title": "试用促销",
                            "icon": "job"
                        }
                    },
                    {
                        "name": "Signset",
                        "path": "signset",
                        "hidden": false,
                        "component": "marketingmanagement/mobilemarketing/signset/signset",
                        "meta": {
                            "visible": "0",
                            "title": "签到活动",
                            "icon": "job"
                        }
                    },
                    {
                        "name": "Panicbuy",
                        "path": "panicbuy",
                        "hidden": false,
                        "component": "marketingmanagement/goodsmarketing/panicbuy/panicbuy",
                        "meta": {
                            "visible": "0",
                            "title": "秒杀促销",
                            "icon": "job"
                        }
                    },
                    {
                        "name": "Crowdfunding",
                        "path": "crowdfunding",
                        "hidden": false,
                        "component": "marketingmanagement/goodsmarketing/crowdfunding/crowdfunding",
                        "meta": {
                            "visible": "0",
                            "title": "众筹项目",
                            "icon": "job"
                        }
                    }
                ]
            },
            {
                "name": "JifenShop",
                "path": "jifenShop",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "积分商城",
                    "icon": "post"
                },
                "children": [
                    {
                        "name": "Pointcate",
                        "path": "pointcate",
                        "hidden": false,
                        "component": "marketingmanagement/pointsmall/pointcate/pointcate",
                        "meta": {
                            "visible": "0",
                            "title": "分类管理",
                            "icon": "job"
                        }
                    },
                    {
                        "name": "Pointskulist",
                        "path": "pointskulist",
                        "hidden": false,
                        "component": "marketingmanagement/pointsmall/pointsku/pointskulist",
                        "meta": {
                            "visible": "0",
                            "title": "商品列表",
                            "icon": "job"
                        }
                    },
                    {
                        "name": "Pointseting",
                        "path": "pointseting",
                        "hidden": false,
                        "component": "marketingmanagement/pointsmall/pointseting/pointseting",
                        "meta": {
                            "visible": "0",
                            "title": "积分设置",
                            "icon": "job"
                        }
                    },
                    {
                        "name": "Pointorderlist",
                        "path": "pointorderlist",
                        "hidden": false,
                        "component": "marketingmanagement/pointsmall/pointorder/pointorderlist",
                        "meta": {
                            "visible": "0",
                            "title": "订单列表",
                            "icon": "job"
                        }
                    }
                ]
            }
        ]
    },
    {
        "name": "FenxiaoManager",
        "path": "/fenxiaoManager",
        "hidden": false,
        "redirect": "noRedirect",
        "component": "Layout",
        "alwaysShow": true,
        "meta": {
            "visible": "0",
            "title": "分销管理",
            "icon": "system"
        },
        "children": [
            {
                "name": "FenxiaoConfig",
                "path": "fenxiaoConfig",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "佣金设置",
                    "icon": "system"
                },
                "children": [
                    {
                        "name": "YongjinConfig",
                        "path": "yongjinConfig",
                        "hidden": false,
                        "component": "distributionmanagement/commissionset/commissionsetlist/commissionsetlist",
                        "meta": {
                            "visible": "0",
                            "title": "佣金设置",
                            "icon": "documentation"
                        }
                    }
                ]
            },
            {
                "name": "FenxiaoMember",
                "path": "fenxiaoMember",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "分销会员",
                    "icon": "system"
                },
                "children": [
                    {
                        "name": "DownMember",
                        "path": "downMember",
                        "hidden": true,
                        "component": "distributionmanagement/distributioncustomer/distributioncustomerlist/subdistributioncustomerlist",
                        "meta": {
                            "visible": "1",
                            "title": "下级会员信息",
                            "icon": "server"
                        }
                    },
                    {
                        "name": "YongjinMember",
                        "path": "yongjinMember",
                        "hidden": false,
                        "component": "distributionmanagement/distributioncustomer/distributioncustomerlist/distributioncustomerlist",
                        "meta": {
                            "visible": "0",
                            "title": "分销会员",
                            "icon": "documentation"
                        }
                    }
                ]
            },
            {
                "name": "FenxiaoOrder",
                "path": "fenxiaoOrder",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "分销订单",
                    "icon": "system"
                },
                "children": [
                    {
                        "name": "YongjinOrder",
                        "path": "yongjinOrder",
                        "hidden": false,
                        "component": "distributionmanagement/distributionorder/distributionorderlist/distributionorderlist",
                        "meta": {
                            "visible": "0",
                            "title": "分销订单",
                            "icon": "documentation"
                        }
                    }
                ]
            },
            {
                "name": "WithdrawManager",
                "path": "withdrawManager",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "goods/goods/views",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "提现管理",
                    "icon": "system"
                },
                "children": [
                    {
                        "name": "UmsWithdraw",
                        "path": "UmsWithdraw",
                        "hidden": false,
                        "component": "member/UmsWithdraw/index",
                        "meta": {
                            "visible": "0",
                            "title": "提现记录",
                            "icon": "documentation"
                        }
                    }
                ]
            }
        ]
    },
    {
        "name": "System",
        "path": "/system",
        "hidden": false,
        "redirect": "noRedirect",
        "component": "Layout",
        "alwaysShow": true,
        "meta": {
            "visible": "0",
            "title": "系统管理",
            "icon": "system"
        },
        "children": [
            {
                "name": "User",
                "path": "user",
                "hidden": false,
                "component": "system/user/index",
                "meta": {
                    "visible": "0",
                    "title": "用户管理",
                    "icon": "user"
                }
            },
            {
                "name": "Role",
                "path": "role",
                "hidden": false,
                "component": "system/role/index",
                "meta": {
                    "visible": "0",
                    "title": "角色管理",
                    "icon": "peoples"
                }
            },
            {
                "name": "Menu",
                "path": "menu",
                "hidden": false,
                "component": "system/menu/index",
                "meta": {
                    "visible": "0",
                    "title": "菜单管理",
                    "icon": "tree-table"
                }
            },
            {
                "name": "Dept",
                "path": "dept",
                "hidden": false,
                "component": "system/dept/index",
                "meta": {
                    "visible": "0",
                    "title": "部门管理",
                    "icon": "tree"
                }
            },
            {
                "name": "Post",
                "path": "post",
                "hidden": false,
                "component": "system/post/index",
                "meta": {
                    "visible": "0",
                    "title": "岗位管理",
                    "icon": "post"
                }
            },
            {
                "name": "Dict",
                "path": "dict",
                "hidden": false,
                "component": "system/dict/index",
                "meta": {
                    "visible": "0",
                    "title": "字典管理",
                    "icon": "dict"
                }
            },
            {
                "name": "Config",
                "path": "config",
                "hidden": false,
                "component": "system/config/index",
                "meta": {
                    "visible": "0",
                    "title": "参数设置",
                    "icon": "edit"
                }
            },
            {
                "name": "Notice",
                "path": "notice",
                "hidden": false,
                "component": "system/notice/index",
                "meta": {
                    "visible": "0",
                    "title": "通知公告",
                    "icon": "message"
                }
            },
            {
                "name": "Log",
                "path": "log",
                "hidden": false,
                "redirect": "noRedirect",
                "component": "system/log/index",
                "alwaysShow": true,
                "meta": {
                    "visible": "0",
                    "title": "日志管理",
                    "icon": "log"
                },
                "children": [
                    {
                        "name": "Operlog",
                        "path": "operlog",
                        "hidden": false,
                        "component": "monitor/operlog/index",
                        "meta": {
                            "visible": "0",
                            "title": "操作日志",
                            "icon": "form"
                        }
                    },
                    {
                        "name": "Logininfor",
                        "path": "logininfor",
                        "hidden": false,
                        "component": "monitor/logininfor/index",
                        "meta": {
                            "visible": "0",
                            "title": "登录日志",
                            "icon": "logininfor"
                        }
                    }
                ]
            }
        ]
    },
    {
        "name": "Tool",
        "path": "/tool",
        "hidden": false,
        "redirect": "noRedirect",
        "component": "Layout",
        "alwaysShow": true,
        "meta": {
            "visible": "0",
            "title": "系统工具",
            "icon": "tool"
        },
        "children": [
            {
                "name": "Build",
                "path": "build",
                "hidden": false,
                "component": "tool/build/index",
                "meta": {
                    "visible": "0",
                    "title": "表单构建",
                    "icon": "build"
                }
            },
            {
                "name": "Gen",
                "path": "gen",
                "hidden": false,
                "component": "tool/gen/index",
                "meta": {
                    "visible": "0",
                    "title": "代码生成",
                    "icon": "code"
                }
            },
            {
                "name": "Swagger",
                "path": "swagger",
                "hidden": false,
                "component": "tool/swagger/index",
                "meta": {
                    "visible": "0",
                    "title": "系统接口",
                    "icon": "swagger"
                }
            }
        ]
    },
    {
        "name": "Monitor",
        "path": "/monitor",
        "hidden": false,
        "redirect": "noRedirect",
        "component": "Layout",
        "alwaysShow": true,
        "meta": {
            "visible": "0",
            "title": "系统监控",
            "icon": "monitor"
        },
        "children": [
            {
                "name": "Online",
                "path": "online",
                "hidden": false,
                "component": "monitor/online/index",
                "meta": {
                    "visible": "0",
                    "title": "在线用户",
                    "icon": "online"
                }
            },
            {
                "name": "Job",
                "path": "job",
                "hidden": false,
                "component": "monitor/job/index",
                "meta": {
                    "visible": "0",
                    "title": "定时任务",
                    "icon": "job"
                }
            },
            {
                "name": "Druid",
                "path": "druid",
                "hidden": false,
                "component": "monitor/druid/index",
                "meta": {
                    "visible": "0",
                    "title": "数据监控",
                    "icon": "druid"
                }
            },
            {
                "name": "Server",
                "path": "server",
                "hidden": false,
                "component": "monitor/server/index",
                "meta": {
                    "visible": "0",
                    "title": "服务监控",
                    "icon": "server"
                }
            }
        ]
    }
]