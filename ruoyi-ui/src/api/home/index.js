import request from '@/utils/request'

// 查询首页信息
export function listHomeAmount(query) {
    return request({
        url: '/order/OmsOrder/sales/amount',
        method: 'get',
        params: query
    })
}
