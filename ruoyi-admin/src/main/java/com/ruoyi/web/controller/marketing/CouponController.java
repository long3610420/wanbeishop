package com.ruoyi.web.controller.marketing;


import com.ruoyi.marketing.domain.Coupon;
import com.ruoyi.marketing.domain.CouponDetails;
import com.ruoyi.marketing.service.CouponService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by 魔金商城平台管理on 2019/7/5.
 * 优惠券控制器
 */
@RestController
@Api("优惠券接口")
public class CouponController {


    /**
     * 注入优惠券service
     */
    @Autowired
    private CouponService couponService;

    /**
     * 分页查询优惠券
     *
     * @param pageHelper 分页帮助类
     * @param name       查询条件,优惠券名称
     * @return 优惠券集合
     */
    @GetMapping("/coupon")
    @ApiOperation(value = "查询优惠券", notes = "查询优惠券（需要认证）")
    @PreAuthorize("@ss.hasPermi('marketingcoupon')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "优惠券名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "优惠券集合", response = Coupon.class)
    })
    public BaseResponse queryCoupon(@ApiIgnore PageHelper<Coupon> pageHelper, String name) {
        return BaseResponse.build(couponService.queryCoupon(pageHelper, name, CommonConstant.ADMIN_STOREID));
    }


    /**
     * 复制优惠券链接
     *
     * @param couponId 优惠券id
     * @return -1:没有查询到该优惠券, 0:优惠券已过期, 其他字符串为优惠券链接
     */
    @GetMapping("/copycoupon/{couponId}")
    @ApiOperation(value = "复制优惠券链接", notes = "复制优惠券链接（需要认证）")
    @PreAuthorize("@ss.hasPermi('marketingcoupon')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "couponId", value = "优惠券id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "-1:没有查询到该优惠券, 0:优惠券已过期, 其他字符串为优惠券链接", response = String.class)
    })
    public String copyCoupon(@PathVariable long couponId) {
        return couponService.copyCoupon(couponId, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 导出优惠券
     *
     * @param couponId 优惠券id
     */
    @PostMapping("/exportcoupon/{couponId}")
    @ApiOperation(value = "导出优惠券", notes = "导出优惠券（需要认证）")
    @PreAuthorize("@ss.hasPermi('marketingcoupon')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "couponId", value = "优惠券id"),
    })
    public void exportCoupon(@PathVariable long couponId, HttpServletResponse response) throws IOException {
       /* OutputStream os = response.getOutputStream();
        ExcelUtils.exportExcel(response, String.valueOf(couponId).concat("优惠券券码.xls"), () ->
                couponService.exportCoupon(os, CommonConstant.ADMIN_STOREID, couponId)
        );*/
    }

    /**
     * 删除优惠券
     *
     * @param ids 优惠券ids数组
     * @return 删除返回码 -1失败 >=1成功
     */
    @DeleteMapping("/coupon")
    @ApiOperation(value = "删除优惠券", notes = "删除优惠券（需要认证）")
    @PreAuthorize("@ss.hasPermi('marketingcoupon')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "优惠券ids数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "删除返回码 -1失败 >=1成功", response = Integer.class)
    })
    public int deleteCoupon(long[] ids) {
        return couponService.deleteCoupon(ids, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 添加优惠券
     *
     * @param coupon 优惠券实体类
     * @return 添加返回码 -1 失败 -2 限领张数不正确 -3 限领张数不能大于总张数 >=1 成功
     */
    @PostMapping("/coupon")
    @ApiOperation(value = "添加优惠券", notes = "添加优惠券（需要认证）")
    @PreAuthorize("@ss.hasPermi('marketingcoupon')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "添加返回码 -1 失败 -2 限领张数不正确 -3 限领张数不能大于总张数 >=1 成功", response = Integer.class)
    })
    public int addCoupon(@RequestBody Coupon coupon) {
        return couponService.addCoupon(coupon, CommonConstant.ADMIN_STOREID);
    }


    /**
     * 查询优惠券详情
     *
     * @param couponId 优惠券id
     * @return 优惠券详情实体类
     */
    @GetMapping("/coupon/{couponId}")
    @ApiOperation(value = "查询优惠券详情", notes = "查询优惠券详情（需要认证）")
    @PreAuthorize("@ss.hasPermi('marketingcoupon')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "couponId", value = "优惠券id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "优惠券详情实体", response = CouponDetails.class)
    })
    public CouponDetails queryCouponDetails(@PathVariable long couponId) {
        return couponService.queryCouponDetails(CommonConstant.ADMIN_STOREID, couponId);
    }

}
