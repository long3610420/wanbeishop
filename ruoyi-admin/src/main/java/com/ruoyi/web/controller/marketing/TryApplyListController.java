package com.ruoyi.web.controller.marketing;


import com.ruoyi.marketing.domain.TrySkuApply;
import com.ruoyi.marketing.service.TrySkuApplyService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author 魔金商城
 * @date 2019-09-20 14:34
 * <p>
 * 试用申请列表控制器
 */
@RestController
@Api("试用申请列表接口")
public class TryApplyListController {


    /**
     * 注入试用申请服务接口
     */
    @Autowired
    private TrySkuApplyService trySkuApplyService;


    /**
     * 分页查询试用申请列表
     *
     * @param pageHelper 分页帮助类
     * @param tryId      试用促销id
     * @return 返回试用申请列表
     */
    @GetMapping("/tryapply")
    @ApiOperation(value = "分页查询试用申请列表", notes = "分页查询试用申请列表（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "tryId", value = "试用促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回试用申请列表", response = TrySkuApply.class)
    })
    public BaseResponse queryTryApplyList(@ApiIgnore PageHelper<TrySkuApply> pageHelper, long tryId) {
        return BaseResponse.build(trySkuApplyService.queryApplyByTryIdAndStoreId(pageHelper, tryId, CommonConstant.ADMIN_STOREID));
    }


}
