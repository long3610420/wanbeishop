package com.ruoyi.web.controller.promotion;


import com.ruoyi.goods.domain.PmsBrand;
import com.ruoyi.goods.domain.PmsCategory;
import com.ruoyi.goods.domain.PmsGoods;
import com.ruoyi.goods.service.IPmsBrandService;
import com.ruoyi.goods.service.IPmsCategoryService;
import com.ruoyi.goods.service.IPmsGoodsService;
import com.ruoyi.goods.vo.SpuSearchCondition;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * @author 魔金商城
 * @date 2019-06-17 16:24
 * <p>
 * 佣金控制器
 */
@RestController
@Api(description = "佣金接口")
public class CommissionController {


    /**
     * 注入商品服务接口
     */
    @Autowired
    private IPmsGoodsService spuService;

    /**
     * 注入品牌服务接口
     */
    @Autowired
    private IPmsBrandService brandService;

    /**
     * 注入分类服务接口
     */
    @Autowired
    private IPmsCategoryService categoryService;

    /**
     * 分页查询商品列表（佣金）
     *
     * @param pageHelper         分页帮助类
     * @param spuSearchCondition 搜索条件
     * @return 返回商品信息（佣金）
     */
    @GetMapping("/commission")
    @ApiOperation(value = "分页查询商品列表（佣金）", notes = "分页查询商品列表（佣金）（需要认证）")

    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "id", value = "商品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "商品编号"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "brandId", value = "品牌ID"),
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "firstCateId", value = "一级分类id"),
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "secondCateId", value = "二级分类id"),
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "thirdCateId", value = "三级分类id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回商品信息（佣金）", response = PmsGoods.class)
    })
    public BaseResponse querySpusForCommission(@ApiIgnore PageHelper<PmsGoods> pageHelper, @ApiIgnore SpuSearchCondition spuSearchCondition) {
        return BaseResponse.build(spuService.querySpus(pageHelper, spuSearchCondition));
    }


    /**
     * 查询所有品牌
     *
     * @return 品牌集合
     */
    @GetMapping("/commission/brand")
    @ApiOperation(value = "查询所有品牌（佣金）", notes = "查询所有品牌（佣金）（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "品牌集合", response = PmsBrand.class)
    })
    public List<PmsBrand> queryAllBrand() {
        return brandService.queryAllBrands(CommonConstant.ADMIN_STOREID);
    }


    /**
     * 根据分类id查询所有子分类信息
     *
     * @param parentId 父级id
     * @return 返回该父级下的所有分类信息
     */
    @GetMapping("/commission/category/{parentId}")
    @ApiOperation(value = "根据分类id查询所有子分类信息", notes = "根据分类id查询所有子分类信息（需要认证）")

    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "parentId", value = "父级id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回该父级下的所有分类信息", response = PmsCategory.class)
    })
    public List<PmsCategory> queryCategoryByParentId(@PathVariable long parentId) {
        return categoryService.queryCategoryByParentId(parentId);
    }


    /**
     * 设置佣金
     *
     * @param id              商品id
     * @param commissionRate  佣金比例
     * @param sCommissionRate 二级佣金比例
     * @return 成功返回>0 失败返回0
     */
    @PutMapping("/commission")
    @ApiOperation(value = "设置佣金", notes = "设置佣金（需要认证）")

    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "id", value = "父级id"),
            @ApiImplicitParam(paramType = "form", dataType = "BigDecimal", name = "commissionRate", value = "佣金比例"),
            @ApiImplicitParam(paramType = "form", dataType = "BigDecimal", name = "sCommissionRate", value = "二级佣金比例"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回>0 失败返回0", response = Integer.class)
    })
    public int updateCommission(long id, BigDecimal commissionRate, BigDecimal sCommissionRate) {
        return spuService.updateCommission(id, commissionRate, sCommissionRate, CommonConstant.ADMIN_STOREID);
}


    /**
     * 批量设置佣金
     *
     * @param ids             商品id数组
     * @param commissionRate  佣金比例
     * @param sCommissionRate 二级佣金比例
     * @return 成功返回1 失败返回0
     */
    @PutMapping("/commission/batch")
    @ApiOperation(value = "批量设置佣金", notes = "批量设置佣金（需要认证）")

    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "父级id"),
            @ApiImplicitParam(paramType = "form", dataType = "BigDecimal", name = "commissionRate", value = "佣金比例"),
            @ApiImplicitParam(paramType = "form", dataType = "BigDecimal", name = "sCommissionRate", value = "二级佣金比例"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回>0 失败返回0", response = Integer.class)
    })
    public int updateCommissions(Long[] ids, BigDecimal commissionRate, BigDecimal sCommissionRate) {
        return spuService.updateCommissions(Arrays.asList(ids), commissionRate, sCommissionRate, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 根据商品id查询商品佣金信息
     *
     * @param id 商品id
     * @return 返回商品详细信息
     */
    @GetMapping("/commission/spu/{id}")
    @ApiOperation(value = "根据商品id查询商品佣金信息", notes = "根据商品id查询商品佣金信息（需要认证）")

    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "id", value = "商品id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回商品详细信息", response = PmsGoods.class)
    })
    public PmsGoods querySpuById(@PathVariable long id) {
        return spuService.querySpu(id, CommonConstant.QUERY_WITH_NO_STORE);
    }


}
