package com.ruoyi.web.controller.marketing;


import com.ruoyi.marketing.domain.GroupMarketing;
import com.ruoyi.marketing.domain.GroupMarketingShow;
import com.ruoyi.marketing.domain.MarketingCate;
import com.ruoyi.marketing.service.GroupMarketingServiceApi;
import com.ruoyi.marketing.service.GroupMarketingShowService;
import com.ruoyi.marketing.service.GroupMarketingShowServiceApi;
import com.ruoyi.marketing.service.MarketingCateService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 拼团活动控制器
 *
 * @author 魔金商城 created on 2020/5/28
 */
@RestController
@Api(description = "拼团活动接口")
public class GroupMarketingShowController {

    /**
     * 注入拼团活动服务接口
     */
    @Autowired
    private GroupMarketingShowService groupMarketingShowService;

    /**
     * 注入促销分类服务接口
     */
    @Autowired
    private MarketingCateService marketingCateService;

    /**
     * 注入拼团促销聚合服务接口
     */
    @Autowired
    private GroupMarketingServiceApi groupMarketingServiceApi;

    /**
     * 注入拼团活动聚合服务接口
     */
    @Autowired
    private GroupMarketingShowServiceApi groupMarketingShowServiceApi;


    /**
     * 新增拼团活动
     *
     * @param groupMarketingShowList 拼团活动列表
     * @return 成功>0 否则失败 -1 存在重复添加
     */
    @PostMapping("/groupmarketingshow")
    @ApiOperation(value = "新增拼团活动", notes = "新增拼团活动（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "list", name = "groupMarketingShowList", value = "拼团活动列表"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功>0 否则失败 -1 存在重复添加", response = Integer.class)
    })
    public int addGroupMarketingShow(@RequestBody List<GroupMarketingShow> groupMarketingShowList) {
        return groupMarketingShowService.addGroupMarketingShows(groupMarketingShowList, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 批量删除拼团活动
     *
     * @param ids 拼团活动id数组
     * @return 成功>0 否则失败
     */
    @DeleteMapping("/groupmarketingshow")
    @ApiOperation(value = "批量删除拼团活动", notes = "批量删除拼团活动（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "拼团活动id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 否则失败", response = Integer.class)
    })
    public int deleteGroupMarketingShowByIds(Long[] ids) {
        return groupMarketingShowService.deleteGroupMarketingShows(ids, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 查询促销分类列表
     *
     * @return 返回促销分类列表
     */
    @GetMapping("/groupmarketingshow/marketingcatelist")
    @ApiOperation(value = "查询促销分类列表", notes = "查询促销分类列表（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "促销分类列表", response = MarketingCate.class)
    })
    public List<MarketingCate> queryMarketingCatesByType() {
        return marketingCateService.queryMarketingCatesByTypeAndStoreId(CommonConstant.GROUP_MARKETING_CATE, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 修改拼团活动
     *
     * @param id     拼团活动id
     * @param cateId 促销分类id
     * @param sort   排序
     * @return 成功1 否则失败
     */
    @PutMapping("/groupmarketingshow/{id}/{cateId}/{sort}")
    @ApiOperation(value = "修改拼团活动", notes = "修改拼团活动（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int updateGroupMarketingShow(@PathVariable long id, @PathVariable long cateId, @PathVariable int sort) {
        return groupMarketingShowService.updateGroupMarketingShow(id, cateId, sort, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 分页查询拼团促销列表
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回拼团促销列表
     */
    @GetMapping("/groupmarketingshow/groupmarketinglist")
    @ApiOperation(value = "分页查询拼团促销列表", notes = "分页查询拼团促销列表")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回拼团促销列表", response = GroupMarketing.class)
    })
    public BaseResponse queryGroupMarketingList(@ApiIgnore PageHelper<GroupMarketing> pageHelper, String name, String skuNo) {
        return BaseResponse.build(groupMarketingServiceApi.queryGroupMarketingList(pageHelper, name, skuNo, CommonConstant.ADMIN_STOREID));
    }

    /**
     * 分页查询拼团活动列表
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回拼团活动列表
     */
    @GetMapping("/groupmarketingshowlist")
    @ApiOperation(value = "分页查询拼团活动列表", notes = "分页查询拼团活动列表")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回拼团活动列表", response = GroupMarketingShow.class)
    })
    public BaseResponse queryGroupMarketingShowList(@ApiIgnore PageHelper<GroupMarketingShow> pageHelper, String name, String skuNo) {
        return BaseResponse.build(groupMarketingShowServiceApi.queryGroupMarketingShowList(pageHelper, name, skuNo, CommonConstant.ADMIN_STOREID));
    }

}
