package com.ruoyi.web.controller.pointsmall;


import com.ruoyi.integral.domain.PointMallOrder;
import com.ruoyi.integral.domain.QueryCriteria;
import com.ruoyi.integral.service.IPointMallOrderService;
import com.ruoyi.integral.service.PointMallOrderService;
import com.ruoyi.order.domain.OmsLogisticsCompany;
import com.ruoyi.order.service.IOmsLogisticsCompanyService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.CommonResponse;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 积分商城订单控制器
 *
 * @author 魔金商城 created on 2019/8/1
 */
@RestController
@Api(description = "积分商城订单接口")
public class PointOrderController {

    /**
     * 注入积分商城订单服务
     */
    @Autowired
    private PointMallOrderService pointMallOrderService;

    /**
     * 注入积分商城订单服务聚合接口
     */
    @Autowired
    private IPointMallOrderService IPointMallOrderService;

    /**
     * 注入物流公司服务接口
     */
    @Autowired
    private IOmsLogisticsCompanyService logisticsCompanyService;


    /**
     * 分页查询积分商城订单
     *
     * @param pageHelper    分页帮助类
     * @param queryCriteria 查询参数
     * @return 积分商城订单列表
     */
    @GetMapping("/pointorderlist")
    @ApiOperation(value = "分页查询积分商城订单", notes = "分页查询积分商城订单（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointorder/querypointorderlist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "code", value = "订单号"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "receiver", value = "收货人"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "mobile", value = "手机号"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "status", value = " 0 待发货，1 已发货，待用户收货 2 用户已收货，订单完成"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回用户消费额", response = PointMallOrder.class)
    })
    public BaseResponse queryPointOrderList(@ApiIgnore PageHelper<PointMallOrder> pageHelper, @ApiIgnore QueryCriteria queryCriteria) {
        return BaseResponse.build(pointMallOrderService.queryPointMallOrders(pageHelper, queryCriteria));
    }

    /**
     * 查询所有物流公司
     *
     * @return 所有物流公司列表
     */
    @GetMapping("/alllogisticscompanys")
    @ApiOperation(value = "查询所有物流公司", notes = "查询所有物流公司（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointorder/queryalllogisticscompanys')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "所有物流公司列表", response = OmsLogisticsCompany.class)
    })
    public List<OmsLogisticsCompany> queryAllLogisticsCompanys() {
        return logisticsCompanyService.selectOmsLogisticsCompanyList(new OmsLogisticsCompany());
    }

    /**
     * 积分商城订单发货
     *
     * @param deliverRequest 积分商城订单发货请求实体类
     * @return 成功返回1 失败返回0 -1:没有运单号 -2:运单号中包含中文 -6 物流公司不存在
     */
    @PutMapping("/deliverpointorder")
    @ApiOperation(value = "积分商城订单发货", notes = "积分商城订单发货（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointorder/deliverpointorder')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0 -1:没有运单号 -2:运单号中包含中文 -6 物流公司不存在", response = Integer.class)
    })
    public int deliverPointOrder(@RequestBody DeliverRequest deliverRequest) {
        return IPointMallOrderService.deliverPointMallOrder(deliverRequest.getId(), deliverRequest.getLogisticsId(), deliverRequest.getLogisticsCode());
    }

    /**
     * 根据id查询积分商城订单详情
     *
     * @param id 订单id
     * @return 订单详情
     */
    @GetMapping("/pointorder/{id}")
    @ApiOperation(value = "根据id查询积分商城订单详情", notes = "根据id查询积分商城订单详情（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointorder/querypointorderbyid')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "id", value = "订单id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "订单详情", response = PointMallOrder.class)
    })
    public CommonResponse<PointMallOrder> queryPointOrderById(@PathVariable long id) {
        return CommonResponse.build(IPointMallOrderService.queryPointMallOrderDetailById(id, null));
    }


    /**
     * 积分商城订单发货请求实体类
     */
    @Data
    @ApiModel(description = "积分商城订单发货请求实体类")
    private static class DeliverRequest {

        /**
         * 积分商城订单id
         */
        @ApiModelProperty(value = "积分商城订单id")
        private long id;

        /**
         * 物流公司id
         */
        @ApiModelProperty(value = "物流公司id")
        private long logisticsId;

        /**
         * 运单号
         */
        @ApiModelProperty(value = "运单号")
        private String logisticsCode;

    }

}
