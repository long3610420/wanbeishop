package com.ruoyi.web.controller.order;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.bean.RefundResponse;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.order.domain.OmsBackOrder;
import com.ruoyi.order.domain.OmsOrder;
import com.ruoyi.order.service.IOmsBackOrderService;
import com.ruoyi.order.service.IOrderApiService;
import com.ruoyi.order.vo.BackOrderItem;
import com.ruoyi.order.vo.OrderItem;
import com.ruoyi.util.CommonConstant;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * 退单退款Controller
 *
 * @author 魔金商城
 * @date 2020-07-24
 */
@RestController
@RequestMapping("/order/OmsBackOrder")
public class OmsBackOrderController extends BaseController {
    @Autowired
    private IOmsBackOrderService omsBackOrderService;
    /**
     * 订单服务接口
     */
    @Autowired
    private IOrderApiService orderServiceApi;

    /**
     * 查询退单退款列表
     */
    @PreAuthorize("@ss.hasPermi('order:OmsBackOrder:list')")
    @GetMapping("/list")
    public TableDataInfo list(OmsBackOrder omsBackOrder) {
        startPage();
        List<OmsBackOrder> list = omsBackOrderService.selectOmsBackOrderList(omsBackOrder);
        return getDataTable(list);
    }

    /**
     * 导出退单退款列表
     */
    @PreAuthorize("@ss.hasPermi('order:OmsBackOrder:export')")
    @Log(title = "退单退款", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OmsBackOrder omsBackOrder) {
        List<OmsBackOrder> list = omsBackOrderService.selectOmsBackOrderList(omsBackOrder);
        ExcelUtil<OmsBackOrder> util = new ExcelUtil<OmsBackOrder>(OmsBackOrder.class);
        return util.exportExcel(list, "OmsBackOrder");
    }

    /**
     * 获取退单退款详细信息
     */
    @PreAuthorize("@ss.hasPermi('order:OmsBackOrder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(omsBackOrderService.selectOmsBackOrderById(id));
    }

    /**
     * 新增退单退款
     */
    @PreAuthorize("@ss.hasPermi('order:OmsBackOrder:add')")
    @Log(title = "退单退款", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OmsBackOrder omsBackOrder) {
        return toAjax(omsBackOrderService.insertOmsBackOrder(omsBackOrder));
    }

    /**
     * 修改退单退款
     */
    @PreAuthorize("@ss.hasPermi('order:OmsBackOrder:edit')")
    @Log(title = "退单退款", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OmsBackOrder omsBackOrder) {
        return toAjax(omsBackOrderService.updateOmsBackOrder(omsBackOrder));
    }

    /**
     * 删除退单退款
     */
    @PreAuthorize("@ss.hasPermi('order:OmsBackOrder:remove')")
    @Log(title = "退单退款", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(omsBackOrderService.deleteOmsBackOrderByIds(ids));
    }
    /**
     * 退款
     *
     * @param backOrderId 退单id
     * @return 返回退款结果
     */
    @PutMapping("agreetorefund/releasemoney/{backOrderId}")
    @ApiOperation(value = "退款发放金额", notes = "发放金额（需要认证）")
    public RefundResponse releaseMoney(@PathVariable long backOrderId) {
        return omsBackOrderService.releaseMoney(backOrderId);
    }

    /**
     * 根据订单id查询订单信息  (订单的所有信息 此接口慎用)
     *
     * @param id 订单id
     * @return 返回订单信息
     */
    @GetMapping("/backorder/order/{id}")
    @ApiOperation(value = "根据订单id查询订单信息  (订单的所有信息 此接口慎用)", notes = "根据订单id查询订单信息  (订单的所有信息 此接口慎用)（需要认证）")
    public OmsOrder queryOrderById(@PathVariable long id) {
        return orderServiceApi.queryOrderDetailById(id, CommonConstant.QUERY_WITH_NO_STORE, CommonConstant.QUERY_WITH_NO_CUSTOMER, OrderItem.LOG, OrderItem.ATTR,  OrderItem.SKUS);
    }
    /**
     * 根据退款id查询退款信息(包含退单的商品, 和操作日志 此接口调用慎重)
     *
     * @param backOrderId 退单id
     * @return 返回退单信息
     */
    @GetMapping("/backorder/refund/{backOrderId}")
    public OmsBackOrder queryBackOrderById(@PathVariable long backOrderId) {
        return omsBackOrderService.queryBackOrderById(backOrderId, CommonConstant.QUERY_WITH_NO_STORE, CommonConstant.QUERY_WITH_NO_CUSTOMER, BackOrderItem.LOG, BackOrderItem.SKUS);
    }

    /**
     * 同意退款
     *
     * @param backOrderId 退单id
     * @param message     留言
     * @return 成功返回1  失败返回0  当前退单状态不对 返回-1
     */
    @PutMapping("/agreetorefund/{backOrderId}")
    public int agreeToRefund(@PathVariable long backOrderId, String message) {
        return omsBackOrderService.agreeToRefund(backOrderId, CommonConstant.ADMIN_STOREID, message);
    }

    /**
     * 拒绝退款
     *
     * @param backOrderId 退单id
     * @param message     留言
     * @return 成功返回1  失败返回0  当前退单状态不对 返回-1
     */
    @PutMapping("/refuserefund/{backOrderId}")
    public int refuseRefund(@PathVariable long backOrderId, String message) {
        return omsBackOrderService.refuseRefund(backOrderId, CommonConstant.ADMIN_STOREID, message);
    }

    /**
     * 根据退单id查询退货信息(包含退单的商品, 和操作日志 此接口调用慎重)
     *
     * @param backOrderId 退单id
     * @return 返回退货信息
     */
    @GetMapping("/backorder/return/{backOrderId}")
    @ApiOperation(value = "根据退单id查询退货信息(包含退单的商品, 和操作日志 此接口调用慎重)", notes = "根据退单id查询退货信息(包含退单的商品, 和操作日志 此接口调用慎重)（需要认证）")
    public OmsBackOrder queryBackOrderForReturnById(@PathVariable long backOrderId) {
        return omsBackOrderService.queryBackOrderById(backOrderId, CommonConstant.QUERY_WITH_NO_STORE, CommonConstant.QUERY_WITH_NO_CUSTOMER, BackOrderItem.LOG, BackOrderItem.SKUS);
    }


    /**
     * 同意退货
     *
     * @param backOrderId 退单id
     * @param message     留言
     * @return 成功返回1  失败返回0  当前退单状态不对 返回-1
     */
    @PutMapping("/agreetoreturn/{backOrderId}")
    @ApiOperation(value = "同意退货", notes = "同意退货（需要认证）")
    public int agreeToReturn(@PathVariable long backOrderId, String message) {
        return omsBackOrderService.agreeToReturn(backOrderId, CommonConstant.ADMIN_STOREID, message);
    }

    /**
     * 拒绝退货
     *
     * @param backOrderId 退单id
     * @param message     留言
     * @return 成功返回1  失败返回0  当前退单状态不对 返回-1
     */
    @PutMapping("/refusereturn/{backOrderId}")
    @ApiOperation(value = "拒绝退货", notes = "拒绝退货（需要认证）")
    public int refuseReturn(@PathVariable long backOrderId, String message) {
        return omsBackOrderService.refuseReturn(backOrderId, CommonConstant.ADMIN_STOREID, message);
    }

    /**
     * 同意确认退货
     *
     * @param backOrderId 退单id
     * @param message     留言
     * @param money       实际退款
     * @return 成功返回1  失败返回0  当前退单状态不对 返回-1
     */
    @PutMapping("/confirmreturn/{backOrderId}")
    @ApiOperation(value = "同意确认退货", notes = "同意确认退货（需要认证）")
    public int confirmReturn(@PathVariable long backOrderId, String message, BigDecimal money) {
        return omsBackOrderService.confirmReturn(backOrderId, CommonConstant.ADMIN_STOREID, message, money);
    }

    /**
     * 拒绝收货
     *
     * @param backOrderId 退单id
     * @param message     留言
     * @return 成功返回1  失败返回0  当前退单状态不对 返回-1
     */
    @PutMapping("/refusetoreceive/{backOrderId}")
    @ApiOperation(value = "拒绝收货", notes = "拒绝收货（需要认证）")
    public int refuseToReceive(@PathVariable long backOrderId, String message) {
        return omsBackOrderService.refuseToReceive(backOrderId, CommonConstant.ADMIN_STOREID, message);
    }

}
