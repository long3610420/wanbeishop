package com.ruoyi.web.controller.marketing;


import com.ruoyi.marketing.domain.MarketingCate;
import com.ruoyi.marketing.domain.TryMarketing;
import com.ruoyi.marketing.domain.TryMarketingShow;
import com.ruoyi.marketing.service.MarketingCateService;
import com.ruoyi.marketing.service.TryMarketingServiceApi;
import com.ruoyi.marketing.service.TryMarketingShowService;
import com.ruoyi.marketing.service.TryMarketingShowServiceApi;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 试用活动控制器
 *
 * @author 魔金商城 created on 2020/6/9
 */
@RestController
@Api(description = "试用活动接口")
public class TryMarketingShowController {

    /**
     * 注入试用活动服务接口
     */
    @Autowired
    private TryMarketingShowService tryMarketingShowService;

    /**
     * 注入促销分类服务接口
     */
    @Autowired
    private MarketingCateService marketingCateService;

    /**
     * 注入试用促销聚合服务接口
     */
    @Autowired
    private TryMarketingServiceApi tryMarketingServiceApi;

    /**
     * 注入试用活动聚合服务接口
     */
    @Autowired
    private TryMarketingShowServiceApi tryMarketingShowServiceApi;


    /**
     * 新增试用活动
     *
     * @param tryMarketingShowList 试用活动列表
     * @return 成功>0 否则失败 -1 存在重复添加
     */
    @PostMapping("/trymarketingshow")
    @ApiOperation(value = "新增试用活动", notes = "新增试用活动（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "list", name = "tryMarketingShowList", value = "试用活动列表"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功>0 否则失败 -1 存在重复添加", response = Integer.class)
    })
    public int addTryMarketingShow(@RequestBody List<TryMarketingShow> tryMarketingShowList) {
        return tryMarketingShowService.addTryMarketingShows(tryMarketingShowList, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 批量删除试用活动
     *
     * @param ids 试用活动id数组
     * @return 成功>0 否则失败
     */
    @DeleteMapping("/trymarketingshow")
    @ApiOperation(value = "批量删除试用活动", notes = "批量删除试用活动（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "试用活动id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 否则失败", response = Integer.class)
    })
    public int deleteTryMarketingShowByIds(Long[] ids) {
        return tryMarketingShowService.deleteTryMarketingShows(ids, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 查询促销分类列表
     *
     * @return 返回促销分类列表
     */
    @GetMapping("/trymarketingshow/marketingcatelist")
    @ApiOperation(value = "查询促销分类列表", notes = "查询促销分类列表（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "促销分类列表", response = MarketingCate.class)
    })
    public List<MarketingCate> queryMarketingCatesByType() {
        return marketingCateService.queryMarketingCatesByTypeAndStoreId(CommonConstant.TRY_MARKETING_CATE, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 修改试用活动
     *
     * @param id     试用活动id
     * @param cateId 促销分类id
     * @param sort   排序
     * @return 成功1 否则失败
     */
    @PutMapping("/trymarketingshow/{id}/{cateId}/{sort}")
    @ApiOperation(value = "修改试用活动", notes = "修改试用活动（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int updateTryMarketingShow(@PathVariable long id, @PathVariable long cateId, @PathVariable int sort) {
        return tryMarketingShowService.updateTryMarketingShow(id, cateId, sort, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 分页查询试用促销列表
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回试用促销列表
     */
    @GetMapping("/trymarketingshow/trymarketinglist")
    @ApiOperation(value = "分页查询试用促销列表", notes = "分页查询试用促销列表")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回试用促销列表", response = TryMarketing.class)
    })
    public BaseResponse queryTryMarketingList(@ApiIgnore PageHelper<TryMarketing> pageHelper, String name, String skuNo) {
        return BaseResponse.build(tryMarketingServiceApi.queryTryMarketingList(pageHelper, name, skuNo, CommonConstant.ADMIN_STOREID));
    }

    /**
     * 分页查询试用活动列表
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回试用活动列表
     */
    @GetMapping("/trymarketingshowlist")
    @ApiOperation(value = "分页查询试用活动列表", notes = "分页查询试用活动列表")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回试用活动列表", response = TryMarketingShow.class)
    })
    public BaseResponse queryTryMarketingShowList(@ApiIgnore PageHelper<TryMarketingShow> pageHelper, String name, String skuNo) {
        return BaseResponse.build(tryMarketingShowServiceApi.queryTryMarketingShowList(pageHelper, name, skuNo, CommonConstant.ADMIN_STOREID));
    }

}
