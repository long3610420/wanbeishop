package com.ruoyi.web.controller.pointsmall;


import com.ruoyi.integral.domain.PointCate;
import com.ruoyi.integral.service.PointCateService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 积分商城分类控制器
 *
 * @author 魔金商城 created on 2019/7/26
 */
@RestController
@Api(description = "积分商城分类接口")
public class PointCateController {

    /**
     * 注入积分商城分类服务接口
     */
    @Autowired
    private PointCateService pointCateService;


    /**
     * 分页查询积分商城分类
     *
     * @param pageHelper 分页帮助类
     * @param name       分类名称
     * @return 积分商城分类集合
     */
    @GetMapping("/pointcates")
    @ApiOperation(value = "分页查询积分商城分类", notes = "分页查询积分商城分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointcate/querypointcates')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "分类名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "积分商城分类集合", response = PointCate.class)
    })
    public BaseResponse queryPointCate(@ApiIgnore PageHelper<PointCate> pageHelper, String name) {
        return BaseResponse.build(pointCateService.queryPointCates(pageHelper, name));
    }

    /**
     * 新增积分商城分类
     *
     * @param pointCate 积分商城分类实体
     * @return 成功1 否则失败
     */
    @PostMapping("/pointcate")
    @ApiOperation(value = "新增积分商城分类", notes = "新增积分商城分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointcate/addpointcate')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int addPointCate(@RequestBody PointCate pointCate) {
        return pointCateService.addPointCate(pointCate);
    }

    /**
     * 根据id查询积分商城分类
     *
     * @param id 积分商城分类id
     * @return 积分商城分类信息
     */
    @GetMapping("/pointcate/{id}")
    @ApiOperation(value = "根据id查询积分商城分类", notes = "根据id查询积分商城分类")
    @PreAuthorize("@ss.hasPermi('pointcate/querypointcatebyid')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "积分商城分类id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "积分商城分类信息", response = PointCate.class)
    })
    public PointCate queryPointCateById(@PathVariable long id) {
        return pointCateService.queryPointCateById(id);
    }

    /**
     * 修改积分商城分类
     *
     * @param pointCate 积分商城分类实体
     * @return 成功1 否则失败
     */
    @PutMapping("/pointcate")
    @ApiOperation(value = "修改积分商城分类", notes = "修改积分商城分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointcate/updatepointcate')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int updatePointCate(@RequestBody PointCate pointCate) {
        return pointCateService.updatePointCate(pointCate);
    }

    /**
     * 删除积分商城分类
     *
     * @param ids 积分商城分类id数组
     * @return 成功>0 否则失败
     */
    @DeleteMapping("/pointcate")
    @ApiOperation(value = "删除积分商城分类", notes = "删除积分商城分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointcate/deletepointcate')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "积分商城分类id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功>0 否则失败", response = Integer.class)
    })
    public int deletePointCates(long[] ids) {
        return pointCateService.deletePointCates(ids);
    }

}
