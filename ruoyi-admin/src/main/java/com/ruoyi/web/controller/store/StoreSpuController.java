package com.ruoyi.web.controller.store;


import com.ruoyi.goods.domain.PmsCategory;
import com.ruoyi.goods.domain.PmsGoods;
import com.ruoyi.goods.domain.PmsServiceSupport;
import com.ruoyi.goods.service.IPmsCategoryService;
import com.ruoyi.goods.service.IPmsGoodsService;
import com.ruoyi.goods.service.IPmsServiceSupportService;
import com.ruoyi.goods.service.ISpuApiService;
import com.ruoyi.goods.vo.SpuSearchCondition;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

/**
 * @author 魔金商城
 * @date 2019-06-27 13:17
 * <p>
 * 店铺商品控制器
 */
@RestController
@Api(description = "店铺商品接口")
public class StoreSpuController {


    /**
     * 注入商品服务接口
     */
    @Autowired
    private IPmsGoodsService spuService;

    /**
     * 注入分类服务接口
     */
    @Autowired
    private IPmsCategoryService categoryService;



    /**
     * 注入商品聚合服务
     */
    @Autowired
    private ISpuApiService ISpuApiService;

    /**
     * 注入服务支持服务接口
     */
    @Autowired
    private IPmsServiceSupportService serviceSupportService;


    /**
     * 分页查询所有店铺的商品信息
     *
     * @param spuSearchCondition 搜索条件
     * @return 返回所有店铺的商品信息
     */
    @GetMapping("/storespu")
    @ApiOperation(value = "分页查询所有店铺的商品信息", notes = "分页查询所有店铺的商品信息（需要认证）")

    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "id", value = "商品编号"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "商品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "shelvesStatus", value = "商品上下架状态"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "isVirtual", value = "是否虚拟商品"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "storeName", value = "店铺名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "secondCateId", value = "二级分类id"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "thirdCateId", value = "三级分类id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回所有店铺的商品信息", response = PmsGoods.class)
    })
    public BaseResponse queryAllStoreSpus(@ApiIgnore PageHelper<PmsGoods> pageHelper, @ApiIgnore SpuSearchCondition spuSearchCondition) {
        return BaseResponse.build(spuService.queryAllStoreSpus(pageHelper, spuSearchCondition));
    }


    /**
     * 根据分类id查询所有子分类信息
     *
     * @param parentId 父级id
     * @return 返回该父级下的所有分类信息
     */
    @GetMapping("/storespu/category/{parentId}")
    @ApiOperation(value = "根据分类id查询所有子分类信息", notes = "根据分类id查询所有子分类信息（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "parentId", value = "父级id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回该父级下的所有分类信息", response = PmsCategory.class)
    })
    public List<PmsCategory> queryCategoryByParentId(@PathVariable long parentId) {
        return categoryService.queryCategoryByParentId(parentId);
    }


    /**
     * 违规下架
     *
     * @param ids 商品id
     * @return 成功返回>1 失败返回0
     */
    @PutMapping("/storespu")
    @ApiOperation(value = "违规下架", notes = "违规下架（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "商品id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回>1 失败返回0", response = Integer.class)
    })
    public int updateShelvesStatusIllegal(Long[] ids) {
        return spuService.updateShelvesStatus(Arrays.asList(ids), "2", CommonConstant.QUERY_WITH_NO_STORE, null);
    }

    /**
     * 导出所有店铺商品信息
     *
     * @throws IOException io异常
     */
    @PostMapping("/storespu/all")
    @ApiOperation(value = "导出所有店铺商品信息", notes = "导出所有店铺商品信息（需要认证）")
    public void exportStoreAllSpu(HttpServletResponse response) throws IOException {
        OutputStream os = response.getOutputStream();
     //   ExcelUtils.exportExcel(response, String.valueOf("店铺商品信息_" + System.currentTimeMillis()).concat(".xls"), () -> spuServiceApi.exportAllSpu(os, CommonConstant.QUERY_WITH_NO_STORE));
    }


    /**
     * 导出选中的店铺商品信息
     *
     * @param ids 店铺商品id 数组
     * @throws IOException io异常
     */
    @PostMapping("/storespu/checked")
    @ApiOperation(value = "导出选中的店铺商品信息", notes = "导出选中的店铺商品信息（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "店铺商品id 数组"),
    })
    public void exportStoreCheckedSpu(HttpServletResponse response, Long... ids) throws IOException {
        OutputStream os = response.getOutputStream();
       /* ExcelUtils.exportExcel(response, String.valueOf("店铺商品信息_" + System.currentTimeMillis()).concat(".xls"), () ->
                spuServiceApi.exportCheckedSpu(os, ids, CommonConstant.QUERY_WITH_NO_STORE)
        );*/
    }

    /**
     * 根据商品id查询商品信息
     *
     * @param id 商品id
     * @return 返回商品详细信息(包含 商品服务支持, 商品图片, 商品属性值, 商品规格值, 商品单品......)
     */
    @GetMapping("/store/spu/{id}")
    @ApiOperation(value = "根据商品id查询商品信息", notes = "根据商品id查询商品信息（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "商品id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回商品详细信息", response = PmsGoods.class)
    })
    public PmsGoods querySpuByIdForStore(@PathVariable long id) {
        return spuService.querySpu(id, CommonConstant.QUERY_WITH_NO_STORE);
    }


    /**
     * 查询所有服务支持
     *
     * @return 返回所有服务支持
     */
    @GetMapping("/store/spu/servicesupport")
    @ApiOperation(value = "查询所有服务支持", notes = "查询所有服务支持（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回所有服务支持", response = PmsServiceSupport.class)
    })
    public List<PmsServiceSupport> queryAllServiceSupportForstore() {
        return serviceSupportService.queryAllServiceSupport();
    }

}
