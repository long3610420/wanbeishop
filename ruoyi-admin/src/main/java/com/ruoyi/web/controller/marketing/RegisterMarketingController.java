package com.ruoyi.web.controller.marketing;


import com.ruoyi.marketing.domain.RegisterMarketing;
import com.ruoyi.marketing.service.RegisterMarketingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 注册营销设置控制器
 * Created by 魔金商城平台管理on 2019/7/4.
 */
@RestController
@Api(description = "注册营销设置接口")
public class RegisterMarketingController {

    /**
     * 自动注入注册营销service
     */
    @Autowired
    private RegisterMarketingService registerMarketingService;


    /**
     * 查找注册营销
     *
     * @return 返回注册营销
     */
    @GetMapping("/registermarketing")
    @ApiOperation(value = "查找注册营销", notes = "查找注册营销（需要认证）")
    @PreAuthorize("@ss.hasPermi('registermarketing')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回注册营销", response = RegisterMarketing.class)
    })
    public RegisterMarketing queryRegisterMarketing() {
        return registerMarketingService.queryRegisterMarketing();
    }

    /**
     * 更新注册营销
     *
     * @param registerMarketing 注册营销
     * @return 成功返回1 失败返回0
     */
    @PutMapping("/registermarketing")
    @ApiOperation(value = "更新注册营销", notes = "更新注册营销（需要认证）")
    @PreAuthorize("@ss.hasPermi('registermarketing')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int updateRegisterMarketing(@RequestBody RegisterMarketing registerMarketing) {
        return registerMarketingService.updateRegisterMarketing(registerMarketing);
    }

}
