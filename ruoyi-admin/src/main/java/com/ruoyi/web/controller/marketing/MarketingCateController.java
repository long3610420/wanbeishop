package com.ruoyi.web.controller.marketing;


import com.ruoyi.marketing.domain.MarketingCate;
import com.ruoyi.marketing.service.MarketingCateService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 促销分类控制器
 *
 * @author 魔金商城 created on 2019/5/24
 */
@RestController
@Api(description = "促销分类接口")
public class MarketingCateController {

    /**
     * 注入促销分类服务接口
     */
    @Autowired
    private MarketingCateService marketingCateService;


    /**
     * 分页查询促销分类
     *
     * @param pageHelper 分页帮助类
     * @param name       促销分类名称
     * @param type       促销分类类型 1 预售 2 拼团 3 试用 4 抢购 5 众筹
     * @return 促销分类列表
     */
    @GetMapping("/marketingcates")
    @ApiOperation(value = "分页查询促销分类", notes = "分页查询促销分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('marketingcate')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "促销分类名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "type", value = "促销分类类型"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "促销分类列表", response = MarketingCate.class)
    })
    public BaseResponse queryMarketingCates(@ApiIgnore PageHelper<MarketingCate> pageHelper, String name, String type) {
        return BaseResponse.build(marketingCateService.queryMarketingCates(pageHelper, name, type, CommonConstant.QUERY_WITH_NO_STORE));
    }

    /**
     * 新增促销分类
     *
     * @param marketingCate 促销分类信息
     * @return 成功1 否则失败
     */
    @PostMapping("/marketingcate")
    @ApiOperation(value = "新增促销分类", notes = "新增促销分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('marketingcate')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int addMarketingCate(@RequestBody MarketingCate marketingCate) {
        marketingCate.setStoreId(CommonConstant.ADMIN_STOREID);
        return marketingCateService.addMarketingCate(marketingCate);
    }

    /**
     * 根据id查询促销分类
     *
     * @param id 促销分类id
     * @return 促销分类信息
     */
    @GetMapping("/marketingcate/{id}")
    @ApiOperation(value = "根据id查询促销分类", notes = "根据id查询促销分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('marketingcate')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销分类id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "促销分类信息", response = MarketingCate.class)
    })
    public MarketingCate queryMarketingCateById(@PathVariable long id) {
        return marketingCateService.queryMarketingCateById(id, CommonConstant.QUERY_WITH_NO_STORE);
    }

    /**
     * 修改促销分类
     *
     * @param marketingCate 促销分类信息
     * @return 成功1 否则失败
     */
    @PutMapping("/marketingcate")
    @ApiOperation(value = "修改促销分类", notes = "修改促销分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('marketingcate')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int updateMarketingCate(@RequestBody MarketingCate marketingCate) {
        marketingCate.setStoreId(CommonConstant.QUERY_WITH_NO_STORE);
        return marketingCateService.updateMarketingCate(marketingCate);
    }

    /**
     * 删除促销分类
     *
     * @param ids 促销分类id数组
     * @return 成功>0 否则失败
     */
    @DeleteMapping("/marketingcate")
    @ApiOperation(value = "删除促销分类", notes = "删除促销分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('marketingcate')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "促销分类id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功>0 否则失败", response = Integer.class)
    })
    public int deleteMarketingCate(long[] ids) {
        return marketingCateService.deleteMarketingCates(ids, CommonConstant.QUERY_WITH_NO_STORE, "");
    }

}
