package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStoreOrder;
import com.ruoyi.store.service.ITStoreOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 门店订单Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStoreOrder")
public class TStoreOrderController extends BaseController {
    @Autowired
    private ITStoreOrderService tStoreOrderService;

    /**
     * 查询门店订单列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrder:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStoreOrder tStoreOrder) {
        startPage();
        List<TStoreOrder> list = tStoreOrderService.selectTStoreOrderList(tStoreOrder);
        return getDataTable(list);
    }

    /**
     * 导出门店订单列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrder:export')")
    @Log(title = "门店订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStoreOrder tStoreOrder) {
        List<TStoreOrder> list = tStoreOrderService.selectTStoreOrderList(tStoreOrder);
        ExcelUtil<TStoreOrder> util = new ExcelUtil<TStoreOrder>(TStoreOrder.class);
        return util.exportExcel(list, "TStoreOrder");
    }

    /**
     * 获取门店订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStoreOrderService.selectTStoreOrderById(id));
    }

    /**
     * 新增门店订单
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrder:add')")
    @Log(title = "门店订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStoreOrder tStoreOrder) {
        return toAjax(tStoreOrderService.insertTStoreOrder(tStoreOrder));
    }

    /**
     * 修改门店订单
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrder:edit')")
    @Log(title = "门店订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStoreOrder tStoreOrder) {
        return toAjax(tStoreOrderService.updateTStoreOrder(tStoreOrder));
    }

    /**
     * 删除门店订单
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrder:remove')")
    @Log(title = "门店订单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStoreOrderService.deleteTStoreOrderByIds(ids));
    }
}
