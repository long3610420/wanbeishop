package com.ruoyi.web.controller.promotion;


import com.ruoyi.common.utils.bean.WithdrawResponse;
import com.ruoyi.member.domain.UmsWithdraw;
import com.ruoyi.member.service.IUmsWithdrawService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author 魔金商城
 * @date 2019-06-18 20:20
 * <p>
 * 提现控制器
 */
@RestController
@Api(description = "提现接口")
public class WithdrawController {


    /**
     * 注入提现记录服务
     */
    @Autowired
    private IUmsWithdrawService withdrawRecordService;


    /**
     * 分页查询提现记录
     *
     * @param pageHelper    分页帮助类
     * @param queryCriteria 查询参数
     * @return 提现记录
     */
    @GetMapping("/withdrawrecords")
    @ApiOperation(value = "分页查询提现记录", notes = "分页查询提现记录（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "tradeNo", value = "交易流水号"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "mobile", value = "手机号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "提现记录", response = UmsWithdraw.class)
    })
    public BaseResponse queryWithdrawRecords(@ApiIgnore PageHelper<UmsWithdraw> pageHelper, @ApiIgnore UmsWithdraw.QueryCriteria queryCriteria) {
        return BaseResponse.build(withdrawRecordService.queryWithdrawRecordsForAdmin(pageHelper, queryCriteria));
    }


    /**
     * 审核通过
     *
     * @param id 提现申请id
     * @return 成功返回1 失败返回0
     */
    @PutMapping("/withdrawrecords/{id}")
    @ApiOperation(value = "审核通过", notes = "审核通过（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "提现申请id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int passApply(@PathVariable long id) {
        return withdrawRecordService.updateWithdrawRecordStatus(id, "1");
    }


    /**
     * 拒绝提现申请
     *
     * @param id 提现申请id
     * @return 成功返回1 失败返回0
     */
    @PutMapping("withdrawrecords/refuseapply/{id}")
    @ApiOperation(value = "拒绝提现申请", notes = "拒绝提现申请（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "提现申请id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int refuseApply(@PathVariable long id) {
        return withdrawRecordService.updateWithdrawRecordStatus(id, "2");
    }


    /**
     * 发放金额
     *
     * @param id 提现id
     * @return 返回提现结果
     */
    @PutMapping("withdrawrecords/releasemoney/{id}")
    @ApiOperation(value = "发放金额", notes = "发放金额（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "提现申请id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回提现结果", response = WithdrawResponse.class)
    })
    public WithdrawResponse releaseMoney(@PathVariable long id) {
        return withdrawRecordService.releaseMoney(id);
    }


}
