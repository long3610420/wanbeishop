package com.ruoyi.web.controller.marketing;


import com.ruoyi.integral.domain.PointSignRule;
import com.ruoyi.integral.service.PointSignRuleService;
import com.ruoyi.util.CommonResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 签到积分规则控制器
 * Created by 魔金商城平台管理on 2019/7/10.
 */
@RestController
@Api(description = "签到积分规则接口")
public class PointSignRuleController {

    /**
     * 注入签到积分规则服务
     */
    @Autowired
    private PointSignRuleService pointSignRuleService;

    /**
     * 查找签到积分规则
     */
    @GetMapping("/pointsignrule")
    @ApiOperation(value = "查找签到积分规则", notes = "查找签到积分规则（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointsignrule')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "积分规则实体", response = PointSignRule.class)
    })
    public CommonResponse<PointSignRule> queryPointSignRule() {
        return CommonResponse.build(pointSignRuleService.queryPointSignRule());
    }


    /**
     * 更新签到积分规则
     *
     * @param pointSignRule 签到积分规则实体
     * @return 1：成功，否则失败
     */
    @PutMapping("/pointsignrule")
    @ApiOperation(value = "更新签到积分规则", notes = "更新签到积分规则（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointsignrule')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "1：成功，否则失败", response = Integer.class)
    })
    public int updatePointSignRule(@RequestBody PointSignRule pointSignRule) {
        return pointSignRuleService.updatePointSignRule(pointSignRule);
    }
}
