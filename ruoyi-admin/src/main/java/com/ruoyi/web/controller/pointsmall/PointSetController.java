package com.ruoyi.web.controller.pointsmall;


import com.ruoyi.integral.domain.PointSeting;
import com.ruoyi.integral.service.PointSetingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 积分设置控制器
 *
 * @author 魔金商城 created on 2019/7/31
 */
@RestController
@Api(description = "积分设置接口")
public class PointSetController {

    /**
     * 注入积分设置服务接口
     */
    @Autowired
    private PointSetingService pointSetingService;


    /**
     * 查询积分设置
     *
     * @return 积分设置信息
     */
    @GetMapping("/pointseting")
    @ApiOperation(value = "查询积分设置", notes = "查询积分设置（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointseting/querypointseting')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "积分设置信息", response = PointSeting.class)
    })
    public PointSeting queryPointSeting() {
        return pointSetingService.queryPointSeting();
    }

    /**
     * 修改积分设置
     *
     * @param pointSeting 积分设置实体
     * @return 成功1 否则失败
     */
    @PutMapping("/pointseting")
    @ApiOperation(value = "修改积分设置", notes = "修改积分设置（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointseting/updatepointseting')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int updatePointSeting(@RequestBody PointSeting pointSeting) {
        return pointSetingService.updatePointSeting(pointSeting);
    }

}
