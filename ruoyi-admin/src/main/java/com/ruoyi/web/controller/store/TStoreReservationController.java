package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStoreReservation;
import com.ruoyi.store.service.ITStoreReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 门店预约Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStoreReservation")
public class TStoreReservationController extends BaseController {
    @Autowired
    private ITStoreReservationService tStoreReservationService;

    /**
     * 查询门店预约列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreReservation:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStoreReservation tStoreReservation) {
        startPage();
        List<TStoreReservation> list = tStoreReservationService.selectTStoreReservationList(tStoreReservation);
        return getDataTable(list);
    }

    /**
     * 导出门店预约列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreReservation:export')")
    @Log(title = "门店预约", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStoreReservation tStoreReservation) {
        List<TStoreReservation> list = tStoreReservationService.selectTStoreReservationList(tStoreReservation);
        ExcelUtil<TStoreReservation> util = new ExcelUtil<TStoreReservation>(TStoreReservation.class);
        return util.exportExcel(list, "TStoreReservation");
    }

    /**
     * 获取门店预约详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreReservation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStoreReservationService.selectTStoreReservationById(id));
    }

    /**
     * 新增门店预约
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreReservation:add')")
    @Log(title = "门店预约", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStoreReservation tStoreReservation) {
        return toAjax(tStoreReservationService.insertTStoreReservation(tStoreReservation));
    }

    /**
     * 修改门店预约
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreReservation:edit')")
    @Log(title = "门店预约", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStoreReservation tStoreReservation) {
        return toAjax(tStoreReservationService.updateTStoreReservation(tStoreReservation));
    }

    /**
     * 删除门店预约
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreReservation:remove')")
    @Log(title = "门店预约", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStoreReservationService.deleteTStoreReservationByIds(ids));
    }
}
