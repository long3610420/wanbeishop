package com.ruoyi.web.controller.marketing;


import com.ruoyi.goods.domain.PmsSku;
import com.ruoyi.goods.service.IPmsSkuService;
import com.ruoyi.marketing.domain.Coupon;
import com.ruoyi.marketing.domain.Marketing;
import com.ruoyi.marketing.domain.MarketingCate;
import com.ruoyi.marketing.domain.MarketingPic;
import com.ruoyi.marketing.service.*;
import com.ruoyi.util.*;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

/**
 * Created by 魔金商城平台管理on 2019/7/29.
 * 试用控制器
 */
@RestController
@Api("试用接口")
public class TryController {


    /**
     * 注入促销查询接口
     */
    @Autowired
    private MarketingQueryService marketingQueryService;

    /**
     * 注入促销删除服务接口
     */
    @Resource(name = "marketingDeleteService")
    private MarketingService marketingDeleteService;
    @Autowired
    private CouponService couponService;

    /**
     * 注入单品服务
     */
    @Autowired
    private IPmsSkuService skuService;


    /**
     * 注入试用促销服务接口
     */
    @Resource(name = "tryService")
    private MarketingService tryService;


    /**
     * 注入促销图片服务接口
     */
    @Autowired
    private MarketingPicService marketingPicService;


    /**
     * 注入促销分类服务接口
     */
    @Autowired
    private MarketingCateService marketingCateService;


    /**
     * 分页查询试用促销信息
     *
     * @param pageHelper 分页帮助类
     * @param name       名称
     * @return 返回促销信息
     */
    @GetMapping("/try")
    @ApiOperation(value = "分页查询试用促销信息", notes = "分页查询试用促销信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "试用促销名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "status", value = "活动状态 1 未开始 2 进行中 3 已结束"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回促销信息", response = Marketing.class)
    })
    public BaseResponse queryTryMarketList(@ApiIgnore PageHelper<Marketing> pageHelper, String name, String status) {
        return BaseResponse.build(marketingQueryService.queryMarketings(pageHelper, name, "9", CommonConstant.ADMIN_STOREID, status));
    }

    /**
     * 批量删除试用促销
     *
     * @param marketingIds 促销id数组
     * @return 成功返回1 否则失败
     */
    @DeleteMapping("/try")
    @ApiOperation(value = "批量删除试用促销", notes = "批量删除试用促销（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "marketingIds", value = "促销id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 否则失败", response = Integer.class)
    })
    public int deleteTryByIds(Long[] marketingIds) {
        return marketingDeleteService.deleteMarketings(marketingIds, CommonConstant.ADMIN_STOREID, MarketingConstant.TRY_MARKETING);
    }

    /**
     * 分页查询单品信息（试用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回单品信息(包括规格信息)
     */
    @GetMapping("/try/skus")
    @ApiOperation(value = "分页查询单品信息（试用）", notes = "分页查询单品信息（试用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForTry(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }

    /**
     * 查询交叉时间内含有相同单品的互斥促销数量（试用）
     *
     * @param marketing 促销信息
     * @return 交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品
     */
    @PostMapping("/try/exclusion")
    @ApiOperation(value = "查询交叉时间内含有相同单品的互斥促销数量（试用）", notes = "查询交叉时间内含有相同单品的互斥促销数量（试用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品", response = Integer.class)
    })
    public int queryExclusionMarketingCountForTry(@RequestBody Marketing marketing) {
        return marketingQueryService.queryExclusionMarketingCount(marketing);
    }

    /**
     * 添加试用促销
     *
     * @param marketing 试用促销
     * @return 成功返回1 失败返回0
     */
    @PostMapping("/try")
    @ApiOperation(value = "添加试用促销", notes = "添加试用促销（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int addTryMarketing(@RequestBody Marketing marketing) {
        return tryService.addMarketing(marketing.setAddMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.TRY_MARKETING));
    }


    /**
     * 根据促销id查询试用促销信息
     *
     * @param id 促销id
     * @return 返回试用促销详情信息
     */
    @GetMapping("/try/{id}")
    @ApiOperation(value = "根据促销id查询试用促销信息", notes = "根据促销id查询试用促销信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回试用促销详情信息", response = Marketing.class)
    })
    public Marketing queryTryMarketingById(@PathVariable long id) {
        return marketingQueryService.queryMarketingById(id, CommonConstant.QUERY_WITH_NO_STORE);
    }

    /**
     * 分页查询单品信息（更新试用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回单品信息(包括规格信息)
     */
    @GetMapping("/try/update/skus")
    @ApiOperation(value = "分页查询单品信息（更新试用）", notes = "分页查询单品信息（更新试用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForTryUpdate(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }


    /**
     * 查询交叉时间内含有相同单品的互斥促销数量（更新试用）
     *
     * @param marketing 促销信息
     * @return 交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品
     */
    @PostMapping("/try/update/exclusion")
    @ApiOperation(value = "查询交叉时间内含有相同单品的互斥促销数量（更新试用）", notes = "查询交叉时间内含有相同单品的互斥促销数量（更新试用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品", response = Integer.class)
    })
    public int queryExclusionMarketingCountForTryUpdate(@RequestBody Marketing marketing) {
        return marketingQueryService.queryExclusionMarketingCount(marketing);
    }

    /**
     * 修改试用促销
     *
     * @param marketing 促销信息
     * @return 成功返回1 失败返回0
     */
    @PutMapping("/try")
    @ApiOperation(value = "修改试用促销", notes = "修改试用促销（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int updateTry(@RequestBody Marketing marketing) {
        return tryService.updateMarketing(marketing.setUpdateMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.TRY_MARKETING));
    }


    /**
     * 根据促销id查询试用促销信息
     *
     * @param id 促销id
     * @return 返回试用促销详情信息
     */
    @GetMapping("/try/detail/{id}")
    @ApiOperation(value = "根据促销id查询试用促销信息", notes = "根据促销id查询试用促销信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回试用促销详情信息", response = Marketing.class)
    })
    public Marketing queryTryMarketingByIdForDetail(@PathVariable long id) {
        return marketingQueryService.queryMarketingById(id, CommonConstant.QUERY_WITH_NO_STORE);
    }


    /**
     * 查询试用促销图片信息
     *
     * @return 试用促销图片信息
     */
    @GetMapping("/trypic")
    @ApiOperation(value = "查询试用促销图片信息", notes = "查询试用促销图片信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "试用促销图片信息", response = MarketingPic.class)
    })
    public CommonResponse<MarketingPic> queryTryPic() {
        return CommonResponse.build(marketingPicService.queryMarketingPic(CommonConstant.TRY_MARKETING_PIC_TYPE, CommonConstant.ADMIN_STOREID));
    }

    /**
     * 保存试用促销图片
     *
     * @param marketingPic 促销图片实体
     * @return 成功1 否则失败
     */
    @PostMapping("/trypic")
    @ApiOperation(value = "保存试用促销图片", notes = "保存试用促销图片（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int saveTryPic(@RequestBody MarketingPic marketingPic) {
        return marketingPicService.saveMarketingPic(marketingPic.addType(CommonConstant.TRY_MARKETING_PIC_TYPE).addStoreId(CommonConstant.ADMIN_STOREID));
    }


    /**
     * 分页查询试用促销分类
     *
     * @param pageHelper 分页帮助类
     * @param name       促销分类名称
     * @return 促销分类列表
     */
    @GetMapping("/trymarketing/cates")
    @ApiOperation(value = "分页查询试用促销分类", notes = "分页查询试用促销分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "促销分类名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "试用促销分类列表", response = MarketingCate.class)
    })
    public BaseResponse queryTryMarketingCates(@ApiIgnore PageHelper<MarketingCate> pageHelper, String name) {
        return BaseResponse.build(marketingCateService.queryMarketingCates(pageHelper, name, CommonConstant.TRY_MARKETING_CATE, CommonConstant.ADMIN_STOREID));
    }


    /**
     * 新增试用促销分类
     *
     * @param marketingCate 促销分类信息
     * @return 成功1 否则失败
     */
    @PostMapping("/trymarketing/cate")
    @ApiOperation(value = "新增试用促销分类", notes = "新增试用促销分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int addTryMarketingCate(@RequestBody MarketingCate marketingCate) {
        return marketingCateService.addMarketingCate(marketingCate.buildForAdd(CommonConstant.TRY_MARKETING_CATE, CommonConstant.ADMIN_STOREID));
    }


    /**
     * 删除试用促销分类
     *
     * @param ids 促销分类id数组
     * @return 成功>0 否则失败
     */
    @DeleteMapping("/trymarketing/cate")
    @ApiOperation(value = "删除试用促销分类", notes = "删除试用促销分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "促销分类id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功>0 否则失败", response = Integer.class)
    })
    public int deleteTryMarketingCate(long[] ids) {
        return marketingCateService.deleteMarketingCates(ids, CommonConstant.ADMIN_STOREID, CommonConstant.TRY_MARKETING_CATE);
    }

    /**
     * 根据id查询试用促销分类
     *
     * @param id 促销分类id
     * @return 试用促销分类信息
     */
    @GetMapping("/trymarketing/cate/{id}")
    @ApiOperation(value = "根据id查询试用促销分类", notes = "根据id查询试用促销分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销分类id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "促销分类信息", response = MarketingCate.class)
    })
    public MarketingCate queryMarketingCateById(@PathVariable long id) {
        return marketingCateService.queryMarketingCateById(id, CommonConstant.ADMIN_STOREID);
    }


    /**
     * 修改试用促销分类
     *
     * @param marketingCate 促销分类信息
     * @return 成功1 否则失败
     */
    @PutMapping("/trymarketing/cate")
    @ApiOperation(value = "修改试用促销分类", notes = "修改试用促销分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int updateMarketingCate(@RequestBody MarketingCate marketingCate) {
        marketingCate.setStoreId(CommonConstant.ADMIN_STOREID);
        return marketingCateService.updateMarketingCate(marketingCate);
    }

    /**
     * 分页查询优惠券
     *
     * @param pageHelper 分页帮助类
     * @param name       查询条件,优惠券名称
     * @return 优惠券集合
     */
    @GetMapping("/try/update/coupon")
    @ApiOperation(value = "查询优惠券", notes = "查询优惠券（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "优惠券名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "优惠券集合", response = Coupon.class)
    })
    public BaseResponse queryCoupon(@ApiIgnore PageHelper<Coupon> pageHelper, String name) {
        return BaseResponse.build(couponService.queryCoupon(pageHelper, name, CommonConstant.ADMIN_STOREID));
    }
}
