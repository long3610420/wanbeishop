package com.ruoyi.web.controller.marketing;


import com.ruoyi.marketing.domain.TryReport;
import com.ruoyi.marketing.service.impl.TryReportService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 魔金商城
 * @date 2019-09-20 14:36
 * <p>
 * 试用报告接口
 */
@RestController
@Api("试用报告接口")
public class TryReportController {


    /**
     * 注入试用报告服务接口
     */
    @Autowired
    private TryReportService tryReportService;


    /**
     * 查询试用报告
     *
     * @param tryApplyId 试用申请id
     * @return 返回试用报告信息
     */
    @GetMapping("/tryreport/{tryApplyId}")
    @ApiOperation(value = "查询试用报告", notes = "查询试用报告（需要认证）")
    @PreAuthorize("@ss.hasPermi('try')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "tryApplyId", value = "试用申请id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回试用报告信息", response = TryReport.class)
    })
    public TryReport queryTryReport(@PathVariable long tryApplyId) {
        return tryReportService.queryTryReportByTryApplyId(tryApplyId, -1);
    }


}
