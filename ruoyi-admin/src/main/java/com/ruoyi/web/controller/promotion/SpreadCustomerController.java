package com.ruoyi.web.controller.promotion;


import com.ruoyi.member.domain.UmsMember;
import com.ruoyi.member.service.IUmsMemberService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author 魔金商城
 * @date 2019-06-19 11:01
 * <p>
 * 分销会员控制器
 */
@RestController
@Api(description = "分销会员接口")
public class SpreadCustomerController {

    /**
     * 会员服务接口
     */
    @Autowired
    private IUmsMemberService customerService;


    /**
     * 分页查询有下级的会员信息
     *
     * @param pageHelper 分页帮助类
     * @return 会员列表
     */
    @GetMapping("/spreadcustomer")
    @ApiOperation(value = "分页查询有下级的会员信息", notes = "分页查询有下级的会员信息（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "会员列表", response = UmsMember.class)
    })
    public BaseResponse querySpreadCustomerList(@ApiIgnore PageHelper<UmsMember> pageHelper) {
        return BaseResponse.build(customerService.querySpreadCustomerList(pageHelper));
    }


}
