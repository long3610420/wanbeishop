package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStoreSku;
import com.ruoyi.store.service.ITStoreSkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 门店单品Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStoreSku")
public class TStoreSkuController extends BaseController {
    @Autowired
    private ITStoreSkuService tStoreSkuService;

    /**
     * 查询门店单品列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreSku:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStoreSku tStoreSku) {
        startPage();
        List<TStoreSku> list = tStoreSkuService.selectTStoreSkuList(tStoreSku);
        return getDataTable(list);
    }

    /**
     * 导出门店单品列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreSku:export')")
    @Log(title = "门店单品", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStoreSku tStoreSku) {
        List<TStoreSku> list = tStoreSkuService.selectTStoreSkuList(tStoreSku);
        ExcelUtil<TStoreSku> util = new ExcelUtil<TStoreSku>(TStoreSku.class);
        return util.exportExcel(list, "TStoreSku");
    }

    /**
     * 获取门店单品详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreSku:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStoreSkuService.selectTStoreSkuById(id));
    }

    /**
     * 新增门店单品
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreSku:add')")
    @Log(title = "门店单品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStoreSku tStoreSku) {
        return toAjax(tStoreSkuService.insertTStoreSku(tStoreSku));
    }

    /**
     * 修改门店单品
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreSku:edit')")
    @Log(title = "门店单品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStoreSku tStoreSku) {
        return toAjax(tStoreSkuService.updateTStoreSku(tStoreSku));
    }

    /**
     * 删除门店单品
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreSku:remove')")
    @Log(title = "门店单品", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStoreSkuService.deleteTStoreSkuByIds(ids));
    }
}
