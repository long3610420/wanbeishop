package com.ruoyi.web.controller.marketing;


import com.ruoyi.goods.domain.PmsSku;
import com.ruoyi.goods.service.IPmsSkuService;
import com.ruoyi.marketing.domain.Marketing;
import com.ruoyi.marketing.service.MarketingQueryService;
import com.ruoyi.marketing.service.MarketingService;
import com.ruoyi.marketing.service.MarketingServiceApi;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.MarketingConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

/**
 * Created by 魔金商城平台管理on 2019/7/15.
 * 限时折扣控制器
 */
@RestController
@Api("限时折扣接口")
public class PanicBuyMarketingController {

    /**
     * 注入促销混合api接口
     */
    @Autowired
    private MarketingServiceApi marketingServiceApi;

    /**
     * 注入限时折扣服务接口
     */
    @Resource(name = "panicBuyService")
    private MarketingService panicBuyService;


    /**
     * 注入促销查询接口
     */
    @Autowired
    private MarketingQueryService marketingQueryService;


    /**
     * 注入促销删除服务接口
     */
    @Resource(name = "marketingDeleteService")
    private MarketingService marketingDeleteService;

    /**
     * 注入单品服务
     */
    @Autowired
    private IPmsSkuService skuService;


    /**
     * 分页查询限时折扣促销信息
     *
     * @param pageHelper 分页帮助类
     * @param name       名称
     * @param type       类型
     * @param status     活动状态 1 未开始 2 进行中 3 已结束
     * @return 返回促销信息
     */
    @GetMapping("/panicbuy")
    @ApiOperation(value = "分页查询限时折扣促销信息", notes = "分页查询限时折扣促销信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "限时折扣名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "type", value = "限时折扣类型"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "status", value = "活动状态 1 未开始 2 进行中 3 已结束"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回促销信息", response = Marketing.class)
    })
    public BaseResponse queryPanicBuyMarketing(@ApiIgnore PageHelper<Marketing> pageHelper, String name, String type, String status) {
        return BaseResponse.build(marketingQueryService.queryMarketings(pageHelper, name, type, CommonConstant.ADMIN_STOREID, status));
    }


    /**
     * 批量删除限时折扣促销
     *
     * @param marketingIds 促销id集合
     * @return 成功返回1 否则失败
     */
    @DeleteMapping("/panicbuy")
    @ApiOperation(value = "批量删除限时折扣促销", notes = "批量删除限时折扣促销（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "marketingIds", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 否则失败", response = Integer.class)
    })
    public int deletePanicBuyByIds(Long[] marketingIds) {
        return marketingDeleteService.deleteMarketings(marketingIds, CommonConstant.ADMIN_STOREID, MarketingConstant.PANIC_BUY_MARKETING);
    }

    /**
     * 分页查询单品信息（限时折扣用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回单品信息(包括规格信息)
     */
    @GetMapping("/panicbuy/skus")
    @ApiOperation(value = "分页查询单品信息（限时折扣用）", notes = "分页查询单品信息（限时折扣用）（需要认证")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForPanicBuy(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }

    /**
     * 添加限时折扣促销
     *
     * @param marketing 限时折扣促销
     * @return 成功返回 1 失败返回0
     */
    @PostMapping("/panicbuy")
    @ApiOperation(value = "添加限时折扣促销", notes = "添加限时折扣促销（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回 1 失败返回0", response = Integer.class)
    })
    public int addPanicMarketing(@RequestBody Marketing marketing) {
        return panicBuyService.addMarketing(marketing.setAddMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.PANIC_BUY_MARKETING));
    }


    /**
     * 查询交叉时间内含有相同单品的互斥促销数量（限时折扣用）
     *
     * @param marketing 促销信息
     * @return 交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品
     */
    @PostMapping("panicbuy/exclusionmarketingcount")
    @ApiOperation(value = "查询交叉时间内含有相同单品的互斥促销数量（限时折扣用）", notes = "查询交叉时间内含有相同单品的互斥促销数量（限时折扣用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品", response = Integer.class)
    })
    public int queryExclusionMarketingCountForPanicBuy(@RequestBody Marketing marketing) {
        return marketingQueryService.queryExclusionMarketingCount(marketing);
    }


    /**
     * 根据促销id查询限时折扣促销信息
     *
     * @param id 促销id
     * @return 返回限时折扣促销详情信息
     */
    @GetMapping("/panicbuy/{id}")
    @ApiOperation(value = "根据促销id查询限时折扣促销信息", notes = "根据促销id查询限时折扣促销信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回限时折扣促销详情信息", response = Marketing.class)
    })
    public Marketing queryPanicMarketingById(@PathVariable long id) {
        return marketingQueryService.queryMarketingById(id, CommonConstant.QUERY_WITH_NO_STORE);
    }

    /**
     * 根据促销id查询限时折扣促销信息(详情页用)
     *
     * @param id 促销id
     * @return 返回限时折扣促销详情信息
     */
    @GetMapping("/panicbuy//detail/{id}")
    @ApiOperation(value = "根据促销id查询限时折扣促销信息(详情页用)", notes = "根据促销id查询限时折扣促销信息(详情页用)（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回限时折扣促销详情信息", response = Marketing.class)
    })
    public Marketing queryPanicMarketingByIdForDeatail(@PathVariable long id) {
        return marketingQueryService.queryMarketingById(id, CommonConstant.QUERY_WITH_NO_STORE);
    }


    /**
     * 分页查询单品信息（修改限时折扣用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回单品信息(包括规格信息)
     */
    @GetMapping("/panicbuy/update/skus")
    @ApiOperation(value = "分页查询单品信息（修改限时折扣用）", notes = "分页查询单品信息（修改限时折扣用）（需要认证")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForPanicBuyUpdate(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }


    /**
     * 更新限时折扣
     *
     * @param marketing 促销信息
     * @return 成功返回1 失败返回0 -1 只有未开始的活动才能修改
     */
    @PutMapping("/panicbuy")
    @ApiOperation(value = "更新限时折扣", notes = "更新限时折扣（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int updatePanicMarketing(@RequestBody Marketing marketing) {
        return marketingServiceApi.updatePanic(marketing.setUpdateMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.PANIC_BUY_MARKETING));
    }


    /**
     * 查询交叉时间内含有相同单品的互斥促销数量（更新限时折扣用）
     *
     * @param marketing 促销信息
     * @return 交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品
     */
    @PostMapping("panicbuy/update/exclusionmarketingcount")
    @ApiOperation(value = "查询交叉时间内含有相同单品的互斥促销数量（更新限时折扣用）", notes = "查询交叉时间内含有相同单品的互斥促销数量（限时折扣用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品", response = Integer.class)
    })
    public int queryExclusionMarketingCountForPanicBuyUpdate(@RequestBody Marketing marketing) {
        return marketingQueryService.queryExclusionMarketingCount(marketing);
    }

}
