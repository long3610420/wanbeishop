package com.ruoyi.web.controller.promotion;


import com.ruoyi.member.domain.UmsMember;
import com.ruoyi.member.service.IUmsMemberService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 魔金商城
 * @date 2019-06-19 13:59
 * <p>
 * 子分销会员控制器
 */
@RestController
@Api(description = "子分销会员接口")
public class SubDistributionCustomerList {

    /**
     * 会员服务接口
     */
    @Autowired
    private IUmsMemberService customerService;


    /**
     * 根据会员id查询分销下级会员
     *
     * @param customerId 会员id
     * @return 下级会员列表
     */
    @GetMapping("/subdistributioncustomer/{customerId}")
    @ApiOperation(value = "分页查询有下级的会员信息", notes = "分页查询有下级的会员信息（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "customerId", value = "会员id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "下级会员列表", response = UmsMember.class)
    })
    public List<UmsMember> querySpreadCustomerByCustomerId(@PathVariable long customerId) {
        return customerService.querySpreadCustomerByCustomerId(customerId);
    }


}
