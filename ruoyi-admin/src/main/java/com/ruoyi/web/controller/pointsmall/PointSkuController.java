package com.ruoyi.web.controller.pointsmall;


import com.ruoyi.integral.domain.PointCate;
import com.ruoyi.integral.domain.PointSku;
import com.ruoyi.integral.service.PointCateService;
import com.ruoyi.integral.service.PointSkuService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonResponse;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 积分商品控制器
 *
 * @author 魔金商城 created on 2019/7/26
 */
@RestController
@Api(description = "积分商品接口")
public class PointSkuController {

    /**
     * 注入积分商品服务接口
     */
    @Autowired
    private PointSkuService pointSkuService;

    /**
     * 注入积分商城分类服务接口
     */
    @Autowired
    private PointCateService pointCateService;


    /**
     * 分页查询积分商品
     *
     * @param pageHelper 分页帮助类
     * @param name       名称a
     * @param cateId     分类id
     * @return 积分商品集合
     */
    @GetMapping("/pointskulist")
    @ApiOperation(value = "分页查询积分商品", notes = "分页查询积分商品（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointsku/pointskulist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "cateId", value = "分类id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "积分商品集合", response = PointSku.class)
    })
    public BaseResponse queryPointSkuList(@ApiIgnore PageHelper<PointSku> pageHelper, String name, Long cateId) {
        return BaseResponse.build(pointSkuService.queryPointSkus(pageHelper, cateId, name, false));
    }

    /**
     * 查询所有积分商城分类
     *
     * @return 所有积分商城分类集合
     */
    @GetMapping("/allpointcates")
    @ApiOperation(value = "查询所有积分商城分类", notes = "查询所有积分商城分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointsku/allpointcates')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "所有积分商城分类集合", response = PointCate.class)
    })
    public List<PointCate> queryAllPointCates() {
        return pointCateService.queryAllPointCates();
    }

    /**
     * 删除积分商品
     *
     * @param ids 积分商品id数组
     * @return 成功>0 否则失败
     */
    @DeleteMapping("/pointsku")
    @ApiOperation(value = "删除积分商品", notes = "删除积分商品（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointsku/deletepointsku')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "积分商品id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功>0 否则失败", response = Integer.class)
    })
    public int deletePointSkus(Long... ids) {
        return pointSkuService.deletePointSkus(ids);
    }

    /**
     * 查询所有积分商城分类（新增用）
     *
     * @return 所有积分商城分类集合
     */
    @GetMapping("/allpointcatesforadd")
    @ApiOperation(value = "查询所有积分商城分类（新增用）", notes = "查询所有积分商城分类（新增用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointsku/allpointcatesforadd')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "所有积分商城分类集合", response = PointCate.class)
    })
    public List<PointCate> queryAllPointCatesForAdd() {
        return pointCateService.queryAllPointCates();
    }

    /**
     * 新增积分商品
     *
     * @param pointSku 积分商品实体
     * @return 成功1 否则失败 -1:没有商品编号 -2:商品编号已存在
     */
    @PostMapping("/pointsku")
    @ApiOperation(value = "新增积分商品", notes = "新增积分商品（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointsku/addpointsku')")
    @ApiResponses({
            @ApiResponse(code = 200, message = " 成功1 否则失败 -1:没有商品编号 -2:商品编号已存在", response = Integer.class)
    })
    public int addPointSku(@RequestBody PointSku pointSku) {
        return pointSkuService.addPointSku(pointSku);
    }

    /**
     * 查询所有积分商城分类（修改用）
     *
     * @return 所有积分商城分类集合
     */
    @GetMapping("/allpointcatesforupdate")
    @ApiOperation(value = "查询所有积分商城分类（修改用）", notes = "查询所有积分商城分类（修改用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointsku/allpointcatesforupdate')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "所有积分商城分类集合", response = PointCate.class)
    })
    public List<PointCate> queryAllPointCatesForUpdate() {
        return pointCateService.queryAllPointCates();
    }

    /**
     * 根据id查询积分商品
     *
     * @param id 积分商品id
     * @return 积分商品信息
     */
    @GetMapping("/pointsku/{id}")
    @ApiOperation(value = "根据id查询积分商品", notes = "根据id查询积分商品（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointsku/querypointskubyid')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "积分商品id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "积分商品信息", response = PointSku.class)
    })
    public CommonResponse<PointSku> queryPointSkuById(@PathVariable long id) {
        return CommonResponse.build(pointSkuService.queryPointSkuById(id));
    }

    /**
     * 修改积分商品
     *
     * @param pointSku 积分商品实体
     * @return 成功1 否则失败 -1:没有商品编号 -2:商品编号已存在
     */
    @PutMapping("/pointsku")
    @ApiOperation(value = "修改积分商品", notes = "修改积分商品（需要认证）")
    @PreAuthorize("@ss.hasPermi('pointsku/updatepointsku')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败 -1:没有商品编号 -2:商品编号已存在", response = Integer.class)
    })
    public int updatePointSku(@RequestBody PointSku pointSku) {
        return pointSkuService.updatePointSku(pointSku);
    }

}
