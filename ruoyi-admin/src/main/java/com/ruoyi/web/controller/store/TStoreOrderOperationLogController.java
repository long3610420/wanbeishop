package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStoreOrderOperationLog;
import com.ruoyi.store.service.ITStoreOrderOperationLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 门店订单操作日志Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStoreOrderOperationLog")
public class TStoreOrderOperationLogController extends BaseController {
    @Autowired
    private ITStoreOrderOperationLogService tStoreOrderOperationLogService;

    /**
     * 查询门店订单操作日志列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderOperationLog:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStoreOrderOperationLog tStoreOrderOperationLog) {
        startPage();
        List<TStoreOrderOperationLog> list = tStoreOrderOperationLogService.selectTStoreOrderOperationLogList(tStoreOrderOperationLog);
        return getDataTable(list);
    }

    /**
     * 导出门店订单操作日志列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderOperationLog:export')")
    @Log(title = "门店订单操作日志", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStoreOrderOperationLog tStoreOrderOperationLog) {
        List<TStoreOrderOperationLog> list = tStoreOrderOperationLogService.selectTStoreOrderOperationLogList(tStoreOrderOperationLog);
        ExcelUtil<TStoreOrderOperationLog> util = new ExcelUtil<TStoreOrderOperationLog>(TStoreOrderOperationLog.class);
        return util.exportExcel(list, "TStoreOrderOperationLog");
    }

    /**
     * 获取门店订单操作日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderOperationLog:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStoreOrderOperationLogService.selectTStoreOrderOperationLogById(id));
    }

    /**
     * 新增门店订单操作日志
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderOperationLog:add')")
    @Log(title = "门店订单操作日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStoreOrderOperationLog tStoreOrderOperationLog) {
        return toAjax(tStoreOrderOperationLogService.insertTStoreOrderOperationLog(tStoreOrderOperationLog));
    }

    /**
     * 修改门店订单操作日志
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderOperationLog:edit')")
    @Log(title = "门店订单操作日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStoreOrderOperationLog tStoreOrderOperationLog) {
        return toAjax(tStoreOrderOperationLogService.updateTStoreOrderOperationLog(tStoreOrderOperationLog));
    }

    /**
     * 删除门店订单操作日志
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderOperationLog:remove')")
    @Log(title = "门店订单操作日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStoreOrderOperationLogService.deleteTStoreOrderOperationLogByIds(ids));
    }
}
