package com.ruoyi.web.controller.sms;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.sms.domain.SmsHomeAdvertise;
import com.ruoyi.sms.service.ISmsHomeAdvertiseService;
import com.ruoyi.util.CommonConstant;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * 首页轮播广告Controller
 *
 * @author é­éåå
 * @date 2020-08-06
 */
@RestController
@Api(description = "轮播广告")
@RequestMapping("/sms/SmsHomeAdvertise")
public class SmsHomeAdvertiseController extends BaseController {
    @Autowired
    private ISmsHomeAdvertiseService smsHomeAdvertiseService;

    /**
     * 查询首页轮播广告列表
     */
    @PreAuthorize("@ss.hasPermi('sms:SmsHomeAdvertise:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询首页轮播广告列表", notes = "查询首页轮播广告列表")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "orderCount", value = "类型"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前查找页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示数量")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回广告列表", response = TableDataInfo.class)
    })
    public TableDataInfo list(SmsHomeAdvertise smsHomeAdvertise) {
        smsHomeAdvertise.setStoreId(CommonConstant.ADMIN_STOREID);
        startPage();
        List<SmsHomeAdvertise> list = smsHomeAdvertiseService.selectSmsHomeAdvertiseList(smsHomeAdvertise);
        return getDataTable(list);
    }

    /**
     * 导出首页轮播广告列表
     */
    @PreAuthorize("@ss.hasPermi('sms:SmsHomeAdvertise:export')")
    @Log(title = "首页轮播广告", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SmsHomeAdvertise smsHomeAdvertise) {
        smsHomeAdvertise.setStoreId(CommonConstant.ADMIN_STOREID);
        List<SmsHomeAdvertise> list = smsHomeAdvertiseService.selectSmsHomeAdvertiseList(smsHomeAdvertise);
        ExcelUtil<SmsHomeAdvertise> util = new ExcelUtil<SmsHomeAdvertise>(SmsHomeAdvertise.class);
        return util.exportExcel(list, "SmsHomeAdvertise");
    }

    /**
     * 获取首页轮播广告详细信息
     */
    @PreAuthorize("@ss.hasPermi('sms:SmsHomeAdvertise:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(smsHomeAdvertiseService.selectSmsHomeAdvertiseById(id));
    }

    /**
     * 新增首页轮播广告
     */
    @PreAuthorize("@ss.hasPermi('sms:SmsHomeAdvertise:add')")
    @Log(title = "首页轮播广告", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SmsHomeAdvertise smsHomeAdvertise) {
        smsHomeAdvertise.setStoreId(CommonConstant.ADMIN_STOREID);
        if (Objects.isNull(smsHomeAdvertise.getType())) {
            smsHomeAdvertise.setType(4L);
        }
        if (Objects.isNull(smsHomeAdvertise.getStatus())) {
            smsHomeAdvertise.setStatus(0);
        }
        if (Objects.isNull(smsHomeAdvertise.getOrderCount())) {
            smsHomeAdvertise.setOrderCount(1);
        }
        return toAjax(smsHomeAdvertiseService.insertSmsHomeAdvertise(smsHomeAdvertise));
    }

    /**
     * 修改首页轮播广告
     */
    @PreAuthorize("@ss.hasPermi('sms:SmsHomeAdvertise:edit')")
    @Log(title = "首页轮播广告", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SmsHomeAdvertise smsHomeAdvertise) {
        smsHomeAdvertise.setStoreId(CommonConstant.ADMIN_STOREID);
        return toAjax(smsHomeAdvertiseService.updateSmsHomeAdvertise(smsHomeAdvertise));
    }

    /**
     * 删除首页轮播广告
     */
    @PreAuthorize("@ss.hasPermi('sms:SmsHomeAdvertise:remove')")
    @Log(title = "首页轮播广告", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(smsHomeAdvertiseService.deleteSmsHomeAdvertiseByIds(ids));
    }
}
