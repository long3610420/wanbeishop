package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStorePaytype;
import com.ruoyi.store.service.ITStorePaytypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 门店支付类型Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStorePaytype")
public class TStorePaytypeController extends BaseController {
    @Autowired
    private ITStorePaytypeService tStorePaytypeService;

    /**
     * 查询门店支付类型列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStorePaytype:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStorePaytype tStorePaytype) {
        startPage();
        List<TStorePaytype> list = tStorePaytypeService.selectTStorePaytypeList(tStorePaytype);
        return getDataTable(list);
    }

    /**
     * 导出门店支付类型列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStorePaytype:export')")
    @Log(title = "门店支付类型", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStorePaytype tStorePaytype) {
        List<TStorePaytype> list = tStorePaytypeService.selectTStorePaytypeList(tStorePaytype);
        ExcelUtil<TStorePaytype> util = new ExcelUtil<TStorePaytype>(TStorePaytype.class);
        return util.exportExcel(list, "TStorePaytype");
    }

    /**
     * 获取门店支付类型详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStorePaytype:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStorePaytypeService.selectTStorePaytypeById(id));
    }

    /**
     * 新增门店支付类型
     */
    @PreAuthorize("@ss.hasPermi('store:TStorePaytype:add')")
    @Log(title = "门店支付类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStorePaytype tStorePaytype) {
        return toAjax(tStorePaytypeService.insertTStorePaytype(tStorePaytype));
    }

    /**
     * 修改门店支付类型
     */
    @PreAuthorize("@ss.hasPermi('store:TStorePaytype:edit')")
    @Log(title = "门店支付类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStorePaytype tStorePaytype) {
        return toAjax(tStorePaytypeService.updateTStorePaytype(tStorePaytype));
    }

    /**
     * 删除门店支付类型
     */
    @PreAuthorize("@ss.hasPermi('store:TStorePaytype:remove')")
    @Log(title = "门店支付类型", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStorePaytypeService.deleteTStorePaytypeByIds(ids));
    }
}
