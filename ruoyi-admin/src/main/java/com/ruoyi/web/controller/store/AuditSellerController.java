package com.ruoyi.web.controller.store;


import com.ruoyi.store.domain.TStoreInfo;
import com.ruoyi.store.service.ITStoreInfoService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Arrays;

/**
 * @author 魔金商城
 * @date 2019-06-27 19:48
 * <p>
 * 商家审核控制器
 */
@RestController
@Api(description = "商家审核接口")
public class AuditSellerController {

    /**
     * 注入店铺信息服务
     */
    @Autowired
    private ITStoreInfoService storeInfoService;

    /**
     * 分页查询审核商家列表
     *
     * @param pageHelper  分页帮助类
     * @param companyName 公司名称
     * @param storeName   店铺名称
     * @param mobile      用户手机号
     * @return 已审核通过商家信息
     */
    @GetMapping("/auditseller")
    @PreAuthorize("@ss.hasPermi('store:TStoreInfo:list')")
    @ApiOperation(value = "分页查询审核商家列表", notes = "分页查询审核商家列表（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "companyName", value = "公司名称"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "storeName", value = "店铺名称"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "mobile", value = "用户手机号"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "status", value = "店铺状态 0填写资料中 1店铺审核中 2审核通过 3审核不通过 4店铺关闭"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "已审核通过商家信息", response = TStoreInfo.class)
    })
    public BaseResponse querySellerAduitList(@ApiIgnore PageHelper<TStoreInfo> pageHelper, String companyName, String storeName, String mobile, String status) {
        return BaseResponse.build(storeInfoService.queryStoreInfoForAuditList(pageHelper, status, companyName, storeName, null, mobile, -1));
    }


    /**
     * 删除商家
     *
     * @param id 商家id
     * @return 成功返回1，失败返回0
     */
    @DeleteMapping("/auditseller/{id}")
    @ApiOperation(value = "删除商家", notes = "删除商家（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "商家id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1，失败返回0", response = Integer.class)
    })
    public int deleteStore(@PathVariable long id) {
        return storeInfoService.deleteStore(id);
    }


    /**
     * 商家通过审核
     *
     * @param storeInfo 商家实例
     * @return 成功返回1，失败返回0
     */
    @PutMapping("/auditseller")
    @ApiOperation(value = "商家通过审核", notes = "商家通过审核（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1，失败返回0", response = Integer.class)
    })
    public int passStoreAudit(@RequestBody TStoreInfo storeInfo) {
        return storeInfoService.passStoreAudit(storeInfo);
    }


    /**
     * 拒绝商家
     *
     * @param storeInfo 商家实例
     * @return 成功返回1，失败返回0
     */
    @PutMapping("/auditseller/refuse")
    @ApiOperation(value = "拒绝商家", notes = "拒绝商家（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1，失败返回0", response = Integer.class)
    })
    public int refuseStoreAudit(@RequestBody TStoreInfo storeInfo) {
        return storeInfoService.refuseStoreAudit(storeInfo);
    }


    /**
     * 批量关店
     *
     * @param ids 店铺id
     * @return 成功返回>0 失败返回0
     */
    @PutMapping("auditseller/closestores")
    @ApiOperation(value = "批量关店", notes = "批量关店（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "array", name = "ids", value = "店铺id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1，失败返回0", response = Integer.class)
    })
    public int closeStores(Long[] ids) {
        return storeInfoService.closeStores(Arrays.asList(ids), null);
    }


}
