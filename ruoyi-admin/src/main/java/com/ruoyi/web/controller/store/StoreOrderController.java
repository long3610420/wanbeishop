package com.ruoyi.web.controller.store;


import com.ruoyi.order.domain.OmsOrder;
import com.ruoyi.order.service.IOmsOrderService;
import com.ruoyi.order.service.IOrderApiService;
import com.ruoyi.order.vo.ConfirmOrderParams;
import com.ruoyi.order.vo.OrderItem;
import com.ruoyi.order.vo.QueryOrderCriteria;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import com.ruoyi.web.utils.AdminLoginUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by dujinkai on 2019/6/24.
 * 店铺订单控制器
 */
@RestController
@Api(description = "店铺订单接口")
public class StoreOrderController {

    /**
     * 订单服务接口
     */
    @Autowired
    private IOmsOrderService orderService;

    /**
     * 注入订单混合api接口
     */
    @Autowired
    private IOrderApiService orderServiceApi;

    /**
     * 查询所有店铺订单
     *
     * @param pageHelper    分页帮助类
     * @param queryCriteria 查询条件
     * @return 返回所有店铺订单
     */
    @GetMapping("/storeorders")
    @ApiOperation(value = "查询所有店铺订单", notes = "查询所有店铺订单（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "status", value = "订单状态 1:待付款  （用户刚下单）,2:代发货  （用户付完款 等待商城发货）,3:代收货  （商城已经发货 等待用户确认收货）, 4:已完成  （用户已经确认收货 订单结束）, 5:已关闭 （用户未付款前取消订单,退款成功,退货成功）,6:待评价,7:已评价"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "orderCode", value = "订单编号"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "customerName", value = "用户名"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "storeName", value = "店铺名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "orderType", value = "订单类型 0 普通订单 1 定金预售订单 2全款预售订单"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "startTime", value = "开始时间"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "endTime", value = "结束时间"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回所有店铺订单", response = OmsOrder.class)
    })
    public BaseResponse queryStoreOrders(@ApiIgnore PageHelper<OmsOrder> pageHelper, @ApiIgnore QueryOrderCriteria queryCriteria) {
        return BaseResponse.build(orderService.queryStoreOrders(pageHelper, queryCriteria));
    }


    /**
     * 确认店铺的订单
     *
     * @param id      订单id
     * @param storeId 店铺id
     * @return 成功返回1 失败返回0
     */
    @PutMapping("/confirmstoreorder/{id}/{storeId}")
    @ApiOperation(value = "确认店铺的订单", notes = "确认店铺的订单（需要认证）")

    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "订单id"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "店铺id"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "reason", value = "原因"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int confirmStoreOrder(@PathVariable long id, @PathVariable long storeId, String reason) {
        return orderServiceApi.confirmOrderPayed(ConfirmOrderParams.buildManagerSource(id, storeId, reason, AdminLoginUtils.getInstance().getManagerName()));
    }

    /**
     * 修改店铺订单价格
     *
     * @param id     订单id
     * @param price  修改价格(减多少钱)
     * @param reason 原因
     * @return 成功返回 1 失败返回0 -1 订单最低支付1毛 -2 订单不能修改价格
     */
    @PutMapping("/modifystoreorderprice/{id}/{storeId}")
    @ApiOperation(value = "修改店铺订单价格", notes = "修改店铺订单价格（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "订单id"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "店铺id"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "reason", value = "原因"),
            @ApiImplicitParam(paramType = "form", dataType = "BigDecimal", name = "price", value = "修改价格(减多少钱)"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回 1 失败返回0 -1 订单最低支付1毛 -2 订单不能修改价格", response = Integer.class)
    })
    public int modifyStoreOrderPrice(@PathVariable long id, @PathVariable long storeId, String reason, BigDecimal price) {
        return orderService.modifyPrice(id, price, reason, storeId, AdminLoginUtils.getInstance().getManagerName());
    }





    /**
     * 查询打印订单详情（店铺订单用）
     *
     * @param ids 订单id
     * @return 返回打印订单详情
     */
    @GetMapping("/printstoreorderdetails")
    @ApiOperation(value = "查询打印订单详情（店铺订单用）", notes = "查询打印订单详情（店铺订单用）（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "订单id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回打印订单详情", response = Map.class)
    })
    public Map<String, Object> queryPrintStoreOrderDetails(Long... ids) {
        Map<String, Object> result = new HashMap<>();
        result.put("orders", orderServiceApi.queryPrintOrderDetails(Arrays.asList(ids).stream().filter(Objects::nonNull).collect(Collectors.toList())));
        result.put("operator", AdminLoginUtils.getInstance().getManagerName());
        return result;
    }

    /**
     * 根据订单id查询订单信息  (订单的所有信息 此接口慎用)
     *
     * @param id 订单id
     * @return 返回订单信息
     */
    @GetMapping("/store/orderdetail/{id}")
    @ApiOperation(value = "根据订单id查询订单信息  (订单的所有信息 此接口慎用)", notes = "根据订单id查询订单信息  (订单的所有信息 此接口慎用)（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "订单id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回订单信息", response = OmsOrder.class)
    })
    public OmsOrder queryOrderByIdForStore(@PathVariable long id) {
        return orderServiceApi.queryOrderDetailById(id, CommonConstant.QUERY_WITH_NO_STORE, CommonConstant.QUERY_WITH_NO_CUSTOMER, OrderItem.LOG, OrderItem.ATTR, OrderItem.SKUS);
    }
}
