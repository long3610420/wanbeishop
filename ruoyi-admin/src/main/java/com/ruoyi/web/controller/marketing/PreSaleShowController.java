package com.ruoyi.web.controller.marketing;


import com.ruoyi.marketing.domain.MarketingCate;
import com.ruoyi.marketing.domain.PreSale;
import com.ruoyi.marketing.domain.PreSaleShow;
import com.ruoyi.marketing.service.MarketingCateService;
import com.ruoyi.marketing.service.PreSaleServiceApi;
import com.ruoyi.marketing.service.PreSaleShowService;
import com.ruoyi.marketing.service.PreSaleShowServiceApi;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 预售活动控制器
 *
 * @author 魔金商城 created on 2020/6/12
 */
@RestController
@Api(description = "预售活动接口")
public class PreSaleShowController {

    /**
     * 注入预售活动服务接口
     */
    @Autowired
    private PreSaleShowService preSaleShowService;

    /**
     * 注入促销分类服务接口
     */
    @Autowired
    private MarketingCateService marketingCateService;

    /**
     * 注入预售促销聚合服务接口
     */
    @Autowired
    private PreSaleServiceApi preSaleServiceApi;

    /**
     * 注入预售活动聚合服务接口
     */
    @Autowired
    private PreSaleShowServiceApi preSaleShowServiceApi;


    /**
     * 新增预售活动
     *
     * @param preSaleShowList 预售活动列表
     * @return 成功>0 否则失败 -1 存在重复添加
     */
    @PostMapping("/presaleshow")
    @ApiOperation(value = "新增预售活动", notes = "新增预售活动（需要认证）")
    @PreAuthorize("@ss.hasPermi('presaleshow')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "list", name = "preSaleShowList", value = "预售活动列表"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功>0 否则失败 -1 存在重复添加", response = Integer.class)
    })
    public int addPreSaleShow(@RequestBody List<PreSaleShow> preSaleShowList) {
        return preSaleShowService.addPreSaleShows(preSaleShowList, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 批量删除预售活动
     *
     * @param ids 预售活动id数组
     * @return 成功>0 否则失败
     */
    @DeleteMapping("/presaleshow")
    @ApiOperation(value = "批量删除预售活动", notes = "批量删除预售活动（需要认证）")
    @PreAuthorize("@ss.hasPermi('presaleshow')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "预售活动id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 否则失败", response = Integer.class)
    })
    public int deletePreSaleShowByIds(Long[] ids) {
        return preSaleShowService.deletePreSaleShows(ids, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 查询促销分类列表
     *
     * @return 返回促销分类列表
     */
    @GetMapping("/presaleshow/marketingcatelist")
    @ApiOperation(value = "查询促销分类列表", notes = "查询促销分类列表（需要认证）")
    @PreAuthorize("@ss.hasPermi('presaleshow')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "促销分类列表", response = MarketingCate.class)
    })
    public List<MarketingCate> queryMarketingCatesByType() {
        return marketingCateService.queryMarketingCatesByTypeAndStoreId(CommonConstant.PRESALE_MARKETING_CATE, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 修改预售活动
     *
     * @param id     预售活动id
     * @param cateId 促销分类id
     * @param sort   排序
     * @return 成功1 否则失败
     */
    @PutMapping("/presaleshow/{id}/{cateId}/{sort}")
    @ApiOperation(value = "修改预售活动", notes = "修改预售活动（需要认证）")
    @PreAuthorize("@ss.hasPermi('presaleshow')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int updatePreSaleShow(@PathVariable long id, @PathVariable long cateId, @PathVariable int sort) {
        return preSaleShowService.updatePreSaleShow(id, cateId, sort, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 分页查询预售促销列表
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回预售促销列表
     */
    @GetMapping("/presaleshow/presalelist")
    @ApiOperation(value = "分页查询预售促销列表", notes = "分页查询预售促销列表")
    @PreAuthorize("@ss.hasPermi('presaleshow')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回预售促销列表", response = PreSale.class)
    })
    public BaseResponse queryPreSaleList(@ApiIgnore PageHelper<PreSale> pageHelper, String name, String skuNo) {
        return BaseResponse.build(preSaleServiceApi.queryPreSaleList(pageHelper, name, skuNo, CommonConstant.ADMIN_STOREID));
    }

    /**
     * 分页查询预售活动列表
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回预售活动列表
     */
    @GetMapping("/presaleshowlist")
    @ApiOperation(value = "分页查询预售活动列表", notes = "分页查询预售活动列表")
    @PreAuthorize("@ss.hasPermi('presaleshow')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回预售活动列表", response = PreSaleShow.class)
    })
    public BaseResponse queryPreSaleShowList(@ApiIgnore PageHelper<PreSaleShow> pageHelper, String name, String skuNo) {
        return BaseResponse.build(preSaleShowServiceApi.queryPreSaleShowList(pageHelper, name, skuNo, CommonConstant.ADMIN_STOREID));
    }

}
