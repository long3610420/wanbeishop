package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStoreComment;
import com.ruoyi.store.service.ITStoreCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 店铺评论Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStoreComment")
public class TStoreCommentController extends BaseController {
    @Autowired
    private ITStoreCommentService tStoreCommentService;

    /**
     * 查询店铺评论列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreComment:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStoreComment tStoreComment) {
        startPage();
        List<TStoreComment> list = tStoreCommentService.selectTStoreCommentList(tStoreComment);
        return getDataTable(list);
    }

    /**
     * 导出店铺评论列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreComment:export')")
    @Log(title = "店铺评论", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStoreComment tStoreComment) {
        List<TStoreComment> list = tStoreCommentService.selectTStoreCommentList(tStoreComment);
        ExcelUtil<TStoreComment> util = new ExcelUtil<TStoreComment>(TStoreComment.class);
        return util.exportExcel(list, "TStoreComment");
    }

    /**
     * 获取店铺评论详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreComment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStoreCommentService.selectTStoreCommentById(id));
    }

    /**
     * 新增店铺评论
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreComment:add')")
    @Log(title = "店铺评论", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStoreComment tStoreComment) {
        return toAjax(tStoreCommentService.insertTStoreComment(tStoreComment));
    }

    /**
     * 修改店铺评论
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreComment:edit')")
    @Log(title = "店铺评论", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStoreComment tStoreComment) {
        return toAjax(tStoreCommentService.updateTStoreComment(tStoreComment));
    }

    /**
     * 删除店铺评论
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreComment:remove')")
    @Log(title = "店铺评论", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStoreCommentService.deleteTStoreCommentByIds(ids));
    }
}
