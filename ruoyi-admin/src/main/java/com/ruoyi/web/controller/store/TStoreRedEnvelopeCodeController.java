package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStoreRedEnvelopeCode;
import com.ruoyi.store.service.ITStoreRedEnvelopeCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 门店红包卷吗Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStoreRedEnvelopeCode")
public class TStoreRedEnvelopeCodeController extends BaseController {
    @Autowired
    private ITStoreRedEnvelopeCodeService tStoreRedEnvelopeCodeService;

    /**
     * 查询门店红包卷吗列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreRedEnvelopeCode:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStoreRedEnvelopeCode tStoreRedEnvelopeCode) {
        startPage();
        List<TStoreRedEnvelopeCode> list = tStoreRedEnvelopeCodeService.selectTStoreRedEnvelopeCodeList(tStoreRedEnvelopeCode);
        return getDataTable(list);
    }

    /**
     * 导出门店红包卷吗列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreRedEnvelopeCode:export')")
    @Log(title = "门店红包卷吗", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStoreRedEnvelopeCode tStoreRedEnvelopeCode) {
        List<TStoreRedEnvelopeCode> list = tStoreRedEnvelopeCodeService.selectTStoreRedEnvelopeCodeList(tStoreRedEnvelopeCode);
        ExcelUtil<TStoreRedEnvelopeCode> util = new ExcelUtil<TStoreRedEnvelopeCode>(TStoreRedEnvelopeCode.class);
        return util.exportExcel(list, "TStoreRedEnvelopeCode");
    }

    /**
     * 获取门店红包卷吗详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreRedEnvelopeCode:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStoreRedEnvelopeCodeService.selectTStoreRedEnvelopeCodeById(id));
    }

    /**
     * 新增门店红包卷吗
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreRedEnvelopeCode:add')")
    @Log(title = "门店红包卷吗", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStoreRedEnvelopeCode tStoreRedEnvelopeCode) {
        return toAjax(tStoreRedEnvelopeCodeService.insertTStoreRedEnvelopeCode(tStoreRedEnvelopeCode));
    }

    /**
     * 修改门店红包卷吗
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreRedEnvelopeCode:edit')")
    @Log(title = "门店红包卷吗", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStoreRedEnvelopeCode tStoreRedEnvelopeCode) {
        return toAjax(tStoreRedEnvelopeCodeService.updateTStoreRedEnvelopeCode(tStoreRedEnvelopeCode));
    }

    /**
     * 删除门店红包卷吗
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreRedEnvelopeCode:remove')")
    @Log(title = "门店红包卷吗", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStoreRedEnvelopeCodeService.deleteTStoreRedEnvelopeCodeByIds(ids));
    }
}
