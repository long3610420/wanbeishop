package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStoreBillingRecords;
import com.ruoyi.store.service.ITStoreBillingRecordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 门店账单收入支出Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStoreBillingRecords")
public class TStoreBillingRecordsController extends BaseController {
    @Autowired
    private ITStoreBillingRecordsService tStoreBillingRecordsService;

    /**
     * 查询门店账单收入支出列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreBillingRecords:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStoreBillingRecords tStoreBillingRecords) {
        startPage();
        List<TStoreBillingRecords> list = tStoreBillingRecordsService.selectTStoreBillingRecordsList(tStoreBillingRecords);
        return getDataTable(list);
    }

    /**
     * 导出门店账单收入支出列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreBillingRecords:export')")
    @Log(title = "门店账单收入支出", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStoreBillingRecords tStoreBillingRecords) {
        List<TStoreBillingRecords> list = tStoreBillingRecordsService.selectTStoreBillingRecordsList(tStoreBillingRecords);
        ExcelUtil<TStoreBillingRecords> util = new ExcelUtil<TStoreBillingRecords>(TStoreBillingRecords.class);
        return util.exportExcel(list, "TStoreBillingRecords");
    }

    /**
     * 获取门店账单收入支出详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreBillingRecords:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStoreBillingRecordsService.selectTStoreBillingRecordsById(id));
    }

    /**
     * 新增门店账单收入支出
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreBillingRecords:add')")
    @Log(title = "门店账单收入支出", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStoreBillingRecords tStoreBillingRecords) {
        return toAjax(tStoreBillingRecordsService.insertTStoreBillingRecords(tStoreBillingRecords));
    }

    /**
     * 修改门店账单收入支出
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreBillingRecords:edit')")
    @Log(title = "门店账单收入支出", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStoreBillingRecords tStoreBillingRecords) {
        return toAjax(tStoreBillingRecordsService.updateTStoreBillingRecords(tStoreBillingRecords));
    }

    /**
     * 删除门店账单收入支出
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreBillingRecords:remove')")
    @Log(title = "门店账单收入支出", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStoreBillingRecordsService.deleteTStoreBillingRecordsByIds(ids));
    }
}
