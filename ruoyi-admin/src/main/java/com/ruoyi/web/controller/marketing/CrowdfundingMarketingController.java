package com.ruoyi.web.controller.marketing;


import com.ruoyi.goods.domain.PmsGoods;
import com.ruoyi.goods.domain.PmsSku;
import com.ruoyi.goods.domain.PmsSkuItem;
import com.ruoyi.goods.service.IPmsGoodsService;
import com.ruoyi.goods.service.IPmsSkuService;
import com.ruoyi.goods.vo.SpuSearchCondition;
import com.ruoyi.marketing.domain.CrowdFunding;
import com.ruoyi.marketing.domain.CrowdfundingProgress;
import com.ruoyi.marketing.domain.Marketing;
import com.ruoyi.marketing.domain.MarketingCate;
import com.ruoyi.marketing.service.*;
import com.ruoyi.order.domain.OmsLogisticsCompany;
import com.ruoyi.order.domain.OmsOrder;
import com.ruoyi.order.service.IOmsLogisticsCompanyService;
import com.ruoyi.order.service.IOmsOrderService;
import com.ruoyi.order.service.IOrderApiService;
import com.ruoyi.order.vo.CancelOrderParams;
import com.ruoyi.order.vo.ConfirmOrderParams;
import com.ruoyi.order.vo.OrderItem;
import com.ruoyi.order.vo.QueryOrderCriteria;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.MarketingConstant;
import com.ruoyi.util.PageHelper;
import com.ruoyi.web.utils.AdminLoginUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by 魔金商城平台管理on 2019/8/6.
 * 众筹控制器
 */
@RestController
@Api("众筹接口")
public class CrowdfundingMarketingController {

    /**
     * 注入促销混合api接口
     */
    @Autowired
    private MarketingServiceApi marketingServiceApi;

    /**
     * 注入促销删除服务接口
     */
    @Resource(name = "marketingDeleteService")
    private MarketingService marketingDeleteService;


    /**
     * 注入单品服务
     */
    @Autowired
    private IPmsSkuService skuService;

    /**
     * 注入商品服务
     */
    @Autowired
    private IPmsGoodsService spuService;

    /**
     * 注入众筹进度服务接口
     */
    @Autowired
    private CrowdfundingProgressService crowdfundingProgressService;


    /**
     * 注入促销分类服务接口
     */
    @Autowired
    private MarketingCateService marketingCateService;


    /**
     * 注入促销查询接口
     */
    @Autowired
    private MarketingQueryService marketingQueryService;

    /**
     * 注入众筹促销服务
     */
    @Resource(name = "crowdFundingService")
    private MarketingService crowdFundingService;


    /**
     * 订单服务接口
     */
    @Autowired
    private IOmsOrderService orderService;


    /**
     * 注入订单混合api接口
     */
    @Autowired
    private IOrderApiService IOrderApiService;


    /**
     * 注入物流服务接口
     */
    @Autowired
    private IOmsLogisticsCompanyService logisticsCompanyService;


    /**
     * 分页查询众筹促销信息
     *
     * @param pageHelper    分页帮助类
     * @param queryCriteria 查询参数
     * @return 返回促销信息
     */
    @GetMapping("/crowdfunding")
    @ApiOperation(value = "分页查询众筹促销信息", notes = "分页查询众筹促销信息（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "status", value = "状态 1筹备中 2众筹中 3众筹成功 4众筹失败"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "项目名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "startTime", value = "开始时间"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "endTime", value = "结束时间"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回促销信息", response = Marketing.class)
    })
    public BaseResponse queryCrowdFundingMarketList(@ApiIgnore PageHelper<Marketing> pageHelper, @ApiIgnore CrowdFunding.QueryCriteria queryCriteria) {
        return BaseResponse.build(marketingServiceApi.queryCrowdFundingMarketings(pageHelper, queryCriteria.buildStoreId(CommonConstant.ADMIN_STOREID)));
    }


    /**
     * 删除众筹促销
     *
     * @param marketingId 促销id
     * @return 成功返回1 否则返回0
     */
    @DeleteMapping("/crowdfunding/{marketingId}")
    @ApiOperation(value = "删除众筹促销", notes = "删除众筹促销（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "marketingId", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 否则返回0", response = Integer.class)
    })
    public int deleteCrowdFundingById(@PathVariable long marketingId) {
        return marketingDeleteService.deleteMarketing(marketingId, CommonConstant.ADMIN_STOREID);
    }


    /**
     * 查询众筹进度
     *
     * @param marketingId 促销id
     * @return 返回众筹进度信息
     */
    @GetMapping("/crowdfunding/schedule/{marketingId}")
    @ApiOperation(value = "查询众筹进度", notes = "查询众筹进度（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "marketingId", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回众筹进度信息", response = CrowdfundingProgress.class)
    })
    public List<CrowdfundingProgress> queryCrowdfundingProgressByMarketingId(@PathVariable long marketingId) {
        return crowdfundingProgressService.queryCrowdfundingProgressByMarketingId(marketingId, CommonConstant.QUERY_WITH_NO_STORE);
    }


    /**
     * 新增众筹进度
     *
     * @param crowdfundingProgress 众筹进度实体
     * @return 成功返回1 失败返回0
     */
    @PostMapping("/crowdfunding/schedule")
    @ApiOperation(value = "新增众筹进度", notes = "新增众筹进度（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int addCrowdfundingProgress(@RequestBody CrowdfundingProgress crowdfundingProgress) {
        return crowdfundingProgressService.addCrowdfundingProgress(crowdfundingProgress, CommonConstant.ADMIN_STOREID);
    }


    /**
     * 删除众筹进度
     *
     * @param id          众筹进度id
     * @param marketingId 促销id
     * @return 成功返回1 失败返回0
     */
    @DeleteMapping("/crowdfunding/schedule/{id}/{marketingId}")
    @ApiOperation(value = "删除众筹进度", notes = "删除众筹进度（需要认证）")
    public int deleteCrowdfundingProgress(@PathVariable long id, @PathVariable long marketingId) {
        return crowdfundingProgressService.deleteCrowdfundingProgress(id, marketingId, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 根据众筹进度id查询众筹进度
     *
     * @param id          众筹进度id
     * @param marketingId 促销id
     * @return 返回众筹进度信息
     */
    @GetMapping("/crowdfunding/schedule/{id}/{marketingId}")
    @ApiOperation(value = "根据众筹进度id查询众筹进度", notes = "根据众筹进度id查询众筹进度（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "众筹进度id"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "marketingId", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回众筹进度信息", response = CrowdfundingProgress.class)
    })
    public CrowdfundingProgress queryCrowdfundingProgressById(@PathVariable long id, @PathVariable long marketingId) {
        return crowdfundingProgressService.queryCrowdfundingProgressById(id, marketingId, CommonConstant.ADMIN_STOREID);
    }


    /**
     * 修改众筹进度
     *
     * @param crowdfundingProgress 众筹进度实体
     * @return 成功返回1 失败返回0
     */
    @PutMapping("/crowdfunding/schedule")
    @ApiOperation(value = "修改众筹进度", notes = "修改众筹进度（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int updateCrowdfundingProgress(@RequestBody CrowdfundingProgress crowdfundingProgress) {
        return crowdfundingProgressService.updateCrowdfundingProgress(crowdfundingProgress, CommonConstant.ADMIN_STOREID);
    }


    /**
     * 分页查询商品列表
     *
     * @param pageHelper         分页帮助类
     * @param spuSearchCondition 查询参数
     * @return 商品集合
     */
    @GetMapping("/crowdfunding/spus")
    @ApiOperation(value = "分页查询商品列表", notes = "分页查询商品列表（需要认证）")
    public BaseResponse querySimpleSpus(@ApiIgnore PageHelper<PmsGoods> pageHelper, @ApiIgnore SpuSearchCondition spuSearchCondition) {
        spuSearchCondition.setStoreId(CommonConstant.ADMIN_STOREID);
        spuSearchCondition.setIsVirtual(0);
        return BaseResponse.build(spuService.querySimpleSpus(pageHelper, spuSearchCondition));
    }


    /**
     * 根据商品id查询单品信息
     *
     * @param spuId 商品id
     * @return 单品集合
     */
    @GetMapping("/crowdfunding/skus/{spuId}")
    @ApiOperation(value = "根据商品id查询单品信息", notes = "根据商品id查询单品信息（需要认证）")
    public List<PmsSku> querySkusBySpuId(@PathVariable long spuId) {
        return skuService.querySkuBySpuId(spuId, CommonConstant.ADMIN_STOREID, PmsSkuItem.SPEC);
    }

    /**
     * 查询众筹促销分类（众筹用）
     *
     * @return 返回促销分类信息
     */
    @GetMapping("/crowdfunding/cates")
    @ApiOperation(value = "查询众筹促销分类（众筹用）", notes = "查询众筹促销分类（众筹用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('crowdfunding/querymarketingcatesbytypeforcrowdfunding')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回促销分类信息", response = MarketingCate.class)
    })
    public List<MarketingCate> queryMarketingCatesByTypeForCrowdfunding() {
        return marketingCateService.queryMarketingCatesByType("5");
    }

    /**
     * 查询交叉时间内含有相同单品的互斥促销数量（众筹用）
     *
     * @param marketing 促销信息
     * @return 交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品
     */
    @PostMapping("/crowdfunding/exclusion")
    @ApiOperation(value = "查询交叉时间内含有相同单品的互斥促销数量（众筹用）", notes = "查询交叉时间内含有相同单品的互斥促销数量（众筹用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('crowdfunding/queryexclusionmarketingcountforcrowdfunding')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品", response = Integer.class)
    })
    public int queryExclusionMarketingCountForCrowdfunding(@RequestBody Marketing marketing) {
        return marketingQueryService.queryExclusionMarketingCount(marketing);
    }


    /**
     * 添加众筹促销
     *
     * @param marketing 众筹促销
     * @return 成功返回1 失败返回0
     */
    @PostMapping("/crowdfunding")
    @ApiOperation(value = "添加众筹促销", notes = "添加众筹促销（需要认证）")
    @PreAuthorize("@ss.hasPermi('crowdfunding/addcrowdfundingmarketing')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int addCrowdFundingMarketing(@RequestBody Marketing marketing) {
        return crowdFundingService.addMarketing(marketing.setAddMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.CROWD_FUNDING_MARKETING));
    }


    /**
     * 根据促销id查询众筹促销信息
     *
     * @param id 促销id
     * @return 返回促销详情信息
     */
    @GetMapping("/crowdfunding/{id}")
    @ApiOperation(value = "根据促销id查询直降促销信息", notes = "根据促销id查询直降促销信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('crowdfunding/querycrowdfundingmarketingbyid')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回促销详情信息", response = Marketing.class)
    })
    public Marketing queryCrowdfundingMarketingById(@PathVariable long id) {
        return marketingQueryService.queryMarketingById(id, CommonConstant.QUERY_WITH_NO_STORE);
    }


    /**
     * 查询众筹促销分类（修改众筹用）
     *
     * @return 返回促销分类信息
     */
    @GetMapping("/crowdfunding/update/cates")
    @ApiOperation(value = "查询众筹促销分类（修改众筹用）", notes = "查询众筹促销分类（众筹用）（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回促销分类信息", response = MarketingCate.class)
    })
    public List<MarketingCate> queryMarketingCatesByTypeForCrowdfundingUpdate() {
        return marketingCateService.queryMarketingCatesByType("5");
    }


    /**
     * 分页查询商品列表（修改众筹用）
     *
     * @param pageHelper         分页帮助类
     * @param spuSearchCondition 查询参数
     * @return 商品集合
     */
    @GetMapping("/crowdfunding/update/spus")
    @ApiOperation(value = "分页查询商品列表（修改众筹用）", notes = "分页查询商品列表（需要认证）")

    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "id", value = "商品编号"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "商品名称"),
    })

    public BaseResponse querySimpleSpusUpdate(@ApiIgnore PageHelper<PmsGoods> pageHelper, @ApiIgnore SpuSearchCondition spuSearchCondition) {
        spuSearchCondition.setStoreId(CommonConstant.ADMIN_STOREID);
        spuSearchCondition.setIsVirtual(0);
        return BaseResponse.build(spuService.querySimpleSpus(pageHelper, spuSearchCondition));
    }


    /**
     * 根据商品id查询单品信息（修改众筹用）
     *
     * @param spuId 商品id
     * @return 单品集合
     */
    @GetMapping("/crowdfunding/update/skus/{spuId}")
    @ApiOperation(value = "根据商品id查询单品信息", notes = "根据商品id查询单品信息（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "spuId", value = "商品id"),
    })

    public List<PmsSku> querySkusBySpuIdUpdate(@PathVariable long spuId) {
        return skuService.querySkuBySpuId(spuId, CommonConstant.ADMIN_STOREID, PmsSkuItem.SPEC);
    }


    /**
     * 查询交叉时间内含有相同单品的互斥促销数量（修改众筹用）
     *
     * @param marketing 促销信息
     * @return 交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品
     */
    @PostMapping("/crowdfunding/update/exclusion")
    @ApiOperation(value = "查询交叉时间内含有相同单品的互斥促销数量（修改众筹用）", notes = "查询交叉时间内含有相同单品的互斥促销数量（众筹用）（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品", response = Integer.class)
    })
    public int queryExclusionMarketingCountForCrowdfundingUpdate(@RequestBody Marketing marketing) {
        return marketingQueryService.queryExclusionMarketingCount(marketing);
    }


    /**
     * 修改众筹促销
     *
     * @param marketing 促销信息
     * @return 成功返回1 失败返回0
     */
    @PutMapping("/crowdfunding")
    @ApiOperation(value = "修改众筹促销", notes = "修改众筹促销（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int updateCrowdFunding(@RequestBody Marketing marketing) {
        int res;
        try {
            res = crowdFundingService.updateMarketing(marketing.setUpdateMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.CROWD_FUNDING_MARKETING));
        } catch (RuntimeException e) {
            res = -1;
        }
        return res;
    }


    /**
     * 根据促销id查询众筹促销信息(查看详情用)
     *
     * @param id 促销id
     * @return 返回促销详情信息
     */
    @GetMapping("/crowdfunding/detail/{id}")
    @ApiOperation(value = "根据促销id查询直降促销信息(查看详情用)", notes = "根据促销id查询直降促销信息(查看详情用)（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回促销详情信息", response = Marketing.class)
    })
    public Marketing queryCrowdfundingMarketingByIdDetail(@PathVariable long id) {
        return marketingQueryService.queryMarketingById(id, CommonConstant.QUERY_WITH_NO_STORE);
    }


    /**
     * 查询众筹促销分类（众筹详情用）
     *
     * @return 返回促销分类信息
     */
    @GetMapping("/crowdfunding/detail/cates")
    @ApiOperation(value = "查询众筹促销   分类（众筹详情用）", notes = "查询众筹促销分类（众筹详情用）（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回促销分类信息", response = MarketingCate.class)
    })
    public List<MarketingCate> queryMarketingCatesByTypeForCrowdfundingDetail() {
        return marketingCateService.queryMarketingCatesByType("5");
    }


    /**
     * 分页查询众筹订单
     *
     * @param pageHelper    分页帮助类
     * @param queryCriteria 查询条件
     * @return 返回订单信息
     */
    @GetMapping("/crowdfunding/orders")
    @ApiOperation(value = "分页查询众筹订单", notes = "分页查询众筹订单（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "marketingId", value = "促销id"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "status", value = "订单状态"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "orderCode", value = "订单号"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "customerName", value = "用户名"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "orderType", value = "订单类型 0 普通订单 1 定金预售订单 2全款预售订单"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "startTime", value = "开始时间"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "endTime", value = "结束时间"),
    })

    public BaseResponse queryCrowdFundingOrders(@ApiIgnore PageHelper<OmsOrder> pageHelper, @ApiIgnore QueryOrderCriteria queryCriteria) {
        queryCriteria.setStoreId(CommonConstant.QUERY_WITH_NO_STORE);
        return BaseResponse.build(orderService.queryCrowdFundingOrders(pageHelper, queryCriteria));
    }

    /**
     * 确认众筹订单的付款
     *
     * @param id 订单id
     * @return 成功返回1 失败返回0
     */
    @PutMapping("/crowdfunding/confirmorder/{id}")
    @ApiOperation(value = "确认众筹订单的付款", notes = "确认众筹订单的付款（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "订单id"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "reason", value = "原因"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int confirmOrder(@PathVariable long id, String reason) {
        return IOrderApiService.confirmOrderPayed(ConfirmOrderParams.buildManagerSource(id, CommonConstant.ADMIN_STOREID, reason, AdminLoginUtils.getInstance().getManagerName()));
    }

    /**
     * 查询店铺使用的物流信息
     *
     * @return 返回订单物流模版信息
     */
    @GetMapping("/crowdfunding/logisticscompanys")
    @ApiOperation(value = "查询众筹订单的物流模版信息", notes = "查询众筹订单的物流模版信息（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回订单物流模版信息", response = OmsLogisticsCompany.class)
    })
    public List<OmsLogisticsCompany> queryLogisticsCompanys() {
        return logisticsCompanyService.queryStoreUseCompanys(CommonConstant.ADMIN_STOREID);
    }

    /**
     * 众筹订单发货
     *
     * @param id          订单id
     * @param waybillCode 运单号
     * @param companyId   物流公司id
     * @return 成功返回1 失败返回0 -2 订单正在申请退款 -1 订单号含有中文 -3 拼团未成功  -4 众筹还未成功 -5 拼团没成功
     */
    @PutMapping("/crowdfunding/deliverorder/{id}/{waybillCode}/{companyId}")
    @ApiOperation(value = "众筹订单发货", notes = "众筹订单发货（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "订单id"),
            @ApiImplicitParam(paramType = "path", dataType = "string", name = "waybillCode", value = "运单号"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "companyId", value = "物流公司id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回订单物流模版信息", response = Integer.class)
    })
    public int deliverOrder(@PathVariable long id, @PathVariable String waybillCode, @PathVariable long companyId) {
        return IOrderApiService.deliverOrder(id, CommonConstant.ADMIN_STOREID, waybillCode, AdminLoginUtils.getInstance().getManagerName(), companyId);
    }

    /**
     * 取消众筹订单
     *
     * @param id 订单id
     * @return 1 成功 -1 订单不存在 -2:用户不匹配 0 失败
     */
    @DeleteMapping("/crowdfunding/order/{id}")
    @ApiOperation(value = "取消众筹订单", notes = "取消众筹订单（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "订单id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "1 成功 -1 订单不存在 -2:用户不匹配 0 失败", response = Integer.class)
    })
    public int cancelOrder(@PathVariable long id) {
        return IOrderApiService.cancleOrder(CancelOrderParams.buildManagerSource(CommonConstant.ADMIN_STOREID, id, AdminLoginUtils.getInstance().getManagerName()));
    }

    /**
     * 导出所有众筹订单信息
     *
     * @param status 订单状态
     * @throws IOException
     */
    @PostMapping("/crowdfunding/exportallorder")
    @ApiOperation(value = "导出所有众筹订单信息", notes = "导出所有众筹订单信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "status", value = "订单状态"),
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "marketingId", value = "促销id"),
    })
    public void exportAllOrder(HttpServletResponse response, String status, Long marketingId) throws IOException {
        OutputStream os = response.getOutputStream();
        //  ExcelUtils.exportExcel(response, String.valueOf("订单信息_" + System.currentTimeMillis()).concat(".xls"), () -> IOrderApiService.exportAllOrder(os, status, CommonConstant.ADMIN_STOREID, marketingId));
    }


    /**
     * 导出选中的众筹订单信息
     *
     * @param status 订单状态
     * @param ids    订单id 数组
     * @throws IOException
     */
    @PostMapping("/crowdfunding/exportcheckedorder")
    @ApiOperation(value = "导出选中的众筹订单信息", notes = "导出选中的众筹订单信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "status", value = "订单状态"),
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "订单id数组"),
    })
    public void exportCheckedOrder(HttpServletResponse response, String status, Long... ids) throws IOException {
       /* OutputStream os = response.getOutputStream();
        ExcelUtils.exportExcel(response, String.valueOf("订单信息_" + System.currentTimeMillis()).concat(".xls"), () ->
                IOrderApiService.exportCheckedOrder(os, status, ids, CommonConstant.ADMIN_STOREID)
        );*/
    }


    /**
     * 根据订单id查询众筹订单信息  (订单的所有信息 此接口慎用)
     *
     * @param id 订单id
     * @return 返回订单信息
     */
    @GetMapping("/crowdfunding/order/{id}")
    @ApiOperation(value = "根据订单id查询众筹订单信息  (订单的所有信息 此接口慎用)", notes = "根据订单id查询众筹订单信息  (订单的所有信息 此接口慎用)（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "订单id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回订单信息", response = OmsOrder.class)
    })
    public OmsOrder queryCrowdfundingOrderById(@PathVariable long id) {
        return IOrderApiService.queryOrderDetailById(id, CommonConstant.QUERY_WITH_NO_STORE, CommonConstant.QUERY_WITH_NO_CUSTOMER, OrderItem.LOG, OrderItem.ATTR, OrderItem.SKUS);
    }


    /**
     * 修改众筹订单物流单号
     *
     * @param id          订单id
     * @param waybillCode 运单号
     * @param companyId   物流公司id
     * @return 成功返回1 失败返回0 -2 订单正在申请退款 -1 订单号含有中文 -6 物流公司不存在
     */
    @PutMapping("/crowdfunding/changeexpressno/{id}/{waybillCode}/{companyId}")
    @ApiOperation(value = "修改众筹订单物流单号", notes = "修改众筹订单物流单号（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "订单id"),
            @ApiImplicitParam(paramType = "path", dataType = "string", name = "waybillCode", value = "运单号"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "companyId", value = "物流公司id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0 -2 订单正在申请退款 -1 订单号含有中文 -6 物流公司不存在", response = Integer.class)
    })
    public int changeExpressNo(@PathVariable long id, @PathVariable String waybillCode, @PathVariable long companyId) {
        return orderService.changeExpressNo(id, CommonConstant.ADMIN_STOREID, waybillCode, AdminLoginUtils.getInstance().getManagerName(), companyId);
    }

}
