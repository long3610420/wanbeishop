package com.ruoyi.web.controller.marketing;


import com.ruoyi.marketing.domain.SeckillScene;
import com.ruoyi.marketing.service.SeckillSceneService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by 魔金商城平台管理on 2020/5/12.
 * 秒杀场次控制器
 */
@RestController
@Api("秒杀场次接口")
public class SeckillSceneController {

    /**
     * 注入秒杀场次服务接口
     */
    @Autowired
    private SeckillSceneService seckillSceneService;

    /**
     * 查询秒杀场次
     *
     * @param time 时间
     * @return 返回秒杀场次
     */
    @GetMapping("/seckillscenes/{time}")
    @ApiOperation(value = "查询秒杀场次", notes = "查询秒杀场次（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "time", value = "秒杀场次时间"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回秒杀场次", response = SeckillScene.class)
    })
    public List<SeckillScene> querySeckillScenes(@PathVariable String time) {
        return seckillSceneService.querySeckillSceneByTime(time);
    }

    /**
     * 更新秒杀场次
     *
     * @param seckillScenes 秒杀场次
     * @param seckillTime   秒杀场次时间
     * @return 成功返回1 失败==0 -1 秒杀场次不能超过12场
     */
    @PutMapping("/seckillscene/{seckillTime}")
    @ApiOperation(value = "查询秒杀场次", notes = "查询秒杀场次（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "seckillTime", value = "秒杀场次时间"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败==0 -1 秒杀场次不能超过12场", response = Integer.class)
    })
    public int updateSeckillScene(@RequestBody List<SeckillScene> seckillScenes, @PathVariable String seckillTime) {
        return seckillSceneService.updateSeckillScenes(seckillScenes, seckillTime);
    }

    /**
     * 分页查询秒杀场次活动
     *
     * @param pageHelper  分页帮助类
     * @param type        类型 1 即将开始 2 进行中 3 已结束
     * @param seckillTime 秒杀时间
     * @return 返回秒杀场次活动
     */
    @GetMapping("seckillscene/list")
    @ApiOperation(value = "分页查询秒杀场次活动", notes = "分页查询秒杀场次活动（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "type", value = "类型 1 即将开始 2 进行中 3 已结束"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "seckillTime", value = "秒杀时间"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回秒杀场次活动", response = SeckillScene.class)
    })
    public BaseResponse querySeckillSceneLists(PageHelper<SeckillScene> pageHelper, String type, String seckillTime) {
        return BaseResponse.build(seckillSceneService.querySeckillScenes(pageHelper, type, seckillTime, CommonConstant.ADMIN_STOREID));
    }

}
