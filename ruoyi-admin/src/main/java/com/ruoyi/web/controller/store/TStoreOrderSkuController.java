package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStoreOrderSku;
import com.ruoyi.store.service.ITStoreOrderSkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 门店订单单品信息Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStoreOrderSku")
public class TStoreOrderSkuController extends BaseController {
    @Autowired
    private ITStoreOrderSkuService tStoreOrderSkuService;

    /**
     * 查询门店订单单品信息列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderSku:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStoreOrderSku tStoreOrderSku) {
        startPage();
        List<TStoreOrderSku> list = tStoreOrderSkuService.selectTStoreOrderSkuList(tStoreOrderSku);
        return getDataTable(list);
    }

    /**
     * 导出门店订单单品信息列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderSku:export')")
    @Log(title = "门店订单单品信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStoreOrderSku tStoreOrderSku) {
        List<TStoreOrderSku> list = tStoreOrderSkuService.selectTStoreOrderSkuList(tStoreOrderSku);
        ExcelUtil<TStoreOrderSku> util = new ExcelUtil<TStoreOrderSku>(TStoreOrderSku.class);
        return util.exportExcel(list, "TStoreOrderSku");
    }

    /**
     * 获取门店订单单品信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderSku:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStoreOrderSkuService.selectTStoreOrderSkuById(id));
    }

    /**
     * 新增门店订单单品信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderSku:add')")
    @Log(title = "门店订单单品信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStoreOrderSku tStoreOrderSku) {
        return toAjax(tStoreOrderSkuService.insertTStoreOrderSku(tStoreOrderSku));
    }

    /**
     * 修改门店订单单品信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderSku:edit')")
    @Log(title = "门店订单单品信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStoreOrderSku tStoreOrderSku) {
        return toAjax(tStoreOrderSkuService.updateTStoreOrderSku(tStoreOrderSku));
    }

    /**
     * 删除门店订单单品信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderSku:remove')")
    @Log(title = "门店订单单品信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStoreOrderSkuService.deleteTStoreOrderSkuByIds(ids));
    }
}
