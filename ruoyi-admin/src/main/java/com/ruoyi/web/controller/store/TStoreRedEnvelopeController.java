package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStoreRedEnvelope;
import com.ruoyi.store.service.ITStoreRedEnvelopeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 门店红包Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStoreRedEnvelope")
public class TStoreRedEnvelopeController extends BaseController {
    @Autowired
    private ITStoreRedEnvelopeService tStoreRedEnvelopeService;

    /**
     * 查询门店红包列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreRedEnvelope:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStoreRedEnvelope tStoreRedEnvelope) {
        startPage();
        List<TStoreRedEnvelope> list = tStoreRedEnvelopeService.selectTStoreRedEnvelopeList(tStoreRedEnvelope);
        return getDataTable(list);
    }

    /**
     * 导出门店红包列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreRedEnvelope:export')")
    @Log(title = "门店红包", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStoreRedEnvelope tStoreRedEnvelope) {
        List<TStoreRedEnvelope> list = tStoreRedEnvelopeService.selectTStoreRedEnvelopeList(tStoreRedEnvelope);
        ExcelUtil<TStoreRedEnvelope> util = new ExcelUtil<TStoreRedEnvelope>(TStoreRedEnvelope.class);
        return util.exportExcel(list, "TStoreRedEnvelope");
    }

    /**
     * 获取门店红包详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreRedEnvelope:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStoreRedEnvelopeService.selectTStoreRedEnvelopeById(id));
    }

    /**
     * 新增门店红包
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreRedEnvelope:add')")
    @Log(title = "门店红包", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStoreRedEnvelope tStoreRedEnvelope) {
        return toAjax(tStoreRedEnvelopeService.insertTStoreRedEnvelope(tStoreRedEnvelope));
    }

    /**
     * 修改门店红包
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreRedEnvelope:edit')")
    @Log(title = "门店红包", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStoreRedEnvelope tStoreRedEnvelope) {
        return toAjax(tStoreRedEnvelopeService.updateTStoreRedEnvelope(tStoreRedEnvelope));
    }

    /**
     * 删除门店红包
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreRedEnvelope:remove')")
    @Log(title = "门店红包", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStoreRedEnvelopeService.deleteTStoreRedEnvelopeByIds(ids));
    }
}
