package com.ruoyi.web.controller.marketing;


import com.ruoyi.goods.domain.PmsSku;
import com.ruoyi.goods.service.IPmsSkuService;
import com.ruoyi.marketing.domain.CombinationSku;
import com.ruoyi.marketing.domain.GoodsCombination;
import com.ruoyi.marketing.service.CombinationSkuService;
import com.ruoyi.marketing.service.GoodsCombinationService;
import com.ruoyi.marketing.service.MarketingQueryService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Arrays;
import java.util.List;

/**
 * Created by 魔金商城平台管理on 2019/7/11.
 * 商品组合接口
 */
@RestController
@Api("商品组合接口")
public class GoodsCombinationController {

    /**
     * 自动注入商品组合service
     */
    @Autowired
    private GoodsCombinationService goodsCombinationService;


    /**
     * 自动注入商品组合与单品的关联service
     */
    @Autowired
    private CombinationSkuService combinationSkuService;


    /**
     * 注入单品服务
     */
    @Autowired
    private IPmsSkuService skuService;

    /**
     * 注入促销查询接口
     */
    @Autowired
    private MarketingQueryService marketingQueryService;


    /**
     * 查询商品组合
     *
     * @param pageHelper 分页帮助类
     * @param name       商品组合名称
     * @return 返回商品组合信息
     */
    @GetMapping("/goodscombination")
    @ApiOperation(value = "查询商品组合", notes = "查询商品组合（需要认证）")
    @PreAuthorize("@ss.hasPermi('combinedlist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "商品组合名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回商品组合信息", response = GoodsCombination.class)
    })
    public BaseResponse queryGoodsCombination(@ApiIgnore PageHelper<GoodsCombination> pageHelper, String name) {
        return BaseResponse.build(goodsCombinationService.queryGoodsCombination(pageHelper, name, CommonConstant.ADMIN_STOREID));
    }

    /**
     * 批量删除商品组合
     *
     * @param ids 商品组合id数组
     * @return 成功返回>=1，失败返回0
     */
    @DeleteMapping("/goodscombination")
    @ApiOperation(value = "批量删除商品组合", notes = "批量删除商品组合（需要认证）")
    @PreAuthorize("@ss.hasPermi('combinedlist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "商品组合id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回>=1，失败返回0", response = Integer.class)
    })
    public int deleteGoodsCombination(long[] ids) {
        return goodsCombinationService.batchDeleteGoodsCombination(ids, CommonConstant.ADMIN_STOREID);
    }

    /**
     * 添加商品组合
     *
     * @param goodsCombination 商品组合
     * @return 成功返回1，失败返回0 -1存在互斥促销
     */
    @PostMapping("/goodscombination")
    @ApiOperation(value = "添加商品组合", notes = "添加商品组合（需要认证）")
    @PreAuthorize("@ss.hasPermi('combinedlist')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1，失败返回0 -1存在互斥促销", response = Integer.class)
    })
    public int addGoodsCombination(@RequestBody GoodsCombination goodsCombination) {
        goodsCombination.setStoreId(CommonConstant.ADMIN_STOREID);
        return goodsCombinationService.addGoodsCombination(goodsCombination);
    }


    /**
     * 修改商品组合
     *
     * @param goodsCombination 商品组合
     * @return 成功返回1，失败返回0
     */
    @PutMapping("/goodscombination")
    @ApiOperation(value = "修改商品组合", notes = "修改商品组合（需要认证）")
    @PreAuthorize("@ss.hasPermi('combinedlist')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1，失败返回0", response = Integer.class)
    })
    public int updateGoodsCombination(@RequestBody GoodsCombination goodsCombination) {
        goodsCombination.setStoreId(CommonConstant.ADMIN_STOREID);
        return goodsCombinationService.updateGoodsCombination(goodsCombination);
    }


    /**
     * 通过id查找商品组合
     *
     * @param id 商品组合id
     * @return 商品组合
     */
    @GetMapping("/goodscombination/{id}")
    @ApiOperation(value = "通过id查找商品组合", notes = "通过id查找商品组合（需要认证）")
    @PreAuthorize("@ss.hasPermi('combinedlist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "商品组合id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "商品组合", response = GoodsCombination.class)
    })
    public GoodsCombination queryGoodsCombinationById(@PathVariable long id) {
        return goodsCombinationService.queryGoodsCombinationById(id, CommonConstant.ADMIN_STOREID);
    }


    /**
     * 分页查找商品组合下的商品
     *
     * @param pageHelper 分页帮助类
     * @param id         商品组合id
     * @return 商品组合下的商品
     */
    @GetMapping("/goodscombination/skus/{id}")
    @ApiOperation(value = "查找商品组合下的商品", notes = "查找商品组合下的商品（需要认证）")
    @PreAuthorize("@ss.hasPermi('combinedlist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "商品组合id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "商品组合下的商品", response = CombinationSku.class)
    })
    public BaseResponse queryGoodsOfGoodsCombination(@ApiIgnore PageHelper<CombinationSku> pageHelper, @PathVariable Long id) {
        return BaseResponse.build(combinationSkuService.querySkuOfGoodCom(pageHelper, id, CommonConstant.ADMIN_STOREID));
    }


    /**
     * 批量删除商品组合下的单品
     *
     * @param ids           单品id数组
     * @param combinationId 商品组合id
     * @return 成功返回>=1，失败返回0
     */
    @DeleteMapping("/goodscombination/skus")
    @ApiOperation(value = "批量删除商品组合下的单品", notes = "批量删除商品组合下的单品（需要认证）")
    @PreAuthorize("@ss.hasPermi('combinedlist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "单品id数组"),
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "combinationId", value = "商品组合id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回>=1，失败返回0", response = Integer.class)
    })
    public int deleteCombinationSku(String[] ids, long combinationId) {
        return combinationSkuService.batchDeleteCombiantionSKu(ids, combinationId);
    }


    /**
     * 分页查询单品信息（商品组合用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回单品信息(包括规格信息)
     */
    @GetMapping("/goodscombination/skus")
    @ApiOperation(value = "分页查询单品信息（商品组合用）", notes = "分页查询单品信息（商品组合用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('combinedlist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForGoodsCombination(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }

    /**
     * 含有相同单品的有效互斥促销数量
     *
     * @param skuIds 促销信息
     * @return 含有相同单品的有效互斥促销数量
     */
    @GetMapping("/goodscombination/exclusionmarketingcountbyskuid")
    @ApiOperation(value = "含有相同单品的有效互斥促销数量", notes = "含有相同单品的有效互斥促销数量（需要认证）")
    @PreAuthorize("@ss.hasPermi('combinedlist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "skuIds", value = "促销信息"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "含有相同单品的有效互斥促销数量", response = Integer.class)
    })
    public int queryExclusionMarketingCountBySkuId(String[] skuIds) {
        return marketingQueryService.queryExclusionMarketingCountBySkuIds(Arrays.asList(skuIds), CommonConstant.COMBINATION_EXCLUSION);
    }

    /**
     * 为商品组合添加单品
     *
     * @param combinationSkus 商品组合下的单品
     * @return 成功返回1，失败返回0
     */
    @PostMapping("/goodscombination/skus")
    @ApiOperation(value = "为商品组合添加单品", notes = "为商品组合添加单品（需要认证）")
    @PreAuthorize("@ss.hasPermi('combinedlist')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1，失败返回0", response = Integer.class)
    })
    public int addCombinationSku(@RequestBody List<CombinationSku> combinationSkus) {
        return combinationSkuService.addCombinationSkus(combinationSkus);
    }


}
