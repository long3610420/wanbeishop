package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStoreInfo;
import com.ruoyi.store.service.ITStoreInfoService;
import com.ruoyi.store.vo.StoreBusinessInfo;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 店铺信息Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStoreInfo")
public class TStoreInfoController extends BaseController {
    @Autowired
    private ITStoreInfoService tStoreInfoService;

    /**
     * 查询店铺信息列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStoreInfo tStoreInfo) {
        startPage();
        List<TStoreInfo> list = tStoreInfoService.selectTStoreInfoList(tStoreInfo);
        return getDataTable(list);
    }

    /**
     * 导出店铺信息列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreInfo:export')")
    @Log(title = "店铺信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStoreInfo tStoreInfo) {
        List<TStoreInfo> list = tStoreInfoService.selectTStoreInfoList(tStoreInfo);
        ExcelUtil<TStoreInfo> util = new ExcelUtil<TStoreInfo>(TStoreInfo.class);
        return util.exportExcel(list, "TStoreInfo");
    }

    /**
     * 获取店铺信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStoreInfoService.selectTStoreInfoById(id));
    }

    /**
     * 新增店铺信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreInfo:add')")
    @Log(title = "店铺信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStoreInfo tStoreInfo) {
        return toAjax(tStoreInfoService.insertTStoreInfo(tStoreInfo));
    }

    /**
     * 修改店铺信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreInfo:edit')")
    @Log(title = "店铺信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStoreInfo tStoreInfo) {
        return toAjax(tStoreInfoService.updateTStoreInfo(tStoreInfo));
    }

    /**
     * 删除店铺信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreInfo:remove')")
    @Log(title = "店铺信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStoreInfoService.deleteTStoreInfoByIds(ids));
    }

    /**
     * 批量关店
     *
     * @param ids 店铺id
     * @return 成功返回>0 失败返回0
     */
    @PutMapping("/storelist/close")
    @ApiOperation(value = "批量关店", notes = "批量关店（需要认证）")
    @PreAuthorize("@ss.hasPermi('storelist/closestoresforstore')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "店铺id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回>0 失败返回0", response = Integer.class)
    })
    public int closeStores(Long[] ids) {
        return tStoreInfoService.closeStores(Arrays.asList(ids), null);
    }


    /**
     * 开启门店
     *
     * @param storeId 店铺id
     * @return 成功返回>0 失败返回0
     */
    @PutMapping("/storelist/open/{storeId}")
    @ApiOperation(value = "开启门店", notes = "开启门店（需要认证）")
    @PreAuthorize("@ss.hasPermi('storelist/openstoreforoutletstore')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "店铺id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回>0 失败返回0", response = Integer.class)
    })
    public int openStoreForOutLetStore(@PathVariable long storeId) {
        return tStoreInfoService.openStoreForOutLetStore(storeId);
    }

    /**
     * 根据店铺id查询店铺详情信息
     *
     * @param storeId 店铺id
     * @return 店铺详情信息
     */
    @GetMapping("/shopdetails")
    @PreAuthorize("@ss.hasPermi('shopdetails/storedetailinfobystoreid')")
    @ApiOperation(value = "根据店铺id查询店铺详情信息", notes = "根据店铺id查询店铺详情信息（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "storeId", value = "店铺id"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "status", value = "品牌状态 状态  0 申请中  1通过 2 拒绝"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "店铺详情信息", response = StoreBusinessInfo.class)
    })
    public StoreBusinessInfo storeDetailInfoByStoreId(long storeId, String status) {
        return tStoreInfoService.queryStoreBusinessInfo(storeId, status);
    }
}
