package com.ruoyi.web.controller.marketing;


import com.ruoyi.goods.domain.PmsSku;
import com.ruoyi.goods.service.IPmsSkuService;
import com.ruoyi.marketing.domain.Marketing;
import com.ruoyi.marketing.domain.MarketingCate;
import com.ruoyi.marketing.domain.MarketingPic;
import com.ruoyi.marketing.service.*;
import com.ruoyi.util.*;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

/**
 * Created by 魔金商城平台管理on 2019/7/29.
 * 预售控制器
 */
@RestController
@Api("预售接口")
public class PreSaleController {


    /**
     * 注入促销查询接口
     */
    @Autowired
    private MarketingQueryService marketingQueryService;


    /**
     * 注入促销删除服务接口
     */
    @Resource(name = "marketingDeleteService")
    private MarketingService marketingDeleteService;

    /**
     * 注入单品服务
     */
    @Autowired
    private IPmsSkuService skuService;

    /**
     * 注入促销混合api接口
     */
    @Autowired
    private MarketingServiceApi marketingServiceApi;

    /**
     * 注入促销图片服务接口
     */
    @Autowired
    private MarketingPicService marketingPicService;

    /**
     * 注入促销分类服务接口
     */
    @Autowired
    private MarketingCateService marketingCateService;


    /**
     * 分页查询定金预售促销信息
     *
     * @param pageHelper 分页帮助类
     * @param name       名称
     * @return 返回促销信息
     */
    @GetMapping("/depositpresale")
    @ApiOperation(value = "分页查询定金预售促销信息", notes = "分页查询定金预售促销信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('presale')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "定金预售名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回促销信息", response = Marketing.class)
    })
    public BaseResponse queryDepositPresale(@ApiIgnore PageHelper<Marketing> pageHelper, String name) {
        return BaseResponse.build(marketingQueryService.queryMarketings(pageHelper, name, "7", CommonConstant.ADMIN_STOREID, ""));
    }

    /**
     * 分页查询全款预售促销信息
     *
     * @param pageHelper 分页帮助类
     * @param name       名称
     * @return 返回促销信息
     */
    @GetMapping("/fullpresale")
    @ApiOperation(value = "分页查询全款预售促销信息", notes = "分页查询全款预售促销信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('presale')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "全款预售名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回促销信息", response = Marketing.class)
    })
    public BaseResponse queryFullPresale(@ApiIgnore PageHelper<Marketing> pageHelper, String name) {
        return BaseResponse.build(marketingQueryService.queryMarketings(pageHelper, name, "8", CommonConstant.ADMIN_STOREID, ""));
    }


    /**
     * 批量删除定金预售促销
     *
     * @param marketingIds 促销id集合
     * @return 成功返回>0 否则返回0
     */
    @DeleteMapping("/depositpresale")
    @ApiOperation(value = "批量删除定金预售促销", notes = "批量删除定金预售促销（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "marketingIds", value = "促销id集合"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回>0 否则返回0", response = Integer.class)
    })
    public int deleteDepositPresaleByIds(Long[] marketingIds) {
        return marketingDeleteService.deleteMarketings(marketingIds, CommonConstant.ADMIN_STOREID, MarketingConstant.DEPOSIT_PRE_SALE_MARKETING);
    }

    /**
     * 批量删除全款预售促销
     *
     * @param marketingIds 促销id集合
     * @return 成功返回>0 否则返回0
     */
    @DeleteMapping("/fullpresale")
    @ApiOperation(value = "批量删除全款预售促销", notes = "批量删除全款预售促销（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "marketingIds", value = "促销id集合"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回>0 否则返回0", response = Integer.class)
    })
    public int deleteFullPresaleByIds(Long[] marketingIds) {
        return marketingDeleteService.deleteMarketings(marketingIds, CommonConstant.ADMIN_STOREID, MarketingConstant.FULL_PRE_SALE_MARKETING);
    }

    /**
     * 分页查询单品信息（定金预售用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回单品信息(包括规格信息)
     */
    @GetMapping("/depositpresale/skus")
    @ApiOperation(value = "分页查询单品信息（定金预售用）", notes = "分页查询单品信息（定金预售用）（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForDepositPresale(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }

    /**
     * 查询交叉时间内含有相同单品的互斥促销数量（定金预售用）
     *
     * @param marketing 促销信息
     * @return 交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品
     */
    @PostMapping("/depositpresale/exclusion")
    @ApiOperation(value = "查询交叉时间内含有相同单品的互斥促销数量（定金预售用）", notes = "查询交叉时间内含有相同单品的互斥促销数量（定金预售用）（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品", response = Integer.class)
    })
    public int queryExclusionMarketingCountForDepositPresale(@RequestBody Marketing marketing) {
        return marketingQueryService.queryExclusionMarketingCount(marketing);
    }

    /**
     * 添加定金预售
     *
     * @param marketing 定金预售促销
     * @return 成功返回 1 失败返回0
     */
    @PostMapping("/depositpresale")
    @ApiOperation(value = "添加定金预售", notes = "添加定金预售（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int addDepositPreSale(@RequestBody Marketing marketing) {
        return marketingServiceApi.addDepositPreSale(marketing.setAddMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.DEPOSIT_PRE_SALE_MARKETING).setStartTimeNow());
    }

    /**
     * 根据促销id查询定金预售促销信息
     *
     * @param id 促销id
     * @return 返回定金预售促销详情信息
     */
    @GetMapping("/depositpresale/{id}")
    @ApiOperation(value = "根据促销id查询定金预售促销信息", notes = "根据促销id查询定金预售促销信息（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回定金预售促销详情信息", response = Marketing.class)
    })
    public Marketing queryDepositPresaleMarketingById(@PathVariable long id) {
        return marketingQueryService.queryMarketingById(id, CommonConstant.QUERY_WITH_NO_STORE);
    }

    /**
     * 分页查询单品信息（修改定金预售用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回单品信息(包括规格信息)
     */
    @GetMapping("/depositpresale/update/skus")
    @ApiOperation(value = "分页查询单品信息（定金预售用）", notes = "分页查询单品信息（定金预售用）（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForDepositPresaleUpdate(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }


    /**
     * 查询交叉时间内含有相同单品的互斥促销数量（修改定金预售用）
     *
     * @param marketing 促销信息
     * @return 交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品
     */
    @PostMapping("/depositpresale/update/exclusion")
    @ApiOperation(value = "查询交叉时间内含有相同单品的互斥促销数量（修改定金预售用）", notes = "查询交叉时间内含有相同单品的互斥促销数量（修改定金预售用）（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品", response = Integer.class)
    })
    public int queryExclusionMarketingCountForDepositPresaleUpdate(@RequestBody Marketing marketing) {
        return marketingQueryService.queryExclusionMarketingCount(marketing);
    }

    /**
     * 更新定金预售
     *
     * @param marketing 促销信息
     * @return 成功返回1 失败返回0
     */
    @PutMapping("/depositpresale")
    @ApiOperation(value = "更新定金预售", notes = "更新定金预售（需要认证）", httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int updateDepositPreSale(@RequestBody Marketing marketing) {
        return marketingServiceApi.updateDepositPreSale(marketing.setUpdateMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.DEPOSIT_PRE_SALE_MARKETING));
    }

    /**
     * 分页查询单品信息（全款预售用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回单品信息(包括规格信息)
     */
    @GetMapping("/fullpresale/skus")
    @ApiOperation(value = "分页查询单品信息（全款预售用）", notes = "分页查询单品信息（全款预售用）（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForFullPresale(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }


    /**
     * 查询交叉时间内含有相同单品的互斥促销数量（全款预售用）
     *
     * @param marketing 促销信息
     * @return 交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品
     */
    @PostMapping("/fullpresale/exclusion")
    @ApiOperation(value = "查询交叉时间内含有相同单品的互斥促销数量（全款预售用）", notes = "查询交叉时间内含有相同单品的互斥促销数量（全款预售用）（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品", response = Integer.class)
    })
    public int queryExclusionMarketingCountForFullPresale(@RequestBody Marketing marketing) {
        return marketingQueryService.queryExclusionMarketingCount(marketing);
    }


    /**
     * 添加全款预售促销
     *
     * @param marketing 全款预售
     * @return 成功返回 1 失败返回0
     */
    @PostMapping("/fullpresale")
    @ApiOperation(value = "添加全款预售促销", notes = "添加全款预售促销（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int addFullPreSale(@RequestBody Marketing marketing) {
        return marketingServiceApi.addFullPreSale(marketing.setAddMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.FULL_PRE_SALE_MARKETING).setStartTimeNow());
    }


    /**
     * 根据促销id查询全款预售促销信息
     *
     * @param id 促销id
     * @return 返回全款预售促销详情信息
     */
    @GetMapping("/fullpresale/{id}")
    @ApiOperation(value = "根据促销id查询全款预售促销信息", notes = "根据促销id查询全款预售促销信息（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回全款预售促销详情信息", response = Marketing.class)
    })
    public Marketing queryFullPresaleMarketingById(@PathVariable long id) {
        return marketingQueryService.queryMarketingById(id, CommonConstant.QUERY_WITH_NO_STORE);
    }

    /**
     * 分页查询单品信息（修改全款预售用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回单品信息(包括规格信息)
     */
    @GetMapping("/fullpresale/update/skus")
    @ApiOperation(value = "分页查询单品信息（修改全款预售用）", notes = "分页查询单品信息（修改全款预售用）（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForFullPresaleUpdate(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }

    /**
     * 查询交叉时间内含有相同单品的互斥促销数量（修改全款预售用）
     *
     * @param marketing 促销信息
     * @return 交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品
     */
    @PostMapping("/fullpresale/update/exclusion")
    @ApiOperation(value = "查询交叉时间内含有相同单品的互斥促销数量（全款预售用）", notes = "查询交叉时间内含有相同单品的互斥促销数量（全款预售用）（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品", response = Integer.class)
    })
    public int queryExclusionMarketingCountForFullPresaleUpdate(@RequestBody Marketing marketing) {
        return marketingQueryService.queryExclusionMarketingCount(marketing);
    }

    /**
     * 更新全款预售
     *
     * @param marketing 全款预售
     * @return 成功返回1 失败返回0
     */
    @PutMapping("/fullpresale")
    @ApiOperation(value = "更新全款预售", notes = "更新全款预售（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int updateFullPreSale(@RequestBody Marketing marketing) {
        return marketingServiceApi.updateFullPreSale(marketing.setUpdateMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.FULL_PRE_SALE_MARKETING));
    }

    /**
     * 查询预售促销图片信息
     *
     * @return 预售促销图片信息
     */
    @GetMapping("/presalepic")
    @ApiOperation(value = "查询预售促销图片信息", notes = "查询预售促销图片信息（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "预售促销图片信息", response = MarketingPic.class)
    })
    public CommonResponse<MarketingPic> queryPreSalePic() {
        return CommonResponse.build(marketingPicService.queryMarketingPic(CommonConstant.PRESALE_MARKETING_PIC_TYPE, CommonConstant.ADMIN_STOREID));
    }

    /**
     * 保存预售促销图片
     *
     * @param marketingPic 促销图片实体
     * @return 成功1 否则失败
     */
    @PostMapping("/presalepic")
    @ApiOperation(value = "保存预售促销图片", notes = "保存预售促销图片（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int savePreSalePic(@RequestBody MarketingPic marketingPic) {
        return marketingPicService.saveMarketingPic(marketingPic.addType(CommonConstant.PRESALE_MARKETING_PIC_TYPE).addStoreId(CommonConstant.ADMIN_STOREID));
    }


    /**
     * 分页查询预售促销分类
     *
     * @param pageHelper 分页帮助类
     * @param name       促销分类名称
     * @return 促销分类列表
     */
    @GetMapping("/presale/cates")
    @ApiOperation(value = "分页查询预售促销分类", notes = "分页查询预售促销分类（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "促销分类名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "预售促销分类列表", response = MarketingCate.class)
    })
    public BaseResponse queryPreSaleCates(@ApiIgnore PageHelper<MarketingCate> pageHelper, String name) {
        return BaseResponse.build(marketingCateService.queryMarketingCates(pageHelper, name, CommonConstant.PRESALE_MARKETING_CATE, CommonConstant.ADMIN_STOREID));
    }


    /**
     * 新增预售促销分类
     *
     * @param marketingCate 促销分类信息
     * @return 成功1 否则失败
     */
    @PostMapping("/presale/cate")
    @ApiOperation(value = "新增预售促销分类", notes = "新增预售促销分类（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int addPreSaleCate(@RequestBody MarketingCate marketingCate) {
        return marketingCateService.addMarketingCate(marketingCate.buildForAdd(CommonConstant.PRESALE_MARKETING_CATE, CommonConstant.ADMIN_STOREID));
    }


    /**
     * 删除预售促销分类
     *
     * @param ids 促销分类id数组
     * @return 成功>0 否则失败
     */
    @DeleteMapping("/presale/cate")
    @ApiOperation(value = "删除预售促销分类", notes = "删除预售促销分类（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "促销分类id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功>0 否则失败", response = Integer.class)
    })
    public int deletePreSaleCate(long[] ids) {
        return marketingCateService.deleteMarketingCates(ids, CommonConstant.ADMIN_STOREID, CommonConstant.PRESALE_MARKETING_CATE);
    }

    /**
     * 根据id查询预售促销分类
     *
     * @param id 促销分类id
     * @return 预售促销分类信息
     */
    @GetMapping("/presale/cate/{id}")
    @ApiOperation(value = "根据id查询预售促销分类", notes = "根据id查询预售促销分类（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销分类id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "促销分类信息", response = MarketingCate.class)
    })
    public MarketingCate queryMarketingCateById(@PathVariable long id) {
        return marketingCateService.queryMarketingCateById(id, CommonConstant.ADMIN_STOREID);
    }


    /**
     * 修改预售促销分类
     *
     * @param marketingCate 促销分类信息
     * @return 成功1 否则失败
     */
    @PutMapping("/presale/cate")
    @ApiOperation(value = "修改预售促销分类", notes = "修改预售促销分类（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int updateMarketingCate(@RequestBody MarketingCate marketingCate) {
        marketingCate.setStoreId(CommonConstant.ADMIN_STOREID);
        return marketingCateService.updateMarketingCate(marketingCate);
    }
    
}
