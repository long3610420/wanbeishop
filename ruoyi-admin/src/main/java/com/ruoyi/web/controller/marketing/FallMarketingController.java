package com.ruoyi.web.controller.marketing;


import com.ruoyi.goods.domain.PmsSku;
import com.ruoyi.goods.service.IPmsSkuService;
import com.ruoyi.marketing.domain.Marketing;
import com.ruoyi.marketing.service.MarketingQueryService;
import com.ruoyi.marketing.service.MarketingService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.MarketingConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

/**
 * Created by 魔金商城平台管理on 2019/7/23.
 * 直降促销
 */
@RestController
@Api(description = "直降促销接口")
public class FallMarketingController {

    /**
     * 注入单品服务
     */
    @Autowired
    private IPmsSkuService skuService;


    /**
     * 注入促销查询接口
     */
    @Autowired
    private MarketingQueryService marketingQueryService;

    /**
     * 注入促销删除服务接口
     */
    @Resource(name = "marketingDeleteService")
    private MarketingService marketingDeleteService;

    /**
     * 注入直降服务接口
     */
    @Resource(name = "fallService")
    private MarketingService fallService;


    /**
     * 分页查询直降促销信息
     *
     * @param pageHelper 分页帮助类
     * @param name       名称
     * @return 返回促销信息
     */
    @GetMapping("/fall")
    @ApiOperation(value = "分页查询直降促销信息", notes = "分页查询直降促销信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "直降促销名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回促销信息", response = Marketing.class)
    })
    public BaseResponse queryFallMarketing(@ApiIgnore PageHelper<Marketing> pageHelper, String name) {
        return BaseResponse.build(marketingQueryService.queryMarketings(pageHelper, name, "1", CommonConstant.ADMIN_STOREID, ""));
    }


    /**
     * 批量删除直降促销
     *
     * @param marketingIds 促销id集合
     * @return 成功返回>0 否则返回0
     */
    @DeleteMapping("/fall")
    @ApiOperation(value = "批量删除直降促销", notes = "批量删除直降促销（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "marketingIds", value = "促销id集合"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回>0 否则返回0", response = Integer.class)
    })
    public int deleteFallByIds(Long[] marketingIds) {
        return marketingDeleteService.deleteMarketings(marketingIds, CommonConstant.ADMIN_STOREID, MarketingConstant.FALL_MARKETING);
    }


    /**
     * 分页查询单品信息（直降用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回单品信息(包括规格信息)
     */
    @GetMapping("/fall/skus")
    @ApiOperation(value = "分页查询单品信息（直降用）", notes = "分页查询单品信息（直降用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForFall(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }

    /**
     * 查询交叉时间内含有相同单品的互斥促销数量（直降用）
     *
     * @param marketing 促销信息
     * @return 交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品
     */
    @PostMapping("/fall/exclusionmarketingcount")
    @ApiOperation(value = "查询交叉时间内含有相同单品的互斥促销数量（直降用）", notes = "查询交叉时间内含有相同单品的互斥促销数量（直降用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品", response = Integer.class)
    })
    public int queryExclusionMarketingCountForFall(@RequestBody Marketing marketing) {
        return marketingQueryService.queryExclusionMarketingCount(marketing);
    }

    /**
     * 添加直降促销
     *
     * @param marketing 促销信息
     * @return 成功返回1 失败返回0
     */
    @PostMapping("/fall")
    @ApiOperation(value = "添加直降促销", notes = "添加直降促销（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int addFallMarketing(@RequestBody Marketing marketing) {
        return fallService.addMarketing(marketing.setAddMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.FALL_MARKETING));
    }

    /**
     * 根据促销id查询直降促销信息
     *
     * @param id 促销id
     * @return 返回促销详情信息
     */
    @GetMapping("/fall/{id}")
    @ApiOperation(value = "根据促销id查询直降促销信息", notes = "根据促销id查询直降促销信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回促销详情信息", response = Marketing.class)
    })
    public Marketing queryFallMarketingById(@PathVariable long id) {
        return marketingQueryService.queryMarketingById(id, CommonConstant.QUERY_WITH_NO_STORE);
    }


    /**
     * 查询交叉时间内含有相同单品的互斥促销数量（修改直降用）
     *
     * @param marketing 促销信息
     * @return 交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品
     */
    @PostMapping("/fall/update/exclusionmarketingcount")
    @ApiOperation(value = "查询交叉时间内含有相同单品的互斥促销数量（修改直降用）", notes = "查询交叉时间内含有相同单品的互斥促销数量（修改直降用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品", response = Integer.class)
    })
    public int queryExclusionMarketingCountForFallUpdate(@RequestBody Marketing marketing) {
        return marketingQueryService.queryExclusionMarketingCount(marketing);
    }

    /**
     * 修改直降促销
     *
     * @param marketing 直降促销
     * @return 成功返回1 失败返回0
     */
    @PutMapping("/fall")
    @ApiOperation(value = "修改直降促销", notes = "修改直降促销（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int updateFallMarketing(@RequestBody Marketing marketing) {
        return fallService.updateMarketing(marketing.setUpdateMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.FALL_MARKETING));
    }


    /**
     * 分页查询单品信息（修改直降用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回单品信息(包括规格信息)
     */
    @GetMapping("/fall/update/skus")
    @ApiOperation(value = "分页查询单品信息（修改直降用）", notes = "分页查询单品信息（修改直降用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForFallUpdate(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }

}
