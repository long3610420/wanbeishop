package com.ruoyi.web.controller.store;


import com.ruoyi.goods.domain.PmsBrand;
import com.ruoyi.goods.service.IPmsBrandService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author 魔金商城
 * @date 2019-06-27 19:19
 * <p>
 * 自定义品牌审核控制器
 */
@RestController
@Api(description = "自定义品牌审核接口")
public class CustomBrandAuditController {

    /**
     * 自动注入品牌service
     */
    @Autowired
    private IPmsBrandService brandService;

    /**
     * 分页查询自定义品牌
     *
     * @param pageHelper 分页帮助类
     * @param brandName  品牌名称
     * @param storeName  店铺名称
     * @return 自定义品牌
     */
    @GetMapping("/custombrandaudit")
    @ApiOperation(value = "分页查询自定义品牌", notes = "分页查询自定义品牌（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:custombrandsauditlist:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "brandName", value = "品牌名称"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "storeName", value = "店铺名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "自定义品牌", response = PmsBrand.class)
    })
    public BaseResponse queryMySelfBrands(@ApiIgnore PageHelper<PmsBrand> pageHelper, String brandName, String storeName) {
        return BaseResponse.build(brandService.queryCustomBrandByStatus(pageHelper, brandName, storeName));
    }

    /**
     * 通过自定义品牌审核
     *
     * @param id 自定义品牌id
     * @return 成功返回1，失败返回0
     */
    @PutMapping("/custombrandaudit/pass/{id}")
    @ApiOperation(value = "通过自定义品牌审核", notes = "通过自定义品牌审核（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:custombrandsauditlist:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "自定义品牌id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1，失败返回0", response = Integer.class)
    })
    public int passMySelfBrandAudit(@PathVariable long id) {
        return brandService.passCustomBrandAudit(id);
    }

    /**
     * 批量通过自定义品牌审核
     *
     * @param ids 自定义品牌id数组
     * @return 成功返回>=1，失败返回0
     */
    @PutMapping("/custombrandaudit/batch")
    @ApiOperation(value = "批量通过自定义品牌审核", notes = "批量通过自定义品牌审核（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:custombrandsauditlist:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "array", name = "ids", value = "自定义品牌id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回>=1，失败返回0", response = Integer.class)
    })
    public int batchPassMySelfBrandAudit(long[] ids) {
        return brandService.batchPassCustomBrandAudit(ids);
    }

    /**
     * 拒绝自定义品牌审核
     *
     * @param brand 自定义品牌id
     * @return 成功返回1，失败返回0
     */
    @PostMapping("/custombrandaudit/refuse")
    @ApiOperation(value = "拒绝自定义品牌审核", notes = "拒绝自定义品牌审核（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:custombrandsauditlist:list')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1，失败返回0", response = Integer.class)
    })
    public int refuseMySelfBrandAudit(@RequestBody PmsBrand brand) {
        return brandService.refuseCustomBrandAudit(brand);
    }

    /**
     * 批量拒绝自定义品牌审核
     *
     * @param ids    自定义品牌id数组
     * @param reason 拒绝原因
     * @return 成功返回>=1，失败返回0
     */
    @PutMapping("/custombrandaudit/batchrefuse")
    @ApiOperation(value = "批量拒绝自定义品牌审核", notes = "批量拒绝自定义品牌审核（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:custombrandsauditlist:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "自定义品牌id数组"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "reason", value = "拒绝原因"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回>=1，失败返回0", response = Integer.class)
    })
    public int batchRefuseMySelfBrandAudit(long[] ids, String reason) {
        return brandService.batchRefuseCustomBrandAudit(ids, reason);
    }

}
