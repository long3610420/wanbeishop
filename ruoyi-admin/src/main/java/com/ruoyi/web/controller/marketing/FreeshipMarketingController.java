package com.ruoyi.web.controller.marketing;


import com.ruoyi.marketing.domain.Marketing;
import com.ruoyi.marketing.service.FreeShipService;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.CommonResponse;
import com.ruoyi.util.MarketingConstant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 包邮促销
 * Created by 魔金商城平台管理on 2019/7/4.
 */
@RestController
@Api(description = "包邮促销接口")
public class FreeshipMarketingController {

    /**
     * 注入包邮服务接口
     */
    @Autowired
    private FreeShipService freeShipService;

    /**
     * 查询包邮信息
     *
     * @return 返回包邮信息
     */
    @GetMapping("/freeship")
    @ApiOperation(value = "查询包邮信息", notes = "查询包邮信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('freeship/queryfreeship')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回包邮信息", response = Marketing.class)
    })
    public CommonResponse queryFreeShip() {
        return CommonResponse.build(freeShipService.queryFreeShip(CommonConstant.ADMIN_STOREID));
    }


    /**
     * 新增包邮
     *
     * @param marketing 包邮促销
     * @return 成功返回1 失败返回0
     */
    @PostMapping("/freeship")
    @ApiOperation(value = "新增包邮", notes = "新增包邮（需要认证）")
    @PreAuthorize("@ss.hasPermi('freeship/addfreeship')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int addFreeShip(@RequestBody Marketing marketing) {
        return freeShipService.addMarketing(marketing.setAddMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.FREE_SHIP_MARKETING));
    }

}
