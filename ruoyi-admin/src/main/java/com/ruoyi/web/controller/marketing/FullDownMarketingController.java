package com.ruoyi.web.controller.marketing;


import com.ruoyi.goods.domain.PmsSku;
import com.ruoyi.goods.service.IPmsSkuService;
import com.ruoyi.marketing.domain.Marketing;
import com.ruoyi.marketing.service.MarketingQueryService;
import com.ruoyi.marketing.service.MarketingService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.MarketingConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

/**
 * Created by 魔金商城平台管理on 2019/7/25.
 * 满减促销控制器
 */
@RestController
@Api("满减促销接口")
public class FullDownMarketingController {

    /**
     * 注入满减促销服务接口
     */
    @Resource(name = "fullDownService")
    private MarketingService fullDownService;


    /**
     * 注入促销查询接口
     */
    @Autowired
    private MarketingQueryService marketingQueryService;


    /**
     * 注入促销删除服务接口
     */
    @Resource(name = "marketingDeleteService")
    private MarketingService marketingDeleteService;


    /**
     * 注入单品服务
     */
    @Autowired
    private IPmsSkuService skuService;


    /**
     * 分页查询满减促销信息
     *
     * @param pageHelper 分页帮助类
     * @param name       名称
     * @return 返回促销信息
     */
    @GetMapping("/fulldown")
    @ApiOperation(value = "分页查询满减促销信息", notes = "分页查询满减促销信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('fulldown')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "满减促销名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回促销信息", response = Marketing.class)
    })
    public BaseResponse queryFullDownMarketing(@ApiIgnore PageHelper<Marketing> pageHelper, String name) {
        return BaseResponse.build(marketingQueryService.queryMarketings(pageHelper, name, "4", CommonConstant.ADMIN_STOREID, ""));
    }


    /**
     * 批量删除满减促销
     *
     * @param marketingIds 促销id集合
     * @return 成功返回>0 否则返回0
     */
    @DeleteMapping("/fulldown")
    @ApiOperation(value = "批量删除满减促销", notes = "批量删除满减促销（需要认证）")
    @PreAuthorize("@ss.hasPermi('fulldown')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "marketingIds", value = "促销id集合"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回>0 否则返回0", response = Integer.class)
    })
    public int deleteFullDownByIds(Long[] marketingIds) {
        return marketingDeleteService.deleteMarketings(marketingIds, CommonConstant.ADMIN_STOREID, MarketingConstant.FULL_DOWN_MARKETING);
    }


    /**
     * 分页查询单品信息（满减用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回单品信息(包括规格信息)
     */
    @GetMapping("/fulldown/skus")
    @ApiOperation(value = "分页查询单品信息（满减用）", notes = "分页查询单品信息（满减用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('fulldown')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForFullDown(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }

    /**
     * 添加满减促销
     *
     * @param marketing 满减促销
     * @return 成功返回1 失败返回0
     */
    @PostMapping("/fulldown")
    @ApiOperation(value = "添加满减促销", notes = "添加满减促销（需要认证）")
    @PreAuthorize("@ss.hasPermi('fulldown')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int addFullDownMarketing(@RequestBody Marketing marketing) {
        return fullDownService.addMarketing(marketing.setAddMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.FULL_DOWN_MARKETING));
    }

    /**
     * 根据促销id查询满减促销信息
     *
     * @param id 促销id
     * @return 返回促销详情信息
     */
    @GetMapping("/fulldown/{id}")
    @ApiOperation(value = "根据促销id查询满减促销信息", notes = "根据促销id查询满减促销信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('fulldown')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回促销详情信息", response = Marketing.class)
    })
    public Marketing queryFullDownMarketingById(@PathVariable long id) {
        return marketingQueryService.queryMarketingById(id, CommonConstant.QUERY_WITH_NO_STORE);
    }

    /**
     * 更新满减促销
     *
     * @param marketing 促销信息
     * @return 成功返回1 失败返回0
     */
    @PutMapping("/fulldown")
    @ApiOperation(value = "更新满减促销", notes = "更新满减促销（需要认证）")
    @PreAuthorize("@ss.hasPermi('fulldown')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int updateFullDown(@RequestBody Marketing marketing) {
        return fullDownService.updateMarketing(marketing.setUpdateMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.FULL_DOWN_MARKETING));
    }


    /**
     * 分页查询单品信息（修改满减用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回单品信息(包括规格信息)
     */
    @GetMapping("/fulldown/update/skus")
    @ApiOperation(value = "分页查询单品信息（修改满减用）", notes = "分页查询单品信息（满减用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('fulldown')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForFullDownUpdate(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }
}
