package com.ruoyi.web.controller.marketing;


import com.ruoyi.marketing.domain.SeckillScenePanicbuy;
import com.ruoyi.marketing.service.SeckillScenePanicbuyService;
import com.ruoyi.marketing.service.SeckillScenePanicbuyServiceApi;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;


/**
 * 秒杀活动折扣控制器
 *
 * @author 魔金商城 created on 2020/5/15
 */
@RestController
@Api("秒杀活动折扣接口")
public class SeckillScenePanicbuyController {

    /**
     * 注入秒杀活动折扣服务接口
     */
    @Autowired
    private SeckillScenePanicbuyService seckillScenePanicbuyService;

    /**
     * 注入秒杀活动折扣聚合服务接口
     */
    @Autowired
    private SeckillScenePanicbuyServiceApi seckillScenePanicbuyServiceApi;


    /**
     * 分页查询秒杀场次折扣
     *
     * @param pageHelper     分页帮助类
     * @param seckillSceneId 秒杀场次id
     * @return 返回秒杀场次折扣列表
     */
    @GetMapping("/seckillscenepanicbuylist")
    @ApiOperation(value = "分页查询秒杀场次折扣", notes = "分页查询秒杀场次折扣（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "seckillSceneId", value = "秒杀场次id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回秒杀场次折扣列表", response = SeckillScenePanicbuy.class)
    })
    public BaseResponse querySeckillScenePanicbuyList(@ApiIgnore PageHelper<SeckillScenePanicbuy> pageHelper, long seckillSceneId) {
        return BaseResponse.build(seckillScenePanicbuyServiceApi.querySeckillScenePanicbuyList(pageHelper, seckillSceneId, CommonConstant.ADMIN_STOREID));
    }

    /**
     * 修改秒杀活动折扣
     *
     * @param id     秒杀活动折扣id
     * @param isShow 是否显示
     * @param sort   排序
     * @return 成功1 否则失败
     */
    @PutMapping("/seckillscenepanicbuy/update/{id}/{isShow}/{sort}")
    @ApiOperation(value = "修改秒杀活动折扣", notes = "修改秒杀活动折扣（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "秒杀活动折扣id"),
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "isShow", value = "是否显示"),
            @ApiImplicitParam(paramType = "path", dataType = "Integer", name = "sort", value = "排序"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int updateSeckillScenePanicbuy(@PathVariable long id, @PathVariable String isShow, @PathVariable Integer sort) {
        return seckillScenePanicbuyService.updateSeckillScenePanicbuy(id, isShow, sort);
    }

    /**
     * 批量设置秒杀活动折扣展示
     *
     * @param ids 秒杀活动折扣id数组
     * @return 成功1 否则失败
     */
    @PutMapping("/seckillscenepanicbuy/batchshow")
    @ApiOperation(value = "批量设置秒杀活动折扣展示", notes = "批量设置秒杀活动折扣展示（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "Long[]", name = "ids", value = "秒杀活动折扣id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int batchShowSeckillScenePanicbuy(Long[] ids) {
        return seckillScenePanicbuyService.batchUpdateSeckillScenePanicbuy(ids, CommonConstant.PANIC_SHOW);
    }

    /**
     * 批量设置秒杀活动折扣不展示
     *
     * @param ids 秒杀活动折扣id数组
     * @return 成功1 否则失败
     */
    @PutMapping("/seckillscenepanicbuy/batchhide")
    @ApiOperation(value = "批量设置秒杀活动折扣不展示", notes = "批量设置秒杀活动折扣不展示（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "Long[]", name = "ids", value = "秒杀活动折扣id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int batchHideSeckillScenePanicbuy(Long[] ids) {
        return seckillScenePanicbuyService.batchUpdateSeckillScenePanicbuy(ids, CommonConstant.PANIC_NOT_SHOW);
    }

}
