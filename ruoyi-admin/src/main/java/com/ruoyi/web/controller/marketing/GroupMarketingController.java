package com.ruoyi.web.controller.marketing;


import com.ruoyi.goods.domain.PmsSku;
import com.ruoyi.goods.service.IPmsSkuService;
import com.ruoyi.marketing.domain.Marketing;
import com.ruoyi.marketing.domain.MarketingCate;
import com.ruoyi.marketing.domain.MarketingPic;
import com.ruoyi.marketing.service.MarketingCateService;
import com.ruoyi.marketing.service.MarketingPicService;
import com.ruoyi.marketing.service.MarketingQueryService;
import com.ruoyi.marketing.service.MarketingService;
import com.ruoyi.util.*;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

/**
 * 拼团促销控制器
 *
 * @author 魔金商城 created on 2019/8/7
 */
@RestController
@Api(description = "拼团促销接口")
public class GroupMarketingController {

    /**
     * 注入拼团促销服务接口
     */
    @Resource(name = "groupMarketingService")
    private MarketingService groupMarketingService;

    /**
     * 注入促销查询接口
     */
    @Autowired
    private MarketingQueryService marketingQueryService;

    /**
     * 注入促销删除服务接口
     */
    @Resource(name = "marketingDeleteService")
    private MarketingService marketingDeleteService;

    /**
     * 注入单品服务
     */
    @Autowired
    private IPmsSkuService skuService;


    /**
     * 注入促销图片服务接口
     */
    @Autowired
    private MarketingPicService marketingPicService;


    /**
     * 注入促销分类服务接口
     */
    @Autowired
    private MarketingCateService marketingCateService;


    /**
     * 分页查询拼团促销列表
     *
     * @param pageHelper 分页帮助类
     * @param name       名称
     * @param type       类别
     * @param status     活动状态 1 未开始 2 进行中 3 已结束
     * @return 拼团促销列表
     */
    @GetMapping("/groupmarketinglist")
    @ApiOperation(value = "分页查询拼团促销列表", notes = "分页查询拼团促销列表（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "拼团促销名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "type", value = "拼团促销类别"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "status", value = "活动状态 1 未开始 2 进行中 3 已结束"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "拼团促销列表", response = Marketing.class)
    })
    public BaseResponse queryGroupMarketingList(@ApiIgnore PageHelper<Marketing> pageHelper, String name, String type, String status) {
        return BaseResponse.build(marketingQueryService.queryMarketings(pageHelper, name, type, CommonConstant.ADMIN_STOREID, status));
    }

    /**
     * 批量删除拼团促销
     *
     * @param marketingIds 促销id数组
     * @return 成功返回1 否则失败
     */
    @DeleteMapping("/groupmarketing")
    @ApiOperation(value = "批量删除拼团促销", notes = "批量删除拼团促销（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "marketingIds", value = "促销id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 否则失败", response = Integer.class)
    })
    public int deleteGroupMarketingByIds(Long[] marketingIds) {
        return marketingDeleteService.deleteMarketings(marketingIds, CommonConstant.ADMIN_STOREID, MarketingConstant.GROUP_MARKETING);
    }

    /**
     * 分页查询单品信息（新增拼团促销用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 单品信息（包含规格信息）
     */
    @GetMapping("/addgroupmarketing/skus")
    @ApiOperation(value = "分页查询单品信息（新增拼团促销用）", notes = "分页查询单品信息（新增拼团促销用）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForAddGroup(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }

    /**
     * 查询交叉时间内含有相同单品的互斥促销数量（新增拼团促销用）
     *
     * @param marketing 促销信息
     * @return 交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品
     */
    @PostMapping("/addgroupmarketing/exclusionmarketingcount")
    @ApiOperation(value = "查询交叉时间内含有相同单品的互斥促销数量（新增拼团促销用）", notes = "查询交叉时间内含有相同单品的互斥促销数量（新增拼团促销用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品", response = Integer.class)
    })
    public int queryExclusionMarketingCountForAddGroup(@RequestBody Marketing marketing) {
        return marketingQueryService.queryExclusionMarketingCount(marketing);
    }

    /**
     * 新增拼团促销
     *
     * @param marketing 促销信息
     * @return 成功1 否则失败
     */
    @PostMapping("/groupmarketing")
    @ApiOperation(value = "新增拼团促销", notes = "新增拼团促销（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int addGroupMarketing(@RequestBody Marketing marketing) {
        return groupMarketingService.addMarketing(marketing.setAddMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.GROUP_MARKETING));
    }

    /**
     * 根据id查询拼团促销（修改拼团促销用）
     *
     * @param id 促销id
     * @return 拼团促销信息
     */
    @GetMapping("/groupmarketing/{id}")
    @ApiOperation(value = "根据id查询拼团促销（修改拼团促销用）", notes = "根据id查询拼团促销（修改拼团促销用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "拼团促销信息", response = Marketing.class)
    })
    public Marketing queryGroupMarketingById(@PathVariable long id) {
        return marketingQueryService.queryMarketingById(id, CommonConstant.QUERY_WITH_NO_STORE);
    }

    /**
     * 分页查询单品信息（修改拼团促销用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 单品信息（包含规格信息）
     */
    @GetMapping("/updategroupmarketing/skus")
    @ApiOperation(value = "分页查询单品信息（修改拼团促销用）", notes = "分页查询单品信息（修改拼团促销用）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForUpdateGroup(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }

    /**
     * 查询交叉时间内含有相同单品的互斥促销数量（修改拼团促销用）
     *
     * @param marketing 促销信息
     * @return 交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品
     */
    @PostMapping("/updategroupmarketing/exclusionmarketingcount")
    @ApiOperation(value = "查询交叉时间内含有相同单品的互斥促销数量（修改拼团促销用）", notes = "查询交叉时间内含有相同单品的互斥促销数量（修改拼团促销用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "交叉时间内含有相同单品的互斥促销数量  -1缺少参数 -2所选商品中存在有商品组合的商品 -3所选商品中存在有会员价的商品", response = Integer.class)
    })
    public int queryExclusionMarketingCountForUpdateGroup(@RequestBody Marketing marketing) {
        return marketingQueryService.queryExclusionMarketingCount(marketing);
    }

    /**
     * 修改拼团促销
     *
     * @param marketing 促销信息
     * @return 成功1 否则失败 -1 只有未开始的活动才能修改
     */
    @PutMapping("/groupmarketing")
    @ApiOperation(value = "修改拼团促销", notes = "修改拼团促销（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int updateGroup(@RequestBody Marketing marketing) {
        return groupMarketingService.updateMarketing(marketing.setUpdateMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.GROUP_MARKETING));
    }

    /**
     * 根据id查询拼团促销（查询拼团促销详情用）
     *
     * @param id 促销id
     * @return 拼团促销信息
     */
    @GetMapping("/groupmarketingdetail/{id}")
    @ApiOperation(value = "根据id查询拼团促销（查询拼团促销详情用）", notes = "根据id查询拼团促销（查询拼团促销详情用）（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "拼团促销信息", response = Marketing.class)
    })
    public Marketing queryGroupMarketingDetailById(@PathVariable long id) {
        return marketingQueryService.queryMarketingById(id, CommonConstant.QUERY_WITH_NO_STORE);
    }


    /**
     * 查询拼团促销图片信息
     *
     * @return 拼团促销图片信息
     */
    @GetMapping("/grouppic")
    @ApiOperation(value = "查询拼团促销图片信息", notes = "查询拼团促销图片信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "拼团促销图片信息", response = MarketingPic.class)
    })
    public CommonResponse<MarketingPic> queryGroupPic() {
        return CommonResponse.build(marketingPicService.queryMarketingPic(CommonConstant.GROUP_MARKETING_PIC_TYPE, CommonConstant.ADMIN_STOREID));
    }

    /**
     * 保存拼团促销图片
     *
     * @param marketingPic 促销图片实体
     * @return 成功1 否则失败
     */
    @PostMapping("/grouppic")
    @ApiOperation(value = "保存拼团促销图片", notes = "保存拼团促销图片（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int saveGroupPic(@RequestBody MarketingPic marketingPic) {
        return marketingPicService.saveMarketingPic(marketingPic.addType(CommonConstant.GROUP_MARKETING_PIC_TYPE).addStoreId(CommonConstant.ADMIN_STOREID));
    }


    /**
     * 分页查询拼团促销分类
     *
     * @param pageHelper 分页帮助类
     * @param name       促销分类名称
     * @return 促销分类列表
     */
    @GetMapping("/groupmarketing/cates")
    @ApiOperation(value = "分页查询拼团促销分类", notes = "分页查询拼团促销分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "促销分类名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "拼团促销分类列表", response = MarketingCate.class)
    })
    public BaseResponse queryGroupMarketingCates(@ApiIgnore PageHelper<MarketingCate> pageHelper, String name) {
        return BaseResponse.build(marketingCateService.queryMarketingCates(pageHelper, name, CommonConstant.GROUP_MARKETING_CATE, CommonConstant.ADMIN_STOREID));
    }


    /**
     * 新增拼团促销分类
     *
     * @param marketingCate 促销分类信息
     * @return 成功1 否则失败
     */
    @PostMapping("/groupmarketing/cate")
    @ApiOperation(value = "新增拼团促销分类", notes = "新增拼团促销分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int addGroupMarketingCate(@RequestBody MarketingCate marketingCate) {
        return marketingCateService.addMarketingCate(marketingCate.buildForAdd(CommonConstant.GROUP_MARKETING_CATE, CommonConstant.ADMIN_STOREID));
    }


    /**
     * 删除拼团促销分类
     *
     * @param ids 促销分类id数组
     * @return 成功>0 否则失败
     */
    @DeleteMapping("/groupmarketing/cate")
    @ApiOperation(value = "删除拼团促销分类", notes = "删除拼团促销分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "促销分类id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功>0 否则失败", response = Integer.class)
    })
    public int deleteGroupMarketingCate(long[] ids) {
        return marketingCateService.deleteMarketingCates(ids, CommonConstant.ADMIN_STOREID, CommonConstant.GROUP_MARKETING_CATE);
    }

    /**
     * 根据id查询拼团促销分类
     *
     * @param id 促销分类id
     * @return 拼团促销分类信息
     */
    @GetMapping("/groupmarketing/cate/{id}")
    @ApiOperation(value = "根据id查询拼团促销分类", notes = "根据id查询拼团促销分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销分类id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "促销分类信息", response = MarketingCate.class)
    })
    public MarketingCate queryMarketingCateById(@PathVariable long id) {
        return marketingCateService.queryMarketingCateById(id, CommonConstant.ADMIN_STOREID);
    }


    /**
     * 修改拼团促销分类
     *
     * @param marketingCate 促销分类信息
     * @return 成功1 否则失败
     */
    @PutMapping("/groupmarketing/cate")
    @ApiOperation(value = "修改拼团促销分类", notes = "修改拼团促销分类（需要认证）")
    @PreAuthorize("@ss.hasPermi('groupmarketinglist')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int updateMarketingCate(@RequestBody MarketingCate marketingCate) {
        marketingCate.setStoreId(CommonConstant.ADMIN_STOREID);
        return marketingCateService.updateMarketingCate(marketingCate);
    }


}
