package com.ruoyi.web.controller.store;


import com.ruoyi.order.domain.OmsBackOrder;
import com.ruoyi.order.domain.OmsOrder;
import com.ruoyi.order.service.IOmsBackOrderService;
import com.ruoyi.order.service.IOrderApiService;
import com.ruoyi.order.vo.BackOrderItem;
import com.ruoyi.order.vo.OrderItem;
import com.ruoyi.order.vo.QueryCriteria;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Created by 魔金商城 on 2019/6/25.
 * 店铺退单控制器
 */
@RestController
@Api(description = "店铺退单接口")
public class StoreBackOrderController {

    /**
     * 注入退单服务接口
     */
    @Autowired
    private IOmsBackOrderService backOrderService;
    /**
     * 注入订单混合api
     */
    @Autowired
    private IOrderApiService IOrderApiService;

    /**
     * 查询店铺退单列表
     *
     * @param pageHelper    分页帮助类
     * @param queryCriteria 搜索条件
     * @return 返回店铺退单数据
     */
    @GetMapping("/storebackorders")
    @ApiOperation(value = "查询店铺退单列表", notes = "查询店铺退单列表（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:storebackorderlist:storebackorderlist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "backCode", value = "退单号"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "orderCode", value = "订单号"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "status", value = "退款／退货状态"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回店铺退单数据", response = OmsBackOrder.class)
    })
    public BaseResponse queryStoreBackOrders(@ApiIgnore PageHelper<OmsBackOrder> pageHelper, @ApiIgnore QueryCriteria queryCriteria) {
        return BaseResponse.build(backOrderService.queryStoreBackOrders(pageHelper, queryCriteria));
    }


    /**
     * 根据退单id查询退单信息(退款用)(包含退单的商品, 和操作日志 此接口调用慎重)
     *
     * @param backOrderId 退单id
     * @return 返回退单信息
     */
    @GetMapping("/refund/backorderdetail/{backOrderId}")
    @ApiOperation(value = "根据退单id查询退单信息(包含退单的商品, 和操作日志 此接口调用慎重)", notes = "根据退单id查询退单信息(包含退单的商品, 和操作日志 此接口调用慎重)（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:TStoreComment:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "backOrderId", value = "退单id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回退单信息", response = OmsBackOrder.class)
    })
    public OmsBackOrder queryBackOrderByIdForRefund(@PathVariable long backOrderId) {
        return backOrderService.queryBackOrderById(backOrderId, CommonConstant.QUERY_WITH_NO_STORE, CommonConstant.QUERY_WITH_NO_CUSTOMER, BackOrderItem.LOG, BackOrderItem.SKUS);
    }


    /**
     * 根据退单id查询退单信息(退货用)(包含退单的商品, 和操作日志 此接口调用慎重)
     *
     * @param backOrderId 退单id
     * @return 返回退单信息
     */
    @GetMapping("/return/backorderdetail/{backOrderId}")
    @ApiOperation(value = "根据退单id查询退单信息(包含退单的商品, 和操作日志 此接口调用慎重)", notes = "根据退单id查询退单信息(包含退单的商品, 和操作日志 此接口调用慎重)（需要认证）")
    @PreAuthorize("@ss.hasPermi('storebackorder/querybackorderbyidforreturn')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "backOrderId", value = "退单id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回退单信息", response = OmsBackOrder.class)
    })
    public OmsBackOrder queryBackOrderByIdForReturn(@PathVariable long backOrderId) {
        return backOrderService.queryBackOrderById(backOrderId, CommonConstant.QUERY_WITH_NO_STORE, CommonConstant.QUERY_WITH_NO_CUSTOMER, BackOrderItem.LOG, BackOrderItem.SKUS);
    }

    /**
     * 根据订单id查询订单信息  (订单的所有信息 此接口慎用)
     *
     * @param id 订单id
     * @return 返回订单信息
     */
    @GetMapping("/store/backorder/orderdetail/{id}")
    @ApiOperation(value = "根据订单id查询订单信息  (订单的所有信息 此接口慎用)", notes = "根据订单id查询订单信息  (订单的所有信息 此接口慎用)（需要认证）")
    @PreAuthorize("@ss.hasPermi('storebackorder/queryorderbyidforstore')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "订单id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回订单信息", response = OmsOrder.class)
    })
    public OmsOrder queryOrderByIdForStore(@PathVariable long id) {
        return IOrderApiService.queryOrderDetailById(id, CommonConstant.QUERY_WITH_NO_STORE, CommonConstant.QUERY_WITH_NO_CUSTOMER, OrderItem.LOG, OrderItem.ATTR, OrderItem.SKUS);
    }
}
