package com.ruoyi.web.controller.store;


import com.ruoyi.goods.domain.PmsBrandApply;
import com.ruoyi.goods.service.IPmsBrandApplyService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author 魔金商城
 * @date 2019-06-27 16:52
 * <p>
 * 品牌审核控制器
 */
@RestController
@Api(description = "品牌审核接口")
public class BrandAuditController {

    /**
     * 自动注入品牌申请service
     */
    @Autowired
    private IPmsBrandApplyService brandApplyService;


    /**
     * 分页查询待审核品牌
     *
     * @param pageHelper 分页帮助类
     * @param name       品牌名称
     * @param storeName  店铺名称
     * @return 返回品牌信息
     */
    @GetMapping("/brandaudit")
    @ApiOperation(value = "分页查询待审核品牌", notes = "分页查询待审核品牌（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:brandsauditlist:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "name", value = "品牌名称"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "storeName", value = "店铺名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回品牌信息", response = PmsBrandApply.class)
    })
    public BaseResponse queryBrandToBeAudit(@ApiIgnore PageHelper<PmsBrandApply> pageHelper, String name, String storeName) {
        return BaseResponse.build(brandApplyService.queryBrandToBeAudit(pageHelper, name, storeName));
    }

    /**
     * 通过品牌审核
     *
     * @param id 品牌审核id
     * @return 成功返回1，失败返回0
     */
    @PutMapping("/brandaudit/pass/{id}")
    @ApiOperation(value = "通过品牌审核", notes = "通过品牌审核（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:brandsauditlist:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "品牌审核id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1，失败返回0", response = Integer.class)
    })
    public int passBrandAudit(@PathVariable long id) {
        return brandApplyService.passBrandAudit(id);
    }

    /**
     * 批量通过品牌审核
     *
     * @param ids 品牌审核id数组
     * @return 成功返回>=1，失败返回0
     */
    @PutMapping("/brandaudit/batchpass")
    @ApiOperation(value = "批量通过品牌审核", notes = "批量通过品牌审核（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:brandsauditlist:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "品牌审核id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回>=1，失败返回0", response = Integer.class)
    })
    public int batchPassBrandAudit(long[] ids) {
        return brandApplyService.batchPassBrandAudit(ids);
    }

    /**
     * 拒绝品牌审核
     *
     * @param brandApply 商品审核实例
     * @return 成功返回1，失败返回0
     */
    @PostMapping("/brandaudit/refuse")
    @ApiOperation(value = "拒绝品牌审核", notes = "拒绝品牌审核（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:brandsauditlist:list')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1，失败返回0", response = Integer.class)
    })
    public int refuseBrandAudit(@RequestBody PmsBrandApply brandApply) {
        return brandApplyService.refuseBrandAudit(brandApply);
    }

    /**
     * 批量拒绝品牌审核
     *
     * @param ids    品牌审核id数组
     * @param reason 拒绝原因
     * @return 成功返回>=1，失败返回0
     */
    @PutMapping("/brandaudit/batchrefuse")
    @ApiOperation(value = "批量拒绝品牌审核", notes = "批量拒绝品牌审核（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:brandsauditlist:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "品牌审核id数组"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "reason", value = "拒绝原因"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回>=1，失败返回0", response = Integer.class)
    })
    public int batchRefuseBrandAudit(long[] ids, String reason) {
        return brandApplyService.batchRefuseBrandAudit(ids, reason);
    }
}
