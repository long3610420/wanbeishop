package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStoreCategory;
import com.ruoyi.store.service.ITStoreCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 店铺分类Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStoreCategory")
public class TStoreCategoryController extends BaseController {
    @Autowired
    private ITStoreCategoryService tStoreCategoryService;

    /**
     * 查询店铺分类列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreCategory:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStoreCategory tStoreCategory) {
        startPage();
        List<TStoreCategory> list = tStoreCategoryService.selectTStoreCategoryList(tStoreCategory);
        return getDataTable(list);
    }

    /**
     * 导出店铺分类列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreCategory:export')")
    @Log(title = "店铺分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStoreCategory tStoreCategory) {
        List<TStoreCategory> list = tStoreCategoryService.selectTStoreCategoryList(tStoreCategory);
        ExcelUtil<TStoreCategory> util = new ExcelUtil<TStoreCategory>(TStoreCategory.class);
        return util.exportExcel(list, "TStoreCategory");
    }

    /**
     * 获取店铺分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreCategory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStoreCategoryService.selectTStoreCategoryById(id));
    }

    /**
     * 新增店铺分类
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreCategory:add')")
    @Log(title = "店铺分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStoreCategory tStoreCategory) {
        return toAjax(tStoreCategoryService.insertTStoreCategory(tStoreCategory));
    }

    /**
     * 修改店铺分类
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreCategory:edit')")
    @Log(title = "店铺分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStoreCategory tStoreCategory) {
        return toAjax(tStoreCategoryService.updateTStoreCategory(tStoreCategory));
    }

    /**
     * 删除店铺分类
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreCategory:remove')")
    @Log(title = "店铺分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStoreCategoryService.deleteTStoreCategoryByIds(ids));
    }
}
