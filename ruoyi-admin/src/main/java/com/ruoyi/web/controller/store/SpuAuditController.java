package com.ruoyi.web.controller.store;


import com.ruoyi.common.enums.StatusEnum;
import com.ruoyi.goods.domain.PmsCategory;
import com.ruoyi.goods.domain.PmsGoods;
import com.ruoyi.goods.domain.PmsServiceSupport;
import com.ruoyi.goods.service.IPmsCategoryService;
import com.ruoyi.goods.service.IPmsGoodsService;
import com.ruoyi.goods.service.IPmsServiceSupportService;
import com.ruoyi.goods.vo.SpuSearchCondition;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * @author 魔金商城
 * @date 2019-06-27 09:41
 * <p>
 * 商品审核控制器
 */
@RestController
@Api(description = "商品审核接口")
public class SpuAuditController {

    /**
     * 注入商品服务接口
     */
    @Autowired
    private IPmsGoodsService spuService;

    /**
     * 注入分类服务接口
     */
    @Autowired
    private IPmsCategoryService categoryService;


    /**
     * 自动注入服务支持业务层接口
     */
    @Autowired
    private IPmsServiceSupportService serviceSupportService;


    /**
     * 分页查询所有店铺待审核的商品
     *
     * @param pageHelper         分页帮助类
     * @param spuSearchCondition 搜索条件
     * @return 返回待审核的商品信息
     */
    @GetMapping("/goodsaudit")
    @ApiOperation(value = "分页查询所有店铺待审核的商品", notes = "分页查询所有店铺待审核的商品（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:storespuslist:storespuslist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "id", value = "商品编号"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "name", value = "商品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "storeName", value = "店铺名称"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "secondCateId", value = "二级分类id"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "thirdCateId", value = "三级分类id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回待审核的商品信息", response = PmsGoods.class)
    })
    public BaseResponse queryAllStoreToBeAuditdSpus(@ApiIgnore PageHelper<PmsGoods> pageHelper, @ApiIgnore SpuSearchCondition spuSearchCondition) {
        spuSearchCondition.setStatus(StatusEnum.AuditType.INIT.code()+"");
        return BaseResponse.build(spuService.queryAllStoreSpus(pageHelper, spuSearchCondition));
    }

    /**
     * 根据分类id查询所有子分类信息
     *
     * @param parentId 父级id
     * @return 返回该父级下的所有分类信息
     */
    @GetMapping("/goodsaudit/category/{parentId}")
    @ApiOperation(value = "根据分类id查询所有子分类信息", notes = "根据分类id查询所有子分类信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:storespuslist:storespuslist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "parentId", value = "父级id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回该父级下的所有分类信息", response = PmsCategory.class)
    })
    public List<PmsCategory> queryCategoryByParentId(@PathVariable long parentId) {
        return categoryService.queryCategoryByParentId(parentId);
    }

    /**
     * 店铺商品审核通过
     *
     * @param spuId 商品id
     * @return 成功返回1 失败返回0
     */
    @PutMapping("/store/audit/{spuId}")
    @ApiOperation(value = "店铺商品审核通过", notes = "店铺商品审核通过（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:storespuslist:storespuslist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "spuId", value = "商品id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int auditPass(@PathVariable long spuId) {
        return spuService.auditPass(null, spuId);
    }


    /**
     * 店铺商品拒绝审核
     *
     * @param spuId  商品id
     * @param reason 原因
     * @return 成功返回1  失败返回0
     */
    @PutMapping("/store/audit/refuse/{spuId}")
    @ApiOperation(value = "店铺商品拒绝审核", notes = "店铺商品拒绝审核（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:storespuslist:storespuslist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "reason", value = "原因"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "spuId", value = "商品id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int auditRefuse(String reason, @PathVariable long spuId) {
        return spuService.auditRefuse(reason, null, spuId);
    }


    /**
     * 根据商品id查询商品信息
     *
     * @param id 商品id
     * @return 返回商品详细信息(包含 商品服务支持, 商品图片, 商品属性值, 商品规格值, 商品单品......)
     */
    @GetMapping("/store/audit/spu/{id}")
    @ApiOperation(value = "根据商品id查询商品信息", notes = "根据商品id查询商品信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:storespuslist:storespuslist')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "商品id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回商品详细信息", response = PmsGoods.class)
    })
    public PmsGoods querySpuById(@PathVariable long id) {
        return spuService.querySpu(id, CommonConstant.QUERY_WITH_NO_STORE);
    }


    /**
     * 查询所有服务支持
     *
     * @return 返回所有服务支持
     */
    @GetMapping("/store/audit/servicesupport")
    @ApiOperation(value = "查询所有服务支持", notes = "查询所有服务支持（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回所有服务支持", response = PmsServiceSupport.class)
    })
    public List<PmsServiceSupport> queryAllServiceSupport() {
        return serviceSupportService.queryAllServiceSupport();
    }


}
