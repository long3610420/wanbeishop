package com.ruoyi.web.controller.setting;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.setting.bean.PaySetCommon;
import com.ruoyi.setting.service.ILsPaySettingService;
import com.ruoyi.util.CommonConstant;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 支付设置Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/setting/LsPaySetting")
public class LsPaySettingController extends BaseController {
    @Autowired
    private ILsPaySettingService lsPaySettingService;



    /**
     * 查询支付设置
     *
     * @return 支付设置信息
     */
    @GetMapping("/payset")
    @ApiOperation(value = "查询支付设置", notes = "查询支付设置（需要认证）")
    @PreAuthorize("@ss.hasPermi('setting:LsPaySetting:list')")
    public PaySetCommon queryPaySet() {
        return lsPaySettingService.queryPaySet(CommonConstant.ADMIN_STOREID);
    }

    /**
     * 修改支付设置
     *
     * @param paySetCommon 实体类参数
     * @param codeType     支付设置类型 1 支付宝 2 微信 3 银联
     * @return 成功>=1 否则失败 -1 支付设置类型不存在
     */
    @PostMapping("/payset/{codeType}")
    @ApiOperation(value = "修改支付设置", notes = "修改支付设置（需要认证）")
    @PreAuthorize("@ss.hasPermi('setting:LsPaySetting:add')")
    public int editPaySet(@RequestBody PaySetCommon paySetCommon, @PathVariable String codeType) {
        return lsPaySettingService.editPaySet(paySetCommon, codeType,CommonConstant.ADMIN_STOREID);
    }


}
