package com.ruoyi.web.controller.store;


import com.ruoyi.goods.domain.PmsCategory;
import com.ruoyi.goods.service.IPmsCategoryService;
import com.ruoyi.store.domain.TStoreInfo;
import com.ruoyi.store.domain.TStoreSignedCategory;
import com.ruoyi.store.service.ITStoreInfoService;
import com.ruoyi.store.service.ITStoreSignedCategoryService;
import com.ruoyi.store.vo.StoreBusinessInfo;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @author 魔金商城
 * @date 2019-06-28 13:28
 * <p>
 * 商家信息设置控制器
 */
@RestController
@Api(description = "商家信息设置接口")
public class StoreInfoSetController {


    /**
     * 注入店铺信息服务
     */
    private ITStoreInfoService storeInfoService;

    /**
     * 注入签约分类接口
     */
    private ITStoreSignedCategoryService storeSignedCategoryService;

    /**
     * 注入分类服务接口
     */
    private IPmsCategoryService categoryService;


    @Autowired
    public StoreInfoSetController(ITStoreInfoService storeInfoService, ITStoreSignedCategoryService storeSignedCategoryService, IPmsCategoryService categoryService) {
        this.storeInfoService = storeInfoService;
        this.storeSignedCategoryService = storeSignedCategoryService;
        this.categoryService = categoryService;
    }

    /**
     * 根据店铺id查询店铺详情信息
     *
     * @param storeId 店铺id
     * @return 店铺详情信息
     */
    @GetMapping("/storeinfoset")
    @PreAuthorize("@ss.hasPermi('store:TStoreBillingRecords:list')")
    @ApiOperation(value = "根据店铺id查询店铺详情信息", notes = "根据店铺id查询店铺详情信息（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "storeId", value = "店铺id"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "status", value = "品牌状态 状态  0 申请中  1通过 2 拒绝"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "店铺详情信息", response = StoreBusinessInfo.class)
    })
    public StoreBusinessInfo storeDetailInfoByStoreId(long storeId, String status) {
        return storeInfoService.queryStoreBusinessInfo(storeId, status);
    }


    /**
     * 添加签约分类
     *
     * @param categoryIds 分类id数组
     * @param storeId     店铺id
     * @return 返回添加码 -1 未添加分类 >=1添加成功
     */
    @PostMapping("/storeinfoset")
    @ApiOperation(value = "添加签约分类", notes = "添加签约分类（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "storeId", value = "店铺id"),
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "categoryIds", value = "分类id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = " 返回添加码 -1 未添加分类 >=1添加成功", response = Integer.class)
    })
    public int addStoreSignedCategory(long[] categoryIds, long storeId) {
        return storeSignedCategoryService.addSignedCategoryAdmin(categoryIds, storeId, true);
    }

    /**
     * 根据父级id查询所有子分类
     *
     * @param parentId 父级id
     * @return 子分类集合
     */
    @GetMapping("/storeinfoset/category/{parentId}")
    @ApiOperation(value = "根据父级id查询所有子分类", notes = "根据父级id查询所有子分类（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "parentId", value = "父级id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "子分类集合", response = PmsCategory.class)
    })
    public List<PmsCategory> queryCategoryByParentId(@PathVariable long parentId) {
        return categoryService.queryCategoryByParentId(parentId);
    }


    /**
     * 根据店铺id和分类id删除签约分类
     *
     * @param storeId 店铺id
     * @param cateId  分类id
     * @return 删除返回码
     */
    @DeleteMapping("/storeinfoset")
    @ApiOperation(value = "根据店铺id和分类id删除签约分类", notes = "根据店铺id和分类id删除签约分类（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "店铺id"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "cateId", value = "分类id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "删除返回码", response = Integer.class)
    })
    public int deleteStoreSingedCategoryById(long storeId, long cateId) {
        return storeSignedCategoryService.deleteSingedCategoryById(storeId, cateId);
    }


    /**
     * 编辑店铺有效期,结算周期,是否关店
     *
     * @param storeInfo 店铺信息
     * @return 编辑返回码
     */
    @PutMapping("/storeinfoset")
    @ApiOperation(value = "编辑店铺有效期,结算周期,是否关店", notes = "编辑店铺有效期,结算周期,是否关店（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "编辑返回码", response = Integer.class)
    })
    public int editStoreTimeAndIsClose(@RequestBody TStoreInfo storeInfo) {
        return storeInfoService.editStoreTimeAndIsClose(storeInfo, null);
    }

    /**
     * 审核通过签约分类申请
     *
     * @param ids 签约id列表
     * @return 返回添加码 -1 未添加分类 >=1添加成功
     */
    @PutMapping("/storeinfoset/auditStoreSignedCategory")
    @ApiOperation(value = "审批签约分类", notes = "审批签约分类（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "签约id列表"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = " 返回添加码 -1 未添加分类 >=1添加成功", response = Integer.class)
    })
    public int auditStoreSignedCategory(@RequestBody Long[] ids) {
        return storeSignedCategoryService.updateState(Arrays.asList(ids), true);
    }

    /**
     * 根据店铺id查询店铺商品分类签约列表
     *
     * @param storeId 店铺id
     * @return 店铺详情信息
     */
    @GetMapping("/storeSignedCategory/{storeId}")
    @ApiOperation(value = "根据店铺id查询店铺商品分类签约列表", notes = "根据店铺id查询店铺商品分类签约列表（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "storeId", value = "店铺id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "商品分类签约信息", response = TStoreSignedCategory.class)
    })
    public List<TStoreSignedCategory> storeSignedCategory(@PathVariable Long storeId) {
        return storeSignedCategoryService.queryAllSignedCategorys(storeId);
    }


}
