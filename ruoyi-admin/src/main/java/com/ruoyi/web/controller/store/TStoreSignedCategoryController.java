package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStoreSignedCategory;
import com.ruoyi.store.service.ITStoreSignedCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 店铺的签约分类Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStoreSignedCategory")
public class TStoreSignedCategoryController extends BaseController {
    @Autowired
    private ITStoreSignedCategoryService tStoreSignedCategoryService;

    /**
     * 查询店铺的签约分类列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreSignedCategory:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStoreSignedCategory tStoreSignedCategory) {
        startPage();
        List<TStoreSignedCategory> list = tStoreSignedCategoryService.selectTStoreSignedCategoryList(tStoreSignedCategory);
        return getDataTable(list);
    }

    /**
     * 导出店铺的签约分类列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreSignedCategory:export')")
    @Log(title = "店铺的签约分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStoreSignedCategory tStoreSignedCategory) {
        List<TStoreSignedCategory> list = tStoreSignedCategoryService.selectTStoreSignedCategoryList(tStoreSignedCategory);
        ExcelUtil<TStoreSignedCategory> util = new ExcelUtil<TStoreSignedCategory>(TStoreSignedCategory.class);
        return util.exportExcel(list, "TStoreSignedCategory");
    }

    /**
     * 获取店铺的签约分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreSignedCategory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStoreSignedCategoryService.selectTStoreSignedCategoryById(id));
    }

    /**
     * 新增店铺的签约分类
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreSignedCategory:add')")
    @Log(title = "店铺的签约分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStoreSignedCategory tStoreSignedCategory) {
        return toAjax(tStoreSignedCategoryService.insertTStoreSignedCategory(tStoreSignedCategory));
    }

    /**
     * 修改店铺的签约分类
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreSignedCategory:edit')")
    @Log(title = "店铺的签约分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStoreSignedCategory tStoreSignedCategory) {
        return toAjax(tStoreSignedCategoryService.updateTStoreSignedCategory(tStoreSignedCategory));
    }

    /**
     * 删除店铺的签约分类
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreSignedCategory:remove')")
    @Log(title = "店铺的签约分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStoreSignedCategoryService.deleteTStoreSignedCategoryByIds(ids));
    }
}
