package com.ruoyi.web.controller.marketing;


import com.ruoyi.marketing.domain.MarketingPic;
import com.ruoyi.marketing.service.MarketingPicService;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.CommonResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 秒杀图片控制器
 *
 * @author 魔金商城 created on 2020/5/12
 */
@RestController
@Api("秒杀图片接口")
public class SeckillPicController {

    /**
     * 注入促销图片服务接口
     */
    @Autowired
    private MarketingPicService marketingPicService;


    /**
     * 查询秒杀促销图片信息
     *
     * @return 秒杀促销图片信息
     */
    @GetMapping("/panicbuypic")
    @ApiOperation(value = "查询秒杀促销图片信息", notes = "查询秒杀促销图片信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "秒杀促销图片信息", response = MarketingPic.class)
    })
    public CommonResponse<MarketingPic> queryPanicBuyPic() {
        return CommonResponse.build(marketingPicService.queryMarketingPic(CommonConstant.PANIC_MARKETING_PIC_TYPE, CommonConstant.ADMIN_STOREID));
    }

    /**
     * 保存秒杀促销图片
     *
     * @param marketingPic 促销图片实体
     * @return 成功1 否则失败
     */
    @PostMapping("/panicbuypic")
    @ApiOperation(value = "保存秒杀促销图片", notes = "保存秒杀促销图片（需要认证）")
    @PreAuthorize("@ss.hasPermi('panicbuy')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int savePanicBuyPic(@RequestBody MarketingPic marketingPic) {
        return marketingPicService.saveMarketingPic(marketingPic.addType(CommonConstant.PANIC_MARKETING_PIC_TYPE).addStoreId(CommonConstant.ADMIN_STOREID));
    }

}
