package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStoreOrderAttr;
import com.ruoyi.store.service.ITStoreOrderAttrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 门店订单附属信息Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStoreOrderAttr")
public class TStoreOrderAttrController extends BaseController {
    @Autowired
    private ITStoreOrderAttrService tStoreOrderAttrService;

    /**
     * 查询门店订单附属信息列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderAttr:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStoreOrderAttr tStoreOrderAttr) {
        startPage();
        List<TStoreOrderAttr> list = tStoreOrderAttrService.selectTStoreOrderAttrList(tStoreOrderAttr);
        return getDataTable(list);
    }

    /**
     * 导出门店订单附属信息列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderAttr:export')")
    @Log(title = "门店订单附属信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStoreOrderAttr tStoreOrderAttr) {
        List<TStoreOrderAttr> list = tStoreOrderAttrService.selectTStoreOrderAttrList(tStoreOrderAttr);
        ExcelUtil<TStoreOrderAttr> util = new ExcelUtil<TStoreOrderAttr>(TStoreOrderAttr.class);
        return util.exportExcel(list, "TStoreOrderAttr");
    }

    /**
     * 获取门店订单附属信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderAttr:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStoreOrderAttrService.selectTStoreOrderAttrById(id));
    }

    /**
     * 新增门店订单附属信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderAttr:add')")
    @Log(title = "门店订单附属信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStoreOrderAttr tStoreOrderAttr) {
        return toAjax(tStoreOrderAttrService.insertTStoreOrderAttr(tStoreOrderAttr));
    }

    /**
     * 修改门店订单附属信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderAttr:edit')")
    @Log(title = "门店订单附属信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStoreOrderAttr tStoreOrderAttr) {
        return toAjax(tStoreOrderAttrService.updateTStoreOrderAttr(tStoreOrderAttr));
    }

    /**
     * 删除门店订单附属信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreOrderAttr:remove')")
    @Log(title = "门店订单附属信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStoreOrderAttrService.deleteTStoreOrderAttrByIds(ids));
    }
}
