package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStoreEvaluation;
import com.ruoyi.store.service.ITStoreEvaluationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 门店评价Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStoreEvaluation")
public class TStoreEvaluationController extends BaseController {
    @Autowired
    private ITStoreEvaluationService tStoreEvaluationService;

    /**
     * 查询门店评价列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreEvaluation:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStoreEvaluation tStoreEvaluation) {
        startPage();
        List<TStoreEvaluation> list = tStoreEvaluationService.selectTStoreEvaluationList(tStoreEvaluation);
        return getDataTable(list);
    }

    /**
     * 导出门店评价列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreEvaluation:export')")
    @Log(title = "门店评价", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStoreEvaluation tStoreEvaluation) {
        List<TStoreEvaluation> list = tStoreEvaluationService.selectTStoreEvaluationList(tStoreEvaluation);
        ExcelUtil<TStoreEvaluation> util = new ExcelUtil<TStoreEvaluation>(TStoreEvaluation.class);
        return util.exportExcel(list, "TStoreEvaluation");
    }

    /**
     * 获取门店评价详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreEvaluation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStoreEvaluationService.selectTStoreEvaluationById(id));
    }

    /**
     * 新增门店评价
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreEvaluation:add')")
    @Log(title = "门店评价", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStoreEvaluation tStoreEvaluation) {
        return toAjax(tStoreEvaluationService.insertTStoreEvaluation(tStoreEvaluation));
    }

    /**
     * 修改门店评价
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreEvaluation:edit')")
    @Log(title = "门店评价", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStoreEvaluation tStoreEvaluation) {
        return toAjax(tStoreEvaluationService.updateTStoreEvaluation(tStoreEvaluation));
    }

    /**
     * 删除门店评价
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreEvaluation:remove')")
    @Log(title = "门店评价", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStoreEvaluationService.deleteTStoreEvaluationByIds(ids));
    }
}
