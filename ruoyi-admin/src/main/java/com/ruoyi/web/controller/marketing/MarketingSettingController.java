package com.ruoyi.web.controller.marketing;


import com.ruoyi.marketing.domain.MarketingSetting;
import com.ruoyi.marketing.service.MarketingSettingService;
import com.ruoyi.util.CommonResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 促销设置控制器
 *
 * @author 魔金商城 created on 2019/5/27
 */
@RestController
@Api(description = "促销设置接口")
public class MarketingSettingController {

    /**
     * 注入促销设置服务接口
     */
    @Autowired
    private MarketingSettingService marketingSettingService;


    /**
     * 查询促销设置
     *
     * @return 促销设置信息
     */
    @GetMapping("/marketingsetting")
    @ApiOperation(value = "查询促销设置", notes = "查询促销设置（需要认证）")
    @PreAuthorize("@ss.hasPermi('marketingsetting')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "促销设置信息", response = MarketingSetting.class)
    })
    public CommonResponse<MarketingSetting> queryMarketingSetting() {
        return CommonResponse.build(marketingSettingService.queryMarketingSetting());
    }

    /**
     * 新增或修改促销设置
     *
     * @param marketingSetting 设置信息
     * @return 成功1 否则失败
     */
    @PostMapping("/marketingsetting")
    @ApiOperation(value = "新增或修改促销设置", notes = "新增或修改促销设置（需要认证）")
    @PreAuthorize("@ss.hasPermi('marketingsetting')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 否则失败", response = Integer.class)
    })
    public int addOrUpdateMarketingSetting(@RequestBody MarketingSetting marketingSetting) {
        return marketingSettingService.saveOrUpdateMarketingSetting(marketingSetting);
    }

}
