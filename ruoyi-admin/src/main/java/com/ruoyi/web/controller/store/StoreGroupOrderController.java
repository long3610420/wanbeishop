package com.ruoyi.web.controller.store;


import com.ruoyi.order.domain.OmsOrder;
import com.ruoyi.order.service.IOmsOrderService;
import com.ruoyi.order.service.IOrderApiService;
import com.ruoyi.order.vo.ConfirmOrderParams;
import com.ruoyi.order.vo.GroupOrder;
import com.ruoyi.order.vo.OrderItem;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import com.ruoyi.web.utils.AdminLoginUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by 魔金商城 on 2019/6/27.
 * 店铺拼团订单控制器
 */
@RestController
@Api(description = "店铺拼团订单接口")
public class StoreGroupOrderController {


    /**
     * 注入订单混合api接口
     */
    @Autowired
    private IOrderApiService IOrderApiService;

    /**
     * 订单服务接口
     */
    @Autowired
    private IOmsOrderService orderService;

    /**
     * 分页查询店铺拼团订单
     *
     * @param pageHelper    分页帮助类
     * @param queryCriteria 查询条件
     * @return 返回店铺拼团订单信息
     */
    @GetMapping("/storegrouporders")
    @ApiOperation(value = "分页查询店铺拼团订单", notes = "分页查询店铺拼团订单（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:storegrouporder:storegrouporders')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "Long", name = "filterPayTime", value = "过滤支付时间 1过滤付款24小时之内的 -1付款24小时以上的"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "groupStatus", value = "拼团状态 0未成团 1已成团 默认0"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "groupId", value = "拼团ID"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "customerName", value = "用户名"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "storeName", value = "店铺名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "startTime", value = "开始时间"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "endTime", value = "结束时间"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回店铺拼团订单信息", response = GroupOrder.class)
    })
    public BaseResponse queryStoreGroupOrders(@ApiIgnore PageHelper<GroupOrder> pageHelper, @ApiIgnore GroupOrder.QueryCriteria queryCriteria) {
        return BaseResponse.build(orderService.queryStoreGroupOrders(queryCriteria, pageHelper));
    }

    /**
     * 导出选中的店铺拼团订单信息
     *
     * @param status 订单状态
     * @param ids    订单id 数组
     * @throws IOException
     */
    @PostMapping("/exportstorecheckedgrouporder")
    @ApiOperation(value = "导出选中的店铺拼团订单信息", notes = "导出选中的店铺拼团订单信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('storegrouporder/exportstorecheckedgrouporder')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "status", value = "订单状态"),
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "订单id 数组"),
    })
    public void exportStoreCheckedGroupOrder(HttpServletResponse response, String status, Long... ids) throws IOException {
        OutputStream os = response.getOutputStream();
       /* ExcelUtils.exportExcel(response, String.valueOf("订单信息_" + System.currentTimeMillis()).concat(".xls"), () ->
                IOrderApiService.exportCheckedGroupOrder(os, status, ids, CommonConstant.QUERY_WITH_NO_STORE)
        );*/
    }

    /**
     * 导出所有店铺拼团订单信息
     *
     * @param status 订单状态
     * @throws IOException
     */
    @PostMapping("/exportstoreallgrouporder")
    @ApiOperation(value = "导出所有店铺拼团订单信息", notes = "导出所有店铺拼团订单信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('storegrouporder/exportstoreallgrouporder')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "status", value = "订单状态"),
    })
    public void exportStoreAllGroupOrder(HttpServletResponse response, String status) throws IOException {
        OutputStream os = response.getOutputStream();
        //   ExcelUtils.exportExcel(response, String.valueOf("订单信息_" + System.currentTimeMillis()).concat(".xls"), () -> IOrderApiService.exportAllGroupOrder(os, status, CommonConstant.QUERY_WITH_NO_STORE));
    }

    /**
     * 根据订单id查询店铺拼团订单信息  (订单的所有信息 此接口慎用)
     *
     * @param id 订单id
     * @return 返回订单信息
     */
    @GetMapping("/queryorderbyidforstoregroup/{id}")
    @ApiOperation(value = "根据订单id查询订单信息  (订单的所有信息 此接口慎用)", notes = "根据订单id查询订单信息  (订单的所有信息 此接口慎用)（需要认证）")
    @PreAuthorize("@ss.hasPermi('storegrouporder/queryorderbyidforstoregroup')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "订单id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回订单信息")
    })
    public OmsOrder queryOrderByIdForStoreGroup(@PathVariable long id) {
        return IOrderApiService.queryOrderDetailById(id, CommonConstant.QUERY_WITH_NO_STORE, CommonConstant.QUERY_WITH_NO_CUSTOMER, OrderItem.LOG, OrderItem.ATTR, OrderItem.SKUS);
    }

    /**
     * 分页查询店铺未支付拼团订单
     *
     * @param pageHelper    分页帮助类
     * @param queryCriteria 查询条件
     * @return 返回店铺未支付的拼团订单信息
     */
    @GetMapping("/querystorenotpaygrouporders")
    @ApiOperation(value = "分页查询店铺未支付拼团订单", notes = "分页查询店铺未支付拼团订单（需要认证）")
    @PreAuthorize("@ss.hasPermi('storegrouporder/querystorenotpaygrouporders')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "groupId", value = "拼团ID"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "customerName", value = "用户名"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "startTime", value = "开始时间"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "endTime", value = "结束时间"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回店铺拼团订单信息", response = GroupOrder.class)
    })
    public BaseResponse queryStoreNotPayGroupOrders(@ApiIgnore PageHelper<GroupOrder> pageHelper, @ApiIgnore GroupOrder.QueryCriteria queryCriteria) {
        return BaseResponse.build(orderService.queryStoreNotPayGroupOrders(queryCriteria, pageHelper));
    }


    /**
     * 导出选中的店铺拼团订单信息(未付款)
     *
     * @param ids 订单id 数组
     * @throws IOException
     */
    @PostMapping("/nopay/exportstorecheckedgrouporder")
    @ApiOperation(value = "导出选中的店铺拼团订单信息", notes = "导出选中的店铺拼团订单信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('storegrouporder/exportstorecheckednopaygrouporder')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "订单id 数组"),
    })
    public void exportStoreCheckedNoPayGroupOrder(HttpServletResponse response, Long... ids) throws IOException {
        OutputStream os = response.getOutputStream();
       /* ExcelUtils.exportExcel(response, String.valueOf("订单信息_" + System.currentTimeMillis()).concat(".xls"), () ->
                IOrderApiService.exportCheckedGroupOrder(os, "-1", ids, CommonConstant.QUERY_WITH_NO_STORE)
        );*/
    }

    /**
     * 导出所有店铺拼团订单信息(未付款)
     *
     * @throws IOException
     */
    @PostMapping("/nopay/exportstoreallgrouporder")
    @ApiOperation(value = "导出所有店铺拼团订单信息", notes = "导出所有店铺拼团订单信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('storegrouporder/exportstoreallnopaygrouporder')")
    public void exportStoreAllNoPayGroupOrder(HttpServletResponse response) throws IOException {
        OutputStream os = response.getOutputStream();
        //  ExcelUtils.exportExcel(response, String.valueOf("订单信息_" + System.currentTimeMillis()).concat(".xls"), () -> IOrderApiService.exportAllGroupOrder(os, "-1", CommonConstant.QUERY_WITH_NO_STORE));
    }

    /**
     * 根据订单id查询店铺拼团订单信息  (未付款)(订单的所有信息 此接口慎用)
     *
     * @param id 订单id
     * @return 返回订单信息
     */
    @GetMapping("nopay/queryorderbyidforstoregroup/{id}")
    @ApiOperation(value = "根据订单id查询订单信息  (订单的所有信息 此接口慎用)", notes = "根据订单id查询订单信息  (订单的所有信息 此接口慎用)（需要认证）")
    @PreAuthorize("@ss.hasPermi('storegrouporder/queryorderbyidfornopaystoregroup')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "订单id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回订单信息", response = OmsOrder.class)
    })
    public OmsOrder queryOrderByIdForNoPayStoreGroup(@PathVariable long id) {
        return IOrderApiService.queryOrderDetailById(id, CommonConstant.QUERY_WITH_NO_STORE, CommonConstant.QUERY_WITH_NO_CUSTOMER, OrderItem.LOG, OrderItem.ATTR, OrderItem.SKUS);
    }


    /**
     * 确认店铺的拼团未付款订单
     *
     * @param id      订单id
     * @param storeId 店铺id
     * @return 成功返回1 失败返回0
     */
    @PutMapping("/group/confirmstoreorder/{id}/{storeId}")
    @ApiOperation(value = " 确认店铺的拼团未付款订单", notes = " 确认店铺的拼团未付款订单（需要认证）")
    @PreAuthorize("@ss.hasPermi('storegrouporder/confirmstoregrouporder')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "订单id"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "店铺id"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "reason", value = "原因"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int confirmStoreGroupOrder(@PathVariable long id, @PathVariable long storeId, String reason) {
        return IOrderApiService.confirmOrderPayed(ConfirmOrderParams.buildManagerSource(id, storeId, reason, AdminLoginUtils.getInstance().getManagerName()));
    }

}
