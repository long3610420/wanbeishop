package com.ruoyi.web.controller.marketing;


import com.ruoyi.marketing.domain.RedEnvelope;
import com.ruoyi.marketing.domain.RedEnvelopeDetails;
import com.ruoyi.marketing.service.RedEnvelopeService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;

/**
 * Created by 魔金商城平台管理on 2019/7/9.
 * 红包控制器
 */
@RestController
@Api(description = "红包接口")
public class RedEnvelopeController {


    /**
     * 注入红包服务接口
     */
    @Autowired
    private RedEnvelopeService redEnvelopeService;

    /**
     * 0
     * 分页查询红包
     *
     * @param pageHelper 分页帮助类
     * @param name       红包名称
     * @return 返回红包信息
     */
    @GetMapping("/redenvelope")
    @ApiOperation(value = "分页查询红包", notes = "分页查询红包（需要认证）")
    @PreAuthorize("@ss.hasPermi('redenvelope')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "红包名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回红包信息", response = RedEnvelope.class)
    })
    public BaseResponse queryRedEnvelopes(@ApiIgnore PageHelper<RedEnvelope> pageHelper, String name) {
        return BaseResponse.build(redEnvelopeService.queryRedEnvelopes(pageHelper, name, CommonConstant.ADMIN_STOREID, false));
    }

    /**
     * 复制红包链接
     *
     * @param id 红包id
     */
    @GetMapping("/copyredenvelope/{id}")
    @ApiOperation(value = "复制红包链接", notes = "复制红包链接（需要认证）")
    @PreAuthorize("@ss.hasPermi('redenvelope')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "红包id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "红包链接", response = String.class)
    })
    public String copyRedEnvelopeUrl(@PathVariable long id) throws IOException {
        return redEnvelopeService.copyRedEnvelopeUrl(id);
    }

    /**
     * 删除红包
     *
     * @param ids 红包id数组
     * @return 成功1 失败0
     */
    @DeleteMapping("/redenvelope")
    @ApiOperation(value = "删除及批量删除红包", notes = "删除及批量删除红包（需要认证）")
    @PreAuthorize("@ss.hasPermi('redenvelope')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "ids", value = "红包id数组"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功1 失败0", response = Integer.class)
    })
    public int deleteRedEnvelope(long[] ids) {
        return redEnvelopeService.deleteRedEnvelope(ids);
    }


    /**
     * 添加红包
     *
     * @param redEnvelope 红包实体类
     * @return 返回 -1失败 -2限领个数小于等于零 -3限领个数大于总个数 -4开始时间大于结束时间 -5店铺报名截止时间大于开始时间 >=1成功
     */
    @PostMapping("/redenvelope")
    @ApiOperation(value = "添加红包", notes = "添加红包（需要认证）")
    @PreAuthorize("@ss.hasPermi('redenvelope')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回 -1失败 -2限领个数小于等于零 -3限领个数大于总个数 -4开始时间大于结束时间 -5店铺报名截止时间大于开始时间 >=1成功", response = Integer.class)
    })
    public int addRedEnvelope(@RequestBody RedEnvelope redEnvelope) {
        return redEnvelopeService.addRedEnvelope(redEnvelope);
    }


    /**
     * 查询红包详情
     *
     * @param id 红包id
     * @return 返回红包详情
     */
    @GetMapping("/redenvelope/{id}")
    @ApiOperation(value = "查询红包详情", notes = "查询红包详情（需要认证）")
    @PreAuthorize("@ss.hasPermi('redenvelope')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "红包id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回红包详情", response = RedEnvelopeDetails.class)
    })
    public RedEnvelopeDetails queryRedEnvelopeDetails(@PathVariable long id) {
        return redEnvelopeService.queryRedEnvelopeDetails(id);
    }
}
