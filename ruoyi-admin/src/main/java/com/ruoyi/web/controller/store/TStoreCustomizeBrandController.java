package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStoreCustomizeBrand;
import com.ruoyi.store.service.ITStoreCustomizeBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 店铺自定义品牌列Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStoreCustomizeBrand")
public class TStoreCustomizeBrandController extends BaseController {
    @Autowired
    private ITStoreCustomizeBrandService tStoreCustomizeBrandService;

    /**
     * 查询店铺自定义品牌列列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreCustomizeBrand:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStoreCustomizeBrand tStoreCustomizeBrand) {
        startPage();
        List<TStoreCustomizeBrand> list = tStoreCustomizeBrandService.selectTStoreCustomizeBrandList(tStoreCustomizeBrand);
        return getDataTable(list);
    }

    /**
     * 导出店铺自定义品牌列列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreCustomizeBrand:export')")
    @Log(title = "店铺自定义品牌列", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStoreCustomizeBrand tStoreCustomizeBrand) {
        List<TStoreCustomizeBrand> list = tStoreCustomizeBrandService.selectTStoreCustomizeBrandList(tStoreCustomizeBrand);
        ExcelUtil<TStoreCustomizeBrand> util = new ExcelUtil<TStoreCustomizeBrand>(TStoreCustomizeBrand.class);
        return util.exportExcel(list, "TStoreCustomizeBrand");
    }

    /**
     * 获取店铺自定义品牌列详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreCustomizeBrand:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStoreCustomizeBrandService.selectTStoreCustomizeBrandById(id));
    }

    /**
     * 新增店铺自定义品牌列
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreCustomizeBrand:add')")
    @Log(title = "店铺自定义品牌列", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStoreCustomizeBrand tStoreCustomizeBrand) {
        return toAjax(tStoreCustomizeBrandService.insertTStoreCustomizeBrand(tStoreCustomizeBrand));
    }

    /**
     * 修改店铺自定义品牌列
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreCustomizeBrand:edit')")
    @Log(title = "店铺自定义品牌列", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStoreCustomizeBrand tStoreCustomizeBrand) {
        return toAjax(tStoreCustomizeBrandService.updateTStoreCustomizeBrand(tStoreCustomizeBrand));
    }

    /**
     * 删除店铺自定义品牌列
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreCustomizeBrand:remove')")
    @Log(title = "店铺自定义品牌列", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStoreCustomizeBrandService.deleteTStoreCustomizeBrandByIds(ids));
    }
}
