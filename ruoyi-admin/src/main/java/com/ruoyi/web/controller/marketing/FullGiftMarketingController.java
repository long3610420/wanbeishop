package com.ruoyi.web.controller.marketing;


import com.ruoyi.goods.domain.PmsSku;
import com.ruoyi.goods.service.IPmsSkuService;
import com.ruoyi.marketing.domain.Marketing;
import com.ruoyi.marketing.service.MarketingQueryService;
import com.ruoyi.marketing.service.MarketingService;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.MarketingConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

/**
 * Created by 魔金商城平台管理on 2019/8/5.
 * 满赠促销控制器
 */
@RestController
@Api("满赠促销接口")
public class FullGiftMarketingController {

    /**
     * 注入促销查询接口
     */
    @Autowired
    private MarketingQueryService marketingQueryService;


    /**
     * 注入促销删除服务接口
     */
    @Resource(name = "marketingDeleteService")
    private MarketingService marketingDeleteService;

    /**
     * 注入单品服务
     */
    @Autowired
    private IPmsSkuService skuService;

    /**
     * 注入满赠促销服务接口
     */
    @Resource(name = "fullGiftService")
    private MarketingService fullGiftService;


    /**
     * 分页查询满赠促销信息
     *
     * @param pageHelper 分页帮助类
     * @param name       名称
     * @return 返回促销信息
     */
    @GetMapping("/fullgift")
    @ApiOperation(value = "分页查询满赠促销信息", notes = "分页查询满赠促销信息（需要认证）")
    @PreAuthorize("@ss.hasPermi('fullgift')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "满赠促销名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回促销信息", response = Marketing.class)
    })
    public BaseResponse queryFullGiftMarketing(@ApiIgnore PageHelper<Marketing> pageHelper, String name) {
        return BaseResponse.build(marketingQueryService.queryMarketings(pageHelper, name, "2", CommonConstant.ADMIN_STOREID, ""));
    }

    /**
     * 批量删除满赠促销
     *
     * @param marketingIds 促销id集合
     * @return 成功返回>0 否则返回0
     */
    @DeleteMapping("/fullgift")
    @ApiOperation(value = "批量删除满赠促销", notes = "批量删除满赠促销（需要认证）")
    @PreAuthorize("@ss.hasPermi('fullgift')")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "array", name = "marketingIds", value = "促销id集合"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回>0 否则返回0", response = Integer.class)
    })
    public int deleteFullGiftByIds(Long[] marketingIds) {
        return marketingDeleteService.deleteMarketings(marketingIds, CommonConstant.ADMIN_STOREID, MarketingConstant.FULL_GIFT_MARKETING);
    }


    /**
     * 分页查询单品信息（满赠用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回单品信息(包括规格信息)
     */
    @GetMapping("/fullgift/skus")
    @ApiOperation(value = "分页查询单品信息（满赠用）", notes = "分页查询单品信息（满赠用）（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForFullGift(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }


    /**
     * 添加满赠促销
     *
     * @param marketing 满赠促销
     * @return 成功返回1 失败返回0
     */
    @PostMapping("/fullgift")
    @ApiOperation(value = "添加满赠促销", notes = "添加满赠促销（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int addFullGiftMarketing(@RequestBody Marketing marketing) {
        return fullGiftService.addMarketing(marketing.setAddMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.FULL_GIFT_MARKETING));
    }

    /**
     * 根据促销id查询满赠促销信息
     *
     * @param id 促销id
     * @return 返回促销详情信息
     */
    @GetMapping("/fullgift/{id}")
    @ApiOperation(value = "根据促销id查询满赠促销信息", notes = "根据促销id查询满赠促销信息（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "促销id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回促销详情信息", response = Marketing.class)
    })
    public Marketing queryFullGiftMarketingById(@PathVariable long id) {
        return marketingQueryService.queryMarketingById(id, CommonConstant.QUERY_WITH_NO_STORE);
    }


    /**
     * 分页查询单品信息（修改满赠用）
     *
     * @param pageHelper 分页帮助类
     * @param name       单品名称
     * @param skuNo      单品编号
     * @return 返回单品信息(包括规格信息)
     */
    @GetMapping("/fullgift/update/skus")
    @ApiOperation(value = "分页查询单品信息（修改满赠用）", notes = "分页查询单品信息（修改满赠用）（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "name", value = "单品名称"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "skuNo", value = "单品编号"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回单品信息(包括规格信息)", response = PmsSku.class)
    })
    public BaseResponse querySkusForFullGiftUpdate(@ApiIgnore PageHelper<PmsSku> pageHelper, String name, String skuNo) {
        return BaseResponse.build(skuService.querySkusWithSpecs(pageHelper, CommonConstant.ADMIN_STOREID, name, skuNo));
    }

    /**
     * 修改满赠促销
     *
     * @param marketing 满赠促销
     * @return 成功返回1 失败返回0
     */
    @PutMapping("/fullgift")
    @ApiOperation(value = "修改满赠促销", notes = "修改满赠促销（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1 失败返回0", response = Integer.class)
    })
    public int updateFullGift(@RequestBody Marketing marketing) {
        return fullGiftService.updateMarketing(marketing.setUpdateMarketingDefaultValues(CommonConstant.ADMIN_STOREID, MarketingConstant.FULL_GIFT_MARKETING));
    }


}
