package com.ruoyi.web.controller.store;


import com.ruoyi.setting.bean.BaseInfoSet;
import com.ruoyi.setting.service.BaseInfoSetService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 魔金商城
 * @date 2019-06-27 10:27
 * <p>
 * 商品审核开关控制器
 */
@RestController
@Api(description = "商品审核开关接口")
public class SpuAuditSetController {


    /**
     * 注入信息设置实现类
     */
    @Autowired
    private BaseInfoSetService baseInfoSetService;


    /**
     * 查询基本信息和高级信息设置
     *
     * @return 基本信息和高级信息设置实体类
     */
    @GetMapping("/goodsauditset")
    @ApiOperation(value = "查询基本信息和高级信息设置", notes = "查询基本信息和高级信息设置（需要认证）")
    @PreAuthorize("@ss.hasPermi('store:goodsaudit:goodsauditlist')")
    @ApiResponses({
            @ApiResponse(code = 200, message = "基本信息和高级信息设置实体类", response = BaseInfoSet.class)
    })
    public BaseInfoSet queryBaseInfoSet() {
        return baseInfoSetService.queryBaseInfoSet();
    }

    /**
     * 设置审核开关状态
     *
     * @param storeSpuAudit 审核开关
     * @return 成功返回1，失败返回0
     */
    @PutMapping("/goodsauditset/{storeSpuAudit}")
    @ApiOperation(value = "设置审核开关状态", notes = "设置审核开关状态（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "storeSpuAudit", value = "审核开关"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功返回1，失败返回0", response = Integer.class)
    })
    public int setAuditSwitch(@PathVariable String storeSpuAudit) {
        return baseInfoSetService.setAuditSwitch(storeSpuAudit);
    }


}
