package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.store.domain.TStoreShoppingCart;
import com.ruoyi.store.service.ITStoreShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 门店购物车Controller
 *
 * @author 魔金商城
 * @date 2020-07-28
 */
@RestController
@RequestMapping("/store/TStoreShoppingCart")
public class TStoreShoppingCartController extends BaseController {
    @Autowired
    private ITStoreShoppingCartService tStoreShoppingCartService;

    /**
     * 查询门店购物车列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreShoppingCart:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStoreShoppingCart tStoreShoppingCart) {
        startPage();
        List<TStoreShoppingCart> list = tStoreShoppingCartService.selectTStoreShoppingCartList(tStoreShoppingCart);
        return getDataTable(list);
    }

    /**
     * 导出门店购物车列表
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreShoppingCart:export')")
    @Log(title = "门店购物车", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStoreShoppingCart tStoreShoppingCart) {
        List<TStoreShoppingCart> list = tStoreShoppingCartService.selectTStoreShoppingCartList(tStoreShoppingCart);
        ExcelUtil<TStoreShoppingCart> util = new ExcelUtil<TStoreShoppingCart>(TStoreShoppingCart.class);
        return util.exportExcel(list, "TStoreShoppingCart");
    }

    /**
     * 获取门店购物车详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreShoppingCart:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStoreShoppingCartService.selectTStoreShoppingCartById(id));
    }

    /**
     * 新增门店购物车
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreShoppingCart:add')")
    @Log(title = "门店购物车", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStoreShoppingCart tStoreShoppingCart) {
        return toAjax(tStoreShoppingCartService.insertTStoreShoppingCart(tStoreShoppingCart));
    }

    /**
     * 修改门店购物车
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreShoppingCart:edit')")
    @Log(title = "门店购物车", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStoreShoppingCart tStoreShoppingCart) {
        return toAjax(tStoreShoppingCartService.updateTStoreShoppingCart(tStoreShoppingCart));
    }

    /**
     * 删除门店购物车
     */
    @PreAuthorize("@ss.hasPermi('store:TStoreShoppingCart:remove')")
    @Log(title = "门店购物车", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStoreShoppingCartService.deleteTStoreShoppingCartByIds(ids));
    }
}
