package com.ruoyi.web.controller.promotion;


import com.ruoyi.order.domain.OmsOrder;
import com.ruoyi.order.service.IOrderApiService;
import com.ruoyi.order.vo.QueryOrderCriteria;
import com.ruoyi.util.BaseResponse;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author 魔金商城
 * @date 2019-06-18 18:52
 * <p>
 * 分销订单控制器
 */
@RestController
@Api(description = "分销订单接口")
public class SpreadOrderController {


    /**
     * 注入订单服务
     */
    @Autowired
    private IOrderApiService IOrderApiService;

    /**
     * 分页查询分销订单信息
     *
     * @param pageHelper    分页帮助类
     * @param queryCriteria 查询参数
     * @return 分销订单信息
     */
    @GetMapping("/spreadorder")
    @ApiOperation(value = "分页查询分销订单信息", notes = "分页查询分销订单信息（需要认证）")

    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "orderCode", value = "订单编号"),
            @ApiImplicitParam(paramType = "form", dataType = "string", name = "storeName", value = "店铺名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "分销订单信息", response = OmsOrder.class)
    })
    public BaseResponse querySpreadOrders(@ApiIgnore PageHelper<OmsOrder> pageHelper, @ApiIgnore QueryOrderCriteria queryCriteria) {
        queryCriteria.setCustomerId(CommonConstant.NO_CUSTOMER_ID);
        queryCriteria.setStoreId(CommonConstant.QUERY_WITH_NO_STORE);
        return BaseResponse.build(IOrderApiService.querySpreadOrdersForBack(pageHelper, queryCriteria));
    }


}
