package com.ruoyi.common.utils;



import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.*;

/**
 * @Author Dylan
 * @Description 对象参数校验工具
 * @Date  下午4:41
 * @Param
 * @return
 **/
public class ValidationUtils {

    private static Validator validator;

    static {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    /**
     * 分组校验
     * @param t 校验对象实例
     * @param groups 验证分组
     * @param <T>
     * @return 属性名:错误信息;属性名:错误信息;属性名:错误信息;
     */
    public static <T> String validateToStringPair(T t,Class... groups) {
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(t,groups);
        if(constraintViolations.size() > 0) {
            StringBuffer buffer = new StringBuffer();
            for(ConstraintViolation<T> violation: constraintViolations) {
                buffer.append(violation.getPropertyPath()).append(":").append(violation.getMessage()).append(";");
            }
            return buffer.toString();
        }
        return null;
    }
    /**
     * 分组校验
     * @param t 校验对象实例
     * @param groups 验证分组
     * @param <T>
     * @return 错误信息;错误信息;错误信息;
     */
    public static <T> String validateToString(T t,Class... groups) {
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(t,groups);
        if(constraintViolations.size() > 0) {
            StringBuffer buffer = new StringBuffer();
            for(ConstraintViolation<T> violation: constraintViolations) {
                buffer.append(violation.getMessage()).append(";");
            }
            return buffer.toString();
        }
        return null;
    }
    /**
     * 分组校验
     * @param t 校验对象实例
     * @param groups 验证分组
     * @param <T>
     * @return list value=错误信息
     */
    public static <T> List<String> validateToList(T t, Class... groups) {
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(t,groups);
        if(constraintViolations.size() > 0) {
            List<String> messages = new ArrayList<>(constraintViolations.size());
            for(ConstraintViolation<T> violation: constraintViolations) {
                messages.add(violation.getMessage());
            }
            return messages;
        }
        return new ArrayList<>();
    }

    /**
     * 分组校验
     * @param t 校验对象实例
     * @param groups 验证分组
     * @param <T>
     * @return Map key=属性名 value=错误信息
     */
    public static <T> Map<String,String> validateToMap(T t, Class... groups) {
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(t,groups);
        if(constraintViolations.size() > 0) {
            Map<String,String> propertyMessageMap = new HashMap<>();
            for(ConstraintViolation<T> violation: constraintViolations) {
                propertyMessageMap.put(violation.getPropertyPath().toString(),violation.getMessage());
            }
            return propertyMessageMap;
        }
        return null;
    }


}
