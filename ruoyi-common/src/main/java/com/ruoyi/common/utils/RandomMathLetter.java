package com.ruoyi.common.utils;

import java.util.Random;

/**
 * 随机生成字母和数字组合
 *
 * @author 魔金商城 on 2017/6/1.
 */
public class RandomMathLetter {
    /**
     * 产生随机字符串
     */
    private static Random randGen = null;
    /**
     * 数字和字母
     */
    private static char[] numbersAndLetters = null;

    /**
     * 字符串
     */
    public static String randomString(int length) {
        if (length < 1) {
            return null;
        }
        if (randGen == null) {
            randGen = new Random();
            numbersAndLetters = ("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
        }
        char[] randBuffer = new char[length];
        for (int i = 0; i < randBuffer.length; i++) {
            randBuffer[i] = numbersAndLetters[randGen.nextInt(33)];
        }
        return new String(randBuffer);
    }
    public static String randomInt(int length) {
        if (length < 1) {
            return null;
        }
        if (randGen == null) {
            randGen = new Random();
            numbersAndLetters = ("012345678901234567890123456789789789").toCharArray();
        }
        char[] randBuffer = new char[length];
        for (int i = 0; i < randBuffer.length; i++) {
            randBuffer[i] = numbersAndLetters[randGen.nextInt(30)];
        }
        return new String(randBuffer);
    }
}
