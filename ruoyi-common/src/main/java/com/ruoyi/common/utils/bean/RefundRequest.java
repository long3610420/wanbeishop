package com.ruoyi.common.utils.bean;

import lombok.Data;

/**
 * Created by dujinkai on 18/2/7.
 * 提现请求实体
 */
@Data
public class RefundRequest {

    /**
     * 交流流水号
     */
    private String tradeNo;

    /**
     * 退款理由
     */
    private String refundReason;

    /**
     * 提现金额
     */
    private String money;

    private String refuseNo;
}
