package com.ruoyi.member.service.impl;


import com.ruoyi.integral.domain.CustomerPoint;
import com.ruoyi.integral.service.CustomerPointService;
import com.ruoyi.marketing.domain.RegisterMarketing;
import com.ruoyi.marketing.service.CouponService;
import com.ruoyi.marketing.service.RegisterMarketingService;
import com.ruoyi.member.domain.UmsMember;
import com.ruoyi.member.mapper.UmsMemberMapper;
import com.ruoyi.member.service.IUmsMemberService;
import com.ruoyi.member.service.RegisterService;
import com.ruoyi.member.service.RegisterServiceApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * 注册服务聚合接口实现
 */
@Service
public class RegisterServiceApiImpl implements RegisterServiceApi {

    /**
     * 日志
     */
    private Logger logger = LoggerFactory.getLogger(RegisterServiceApiImpl.class);

    /**
     * 注入注册服务
     */
    @Autowired
    private RegisterService registerService;

    /**
     * 注入注册营销服务
     */
    @Autowired
    private RegisterMarketingService registerMarketingService;

    /**
     * 注入会员积分服务
     */
    @Autowired
    private CustomerPointService customerPointService;
    /**
     * 注入会员数据库接口
     */
    @Autowired
    private UmsMemberMapper customerMapper;

    /**
     * 注入优惠券服务
     */
    @Autowired
    private CouponService couponService;
    /**
     * 密码工具类
     */
    @Autowired
    @Lazy
    private PasswordEncoder passwordEncoder;

    /**
     * 注入会员服务
     */
    @Autowired
    private IUmsMemberService customerService;

    @Override
    @Transactional
    public int registerCustomer(String mobile, String password, String code, String originCode, String recommondCode) {
        logger.debug("registerCustomer and mobile :{} \r\n code:{} \r\n: originCode{} \r\n recommondCode:{}", mobile, code, originCode, recommondCode);
        //注册用户
        int res = registerService.registerCustomer(mobile, password, code, originCode, recommondCode);
        //注册成功后执行注册营销
        if (res > 0) {
            //根据手机号查找用户
            UmsMember customer = customerService.queryCustomerByNameInWriteDataSource(mobile);
            //查找注册营销
            RegisterMarketing registerMarketing = registerMarketingService.queryRegisterMarketing();
            if (!ObjectUtils.isEmpty(registerMarketing)) {
                if (registerMarketing.isEffective()) {
                    //赠送积分
                    logger.info("registerCustomer success : present point");
                    customerPointService.addCustomerPoint(CustomerPoint.buildForRegister(customer.getId(), registerMarketing.getPointNum()));
                }
                if (registerMarketing.isCanReceiveCoupon()) {
                    //赠送优惠券
                    logger.info("registerCustomer success : present coupon");
                    couponService.receiveCoupon(customer.getId(), registerMarketing.getCouponId(), 2);
                }
            }
        }
        return res;
    }

    @Override
    @Transactional
    public int bindMobile(long customerId, String mobile, String password, String code, String originCode, String recommondCode) {
        logger.debug("bindMobile and mobile :{} \r\n code:{} \r\n: originCode{} \r\n recommondCode:{}", mobile, code, originCode, recommondCode);
        //注册用户
        logger.debug("bindMobile and mobile :{} \r\n code:{} \r\n: originCode{} \r\n recommondCode:{}", mobile, code, originCode, recommondCode);

        if (StringUtils.isEmpty(code)) {
            logger.error("bindMobile fail due to code is error...");
            return -1;
        }

        if (StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)) {
            logger.error("bindMobile fail due to mobile or password is empty...");
            return -2;
        }
        // 校验验证码是否正确
        if (!code.equals(originCode)) {
            logger.error("bindMobile fail due to code is error...");
            return -1;
        }
        UmsMember existCustomer = customerService.queryCustomerByName(mobile);
        // 判断手机号码是否存在
        if (existCustomer != null) {
            UmsMember customer = customerService.queryCustomerInfoById(customerId);
            existCustomer.setUpdateTime(new Date());
            existCustomer.setH5OpenId(customer.getH5OpenId());
            existCustomer.setAppletOpenId(customer.getAppletOpenId());
            existCustomer.setAppOpenId(customer.getAppOpenId());
            customerService.deleteUmsMemberById(customerId);
            return customerService.updateCustomer(existCustomer);
        }

        // 进行会员注册
        UmsMember customer = customerService.queryCustomerInfoById(customerId);
        // 设置会员密码
        customer.setPassword(passwordEncoder.encode(customer.getPassword()));
        customer.setConsumptionAmount(BigDecimal.ZERO);
        //设置自己的推荐码
        customer.addSelfRecommondCode();

        // 如果有推荐人 则查询推荐人信息
        if (customer.hasRecommonded()) {
            // 推荐人
            UmsMember recommonded = customerMapper.queryCustomerByRecommondCode(customer.getRecommondCode());

            // 如果推荐人不存在  则返回错误
            if (Objects.isNull(recommonded)) {
                logger.error("addCustomer fail due to recommoned is not exist...and code:{}", customer.getRecommondCode());
                return -10;
            }
            // 设置推荐人的会员id
            customer.setRecommended(recommonded.getId());

            // 设置会员的二级推荐人/ 推荐人的推荐人
            customer.setSRecommended(recommonded.getRecommended());
        }
        //注册成功后执行注册营销
        if (customerService.updateCustomer(customer) > 0) {

            //查找注册营销
            RegisterMarketing registerMarketing = registerMarketingService.queryRegisterMarketing();
            if (!ObjectUtils.isEmpty(registerMarketing)) {
                if (registerMarketing.isEffective()) {
                    //赠送积分
                    logger.info("registerCustomer success : present point");
                    customerPointService.addCustomerPoint(CustomerPoint.buildForRegister(customer.getId(), registerMarketing.getPointNum()));
                }
                if (registerMarketing.isCanReceiveCoupon()) {
                    //赠送优惠券
                    logger.info("registerCustomer success : present coupon");
                    couponService.receiveCoupon(customer.getId(), registerMarketing.getCouponId(), 2);
                }
            }
        }
        return 0;
    }
}
