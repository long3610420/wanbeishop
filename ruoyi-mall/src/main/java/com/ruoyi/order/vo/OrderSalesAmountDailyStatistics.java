package com.ruoyi.order.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 订单销售查询统计结果item
 */
@Data
public class OrderSalesAmountDailyStatistics implements Serializable {

    private static final long serialVersionUID = 1L;

    private String orderDate;

    private BigDecimal salesAmount;

}
