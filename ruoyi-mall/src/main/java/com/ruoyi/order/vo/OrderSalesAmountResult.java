package com.ruoyi.order.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 订单销售查询统计结果
 */
@Data
public class OrderSalesAmountResult implements Serializable {

    private static final long serialVersionUID = 1L;

    private BigDecimal salesAmount ;

    List<OrderSalesAmountDailyStatistics> itemList;

    public OrderSalesAmountResult calculate() {
        this.setSalesAmount(itemList.stream().map(OrderSalesAmountDailyStatistics::getSalesAmount).reduce(BigDecimal.ZERO, BigDecimal::add));
        return this;
    }

}
