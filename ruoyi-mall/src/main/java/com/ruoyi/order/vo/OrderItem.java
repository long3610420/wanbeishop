package com.ruoyi.order.vo;

/**
 * Created by 魔金商城 on 17/11/13.
 * 订单枚举
 */
public enum OrderItem {
    LOG, ATTR, SKUS, CANREFUND, CANRETRUN, EXPRESSURL, STORE_INFO, BACK_PROGRESS, CUSTOMER, COMMUNITYHEAD
}
