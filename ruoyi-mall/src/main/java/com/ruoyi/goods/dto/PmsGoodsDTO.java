package com.ruoyi.goods.dto;


import com.ruoyi.goods.domain.PmsGoods;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

/**
 * 商品对象传输对象
 *
 * @author Dylan
 * @date 2020-12-23
 * @see PmsGoods
 */
@ApiModel(description = "商品参数对象")
public class PmsGoodsDTO {

    /**
     * 主键id
     */
    @ApiModelProperty(value = "商品id")
    private Long id;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    private String name;

    /**
     * 商品副标题
     */
    private String subtitle;

    /**
     * 商品的店铺id 如果是平台的则为0
     */
    @ApiModelProperty(value = "店铺id")
    private Long storeId;

    /**
     * 一级分类id
     */
    @ApiModelProperty(value = "一级分类id")
    private Long firstCateId;

    /**
     * 二级分类id
     */
    private Long secondCateId;

    /**
     * 三级分类id
     */
    private Long thirdCateId;

    /**
     * 类型id
     */
    private Long typeId;

    /**
     * 品牌id
     */
    @ApiModelProperty(value = "品牌id")
    private Long brandId;

    /**
     * 店铺一级分类
     */
    private Long storeFcateId;

    /**
     * 店铺二级分类
     */
    private Long storeScateId;

    /**
     * 店铺三级分类
     */
    private Long storeTcateId;

    /**
     * 删除标记   0未删除 1删除 默认0
     */
    private int delFlag;

    /**
     * 佣金比例
     */
    private BigDecimal commissionRate;

    /**
     * 二级佣金比例
     */
    private BigDecimal sCommissionRate;

    /**
     * 审核状态  状态  0 申请中  1通过 2 拒绝
     */
    @ApiModelProperty(value = "审核状态  状态  0 申请中  1通过 2 拒绝")
    private String status;

    /**
     * 商品上架状态 0 下架  1上架 2违规下架 默认0
     */
    @ApiModelProperty(value = "商品上架状态 0 下架  1上架 2违规下架 默认0")
    private String shelvesStatus;

    /**
     * 是否是虚拟商品 0 否 1 是默认0
     */
    @ApiModelProperty(value = "是否是虚拟商品 0 否 1 是默认0")
    private String isVirtual;

    /**
     * 商品状态 0 口碑好货 1 新品扫雷
     */
    @ApiModelProperty(value = "商品状态 0 口碑好货 1 新品扫雷")
    private String goodsStatus;


    /**
     * 商品导入id
     */
    private long spuImportId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public Long getFirstCateId() {
        return firstCateId;
    }

    public void setFirstCateId(Long firstCateId) {
        this.firstCateId = firstCateId;
    }

    public Long getSecondCateId() {
        return secondCateId;
    }

    public void setSecondCateId(Long secondCateId) {
        this.secondCateId = secondCateId;
    }

    public Long getThirdCateId() {
        return thirdCateId;
    }

    public void setThirdCateId(Long thirdCateId) {
        this.thirdCateId = thirdCateId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getStoreFcateId() {
        return storeFcateId;
    }

    public void setStoreFcateId(Long storeFcateId) {
        this.storeFcateId = storeFcateId;
    }

    public Long getStoreScateId() {
        return storeScateId;
    }

    public void setStoreScateId(Long storeScateId) {
        this.storeScateId = storeScateId;
    }

    public Long getStoreTcateId() {
        return storeTcateId;
    }

    public void setStoreTcateId(Long storeTcateId) {
        this.storeTcateId = storeTcateId;
    }

    public int getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(int delFlag) {
        this.delFlag = delFlag;
    }

    public BigDecimal getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(BigDecimal commissionRate) {
        this.commissionRate = commissionRate;
    }

    public BigDecimal getsCommissionRate() {
        return sCommissionRate;
    }

    public void setsCommissionRate(BigDecimal sCommissionRate) {
        this.sCommissionRate = sCommissionRate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShelvesStatus() {
        return shelvesStatus;
    }

    public void setShelvesStatus(String shelvesStatus) {
        this.shelvesStatus = shelvesStatus;
    }

    public String getIsVirtual() {
        return isVirtual;
    }

    public void setIsVirtual(String isVirtual) {
        this.isVirtual = isVirtual;
    }

    public String getGoodsStatus() {
        return goodsStatus;
    }

    public void setGoodsStatus(String goodsStatus) {
        this.goodsStatus = goodsStatus;
    }

    public long getSpuImportId() {
        return spuImportId;
    }

    public void setSpuImportId(long spuImportId) {
        this.spuImportId = spuImportId;
    }

    @Override
    public String toString() {
        return "PmsGoodsDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", subtitle='" + subtitle + '\'' +
                ", storeId=" + storeId +
                ", firstCateId=" + firstCateId +
                ", secondCateId=" + secondCateId +
                ", thirdCateId=" + thirdCateId +
                ", typeId=" + typeId +
                ", brandId=" + brandId +
                ", storeFcateId=" + storeFcateId +
                ", storeScateId=" + storeScateId +
                ", storeTcateId=" + storeTcateId +
                ", delFlag=" + delFlag +
                ", commissionRate=" + commissionRate +
                ", sCommissionRate=" + sCommissionRate +
                ", status='" + status + '\'' +
                ", shelvesStatus='" + shelvesStatus + '\'' +
                ", isVirtual='" + isVirtual + '\'' +
                ", goodsStatus='" + goodsStatus + '\'' +
                ", spuImportId=" + spuImportId +
                '}';
    }
}
