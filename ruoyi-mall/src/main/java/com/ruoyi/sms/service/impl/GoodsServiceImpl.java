package com.ruoyi.sms.service.impl;

import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.goods.domain.PmsGoods;
import com.ruoyi.goods.dto.PmsGoodsDTO;
import com.ruoyi.goods.mapper.PmsGoodsMapper;
import com.ruoyi.sms.service.IGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Deacription 商品接口实现
 * @Author Dylan
 * @Date 2020/12/23 下午4:33
 * @Version 1.0
 **/
@Service
public class GoodsServiceImpl implements IGoodsService {

    @Autowired
    private PmsGoodsMapper pmsGoodsMapper;

    @Override
    public List<PmsGoods> list(PmsGoodsDTO dto) {
        PmsGoods params = new PmsGoods();
        BeanUtils.copyBeanProp(params,dto);
        return pmsGoodsMapper.selectPmsGoodsList(params);
    }
}
