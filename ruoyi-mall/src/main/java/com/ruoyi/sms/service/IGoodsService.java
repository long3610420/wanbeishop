package com.ruoyi.sms.service;

import com.ruoyi.goods.domain.PmsGoods;
import com.ruoyi.goods.dto.PmsGoodsDTO;

import java.util.List;

/**
 * @Deacription 商品相关接口
 * @Author Dylan
 * @Date 2020/12/23 下午4:30
 * @Version 1.0
 **/
public interface IGoodsService {

    /**
     * @Author Dylan
     * @Description 根据条件查询商品列表
     * @Date  下午4:32
     * @Param [dto]
     * @return java.util.List<com.ruoyi.goods.domain.PmsGoods>
     **/
    List<PmsGoods> list(PmsGoodsDTO dto);

}
