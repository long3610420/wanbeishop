package com.ruoyi.controller;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.goods.dto.PmsGoodsDTO;
import com.ruoyi.index.IndexController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ClassName IndexControllerTest
 * @Deacription TODO
 * @Author Dylan
 * @Date 2020/12/23 下午6:10
 * @Version 1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
//@AutoConfigureMockMvc
public class IndexControllerTest {

    @Autowired
    private IndexController indexController;

    @Test
    public void testGoodsList(){
        PmsGoodsDTO dto = new PmsGoodsDTO();
        dto.setFirstCateId(19L);
        AjaxResult o = indexController.goodsList(dto, 1, 3);
        Assert.assertNotNull(o);


    }

}
