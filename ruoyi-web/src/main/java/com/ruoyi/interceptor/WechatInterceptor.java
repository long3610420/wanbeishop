package com.ruoyi.interceptor;


import com.ruoyi.setting.service.ILsPaySettingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 微信拦截器 (判断是否微信内置浏览器访问)
 */
@Component
public class WechatInterceptor extends HandlerInterceptorAdapter {

    /**
     * 注入登录设置服务
     */
    @Autowired
    private ILsPaySettingService loginSetService;

    /**
     * 调试日志
     */
    private Logger logger = LoggerFactory.getLogger(WechatInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        return true;
    }




}
