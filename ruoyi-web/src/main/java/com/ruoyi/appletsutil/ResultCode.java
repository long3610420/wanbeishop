package com.ruoyi.appletsutil;

/**
 * Created by 魔金商城 on 17/7/10.
 * 结果返回吗
 */
public interface ResultCode {

    /**
     * 操作成功；
     */
    String SUCCESSFUL = "操作成功";

    /**
     * 操作失败；
     */
    String FAILED = "操作失败";

    /**
     * 微信未授权
     */
    String WX_NOT_AUTHORIZED = "微信未授权";

    /**
     * 微信未关联用户
     */
    String WX_NOT_LINKD = "微信未关联用户";

    /**
     * 小程序账号已绑定错误
     */
    String WX_ALREADY_BIND_ERROR = "小程序账号已绑定错误";

}
