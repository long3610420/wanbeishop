package com.ruoyi.appletsutil;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.constant.Rediskey;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.member.domain.UmsMember;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * 小程序登录工具类
 *
 * @author SK
 * @since 2018/6/13
 */
public class AppletsLoginUtils {
    private static AppletsLoginUtils ourInstance = new AppletsLoginUtils();


    private AppletsLoginUtils() {
    }

    public static AppletsLoginUtils getInstance() {
        return ourInstance;
    }

    /**
     * 获得用户id
     *
     * @return 返回用户id
     */
    public long getCustomerId(HttpServletRequest request) {
        long customerId = 0;
        if (!ObjectUtils.isEmpty(getClaims(getToken(request)))) {
            customerId = getClaims(getToken(request)).getId();
        }
        return customerId;
    }
    /**
     * 获取token
     *
     * @param request request
     * @return token
     */
    private String getToken(HttpServletRequest request) {
        // 认证信息在header 中的key
        final String authHeader = request.getHeader("Authorization");

        if (Objects.isNull(authHeader) || !authHeader.startsWith("Bearer")) {
            throw new UnAuthorizedException(ResultCode.WX_NOT_AUTHORIZED);
        }
        return authHeader.length() >= 7 ? authHeader.substring(7) : authHeader.substring(6);
    }

    /**
     * 获取小程序凭证实体
     *
     * @param token token
     * @return 小程序凭证实体
     */
    private UmsMember getClaims(String token) {
        if (StringUtils.isEmpty(token)){
            return null;
        }
        String claims = SpringUtils.getBean(RedisCache.class).getValue(String.format(Rediskey.TOKEN, token));
        return StringUtils.isEmpty(claims) ? null : JSONObject.parseObject(claims, UmsMember.class);
    }

}
